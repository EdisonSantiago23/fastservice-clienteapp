import React from 'react';

import FlashMessage from 'react-native-flash-message';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {LoginStack} from './app/screens/Navigation/LoginStack';
import {UsuarioTabNavigator} from './app/screens/Navigation/UsuarioTabNavigator';
import {AuthContext} from './app/api/AuthContext';
import {MenuProvider} from 'react-native-popup-menu';
import SplashScreen from './app/common/SplashScreen';
import AsyncStorage from '@react-native-community/async-storage';
import Constants from './app/common/Constants';
import mecanico from './app/api/mecanico';

export default () => {
  let modaApp = new mecanico();
  const [isLoading, setIsLoading] = React.useState(true);
  const [userToken, setUserToken] = React.useState(null);
  const [User, setUser] = React.useState(null);
  const [splash, setSplash] = React.useState([]);

  const authContext = React.useMemo(() => {
    return {
      signIn: user => {
        try {
          setUser(user);
          setIsLoading(false);
          setUserToken(user.idLocal);
          AsyncStorage.setItem(
            Constants.STORE_ID_USER,
            JSON.stringify(user.id),
          );
          AsyncStorage.setItem(Constants.USER_TOKEN, user.idLocal);
        } catch (e) {
          console.log(e);
        }
      },
      getUser: () => User,
      setUser: user => setUser(user),
      getToken: () => userToken,
      getSplash: () => {
        return splash;
      },

      signOut: () => {
        setIsLoading(true);
        setUserToken(null);
        setUser(null);
        //USER DATA
        AsyncStorage.removeItem(Constants.USER_TOKEN);
        AsyncStorage.removeItem(Constants.STORE_ID_USER);
        setTimeout(() => {
          setIsLoading(false);
        }, 2000);
      },
    };
  }, [User, splash, userToken]);

  React.useEffect(() => {
    modaApp.getSplash(0).then(resp => {
      if (resp.length > 1) {
        setSplash(resp);
      }
    });
    setIsLoading(true);
    try {
      AsyncStorage.getItem(Constants.STORE_ID_USER).then(respToken => {
        console.log(respToken);
        if (respToken) {
          AsyncStorage.getItem(Constants.STORE_ID_USER).then(idUser => {
            if (idUser) {
              modaApp.getUserInfo(idUser).then(resp => {
                      if (resp) {
                  setUser(resp);
                  setUserToken(respToken);
                  setIsLoading(false);
                } else {
                  AsyncStorage.removeItem(Constants.USER_TOKEN);
                  AsyncStorage.removeItem(Constants.STORE_ID_USER);
                }
                setIsLoading(false);
              });
            }
          });
        }
      });
    } catch (e) {
      setIsLoading(false);
    }
    setTimeout(() => {
      setIsLoading(false);
    }, 4000);
    // esl
  }, []);

  const Stack = createStackNavigator();

  return (
    <AuthContext.Provider value={authContext}>
      <MenuProvider>
        <NavigationContainer>
          {isLoading ? (
            <Stack.Navigator screenOptions={{headerShown: false}}>
              <Stack.Screen name="SplashScreen" component={SplashScreen} />
            </Stack.Navigator>
          ) : userToken ? (
            <UsuarioTabNavigator />
          ) : (
            <LoginStack />
          )}
        </NavigationContainer>
        <FlashMessage position={'top'} />
      </MenuProvider>
    </AuthContext.Provider>
  );
};
