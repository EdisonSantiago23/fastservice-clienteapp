import { Alert } from 'react-native';

export default class mecanico {
  version = 'v1';
  constructor() { }
  static getQueryURL = version => {
  //  let baseUrl = 'http://192.168.1.2:80/mecanico/public/api/';
    let baseUrl = 'http://mecanico.balinetsoft.com/api/';
    let url = baseUrl;
    url += version;
    return url;
  };

  urlFetch = (url, options) => {
    return fetch(url, options)
      .then(res => res.json())
      .then(res => {
        if (res.status === 'exception') {
          return Promise.resolve(res);
        }
        if (res.status === 'error') {
          return Promise.resolve(res);
        } else {
          if (res.data) {
            return Promise.resolve(res.data);
          } else {
            return res;
          }
        }
      })
      .catch(function (error) {
        let errorTittle = 'Error';
        if (error.message == 'Network request failed') {
          error.message =
            'No se ha podido conectar con el servidor, revisa tu conexión a internet';
          errorTittle = 'Error de conexión';
        }
        Alert.alert(errorTittle, error.message, [{ text: 'Cerrar' }]);
        return { data: '', status: 'fail', error: error.message };
      });
  };

  setFormBody = params => {
    var formBody = [];
    for (var property in params) {
      var encodedKey = encodeURIComponent(property);
      var encodedValue = encodeURIComponent(params[property]);
      formBody.push(encodedKey + '=' + encodedValue);
    }
    formBody = formBody.join('&');
    return formBody;
  };

  loginUser = params => {
    let url = mecanico.getQueryURL(this.version);
    let formBody = this.setFormBody(params);
    let options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: formBody,
    };
    url += '/login';
    return this.urlFetch(url, options);
  };
  createNovedades = params => {
    let url = mecanico.getQueryURL(this.version);
    let options = {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(params),
    };
    url += '/novedadesnew';
    return this.urlFetch(url, options);
  };
  createLocal = params => {
    let url = mecanico.getQueryURL(this.version);
    let options = {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(params),
    };
    url += '/createLocal';
    return this.urlFetch(url, options);
  };
  crearReserva = params => {
    let url = mecanico.getQueryURL(this.version);
    let options = {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(params),
    };
    url += '/createreserva';
    return this.urlFetch(url, options);
  };
  crearReservaRepuesto = params => {
    let url = mecanico.getQueryURL(this.version);
    let options = {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(params),
    };
    url += '/createreservarepuesto';
    return this.urlFetch(url, options);
  };

  finalizarCita = params => {
    let url = mecanico.getQueryURL(this.version);
    let options = {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(params),
    };
    url += '/respuestalocalreservafinalizar';
    return this.urlFetch(url, options);
  };

  
  getInfoUserPay = idLocal => {
    let url = mecanico.getQueryURL(this.version);
    let options = {};
    url += '/getInfoUserPay/' + idLocal;
    console.log(url);
    return this.urlFetch(url, options);
  };
  newpago = (id,idservice) => {
    let url = mecanico.getQueryURL(this.version);
    let options = {};
    url += '/newpago/' + id + '/' + idservice;
    console.log(url);
    return this.urlFetch(url, options);
  };
  
  GetMecanicos = idLocal => {
    let url = mecanico.getQueryURL(this.version);
    let options = {};
    url += '/mecanicos/' + idLocal;
    return this.urlFetch(url, options);
  };
  GetGarantias = idmarca => {
    let url = mecanico.getQueryURL(this.version);
    let options = {};
    url += '/getgarantia/' + idmarca;
    return this.urlFetch(url, options);
  };
  getProvincias = () => {
    let url = mecanico.getQueryURL(this.version);
    let options = {};
    url += '/provincias';
    return this.urlFetch(url, options);
  };
  getMantenimiento = () => {
    let url = mecanico.getQueryURL(this.version);
    let options = {};
    url += '/getmantenimiento';
    return this.urlFetch(url, options);
  };
  GetTipos = idmantenimiento => {
    let url = mecanico.getQueryURL(this.version);
    let options = {};
    url += '/gettipos/' + idmantenimiento;
    return this.urlFetch(url, options);
  };
  GetDetalles = idtipoMantenimiento => {
    let url = mecanico.getQueryURL(this.version);
    let options = {};
    url += '/getdetalle/' + idtipoMantenimiento;
    return this.urlFetch(url, options);
  };
  getEmergencias = () => {
    let url = mecanico.getQueryURL(this.version);
    let options = {};
    url += '/emergencias';
    console.log(url);
    return this.urlFetch(url, options);
  };
  getSplash = tipe=> {
    let url = mecanico.getQueryURL(this.version);
    let options = {};
    url += '/getsplash/'+ tipe;
    console.log(url);
    return this.urlFetch(url, options);
  };
  getUserInfo = id=> {
    let url = mecanico.getQueryURL(this.version);
    let options = {};
    url += '/getLocalById/'+ id;
    console.log(url);
    return this.urlFetch(url, options);
  };
  
  getNovedades = () => {
    let url = mecanico.getQueryURL(this.version);
    let options = {};
    url += '/getnovedadesall';
    return this.urlFetch(url, options);
  };

  getTipoNovedades = () => {
    let url = mecanico.getQueryURL(this.version);
    let options = {};
    url += '/tiponovedades';
    return this.urlFetch(url, options);
  };

  getCiudades = provinciaId => {
    let url = mecanico.getQueryURL(this.version);
    let options = {};
    url += '/ciudades/' + provinciaId;
    return this.urlFetch(url, options);
  };

  GetMotosReservas = idlocal => {
    let url = mecanico.getQueryURL(this.version);
    let options = {};
    url += '/reservamotocliente/' + idlocal;
    return this.urlFetch(url, options);
  };
  
  getReservas = idlocal => {
    let url = mecanico.getQueryURL(this.version);
    let options = {};
    url += '/getreservas/' + idlocal;
    return this.urlFetch(url, options);
  };
  getdetallegarantiamoto = idGarantia => {
    let url = mecanico.getQueryURL(this.version);
    let options = {};
    url += '/getdetallegarantiamoto/' + idGarantia;
    return this.urlFetch(url, options);
  };
  getMotoPlaca = placa => {
    let url = mecanico.getQueryURL(this.version);
    let options = {};
    url += '/getmotoplaca/' + placa;
    return this.urlFetch(url, options);
  };


  getMantenimientos = idlocal => {
    let url = mecanico.getQueryURL(this.version);
    let options = {};
    url += '/getreservasMantenimientocliente/' + idlocal;
    return this.urlFetch(url, options);
  };
  getreservasrepuesto = idlocal => {
    let url = mecanico.getQueryURL(this.version);
    let options = {};
    url += '/getreservasrepuesto/' + idlocal;
    return this.urlFetch(url, options);
  };


  getTiposLocal = () => {
    let url = mecanico.getQueryURL(this.version);
    let options = {};
    url += '/tipolocales'
    return this.urlFetch(url, options);
  };
  getLocales = (lat,long) => {
    let url = mecanico.getQueryURL(this.version);
    let options = {};
    url += '/getAlllocales/'+lat+'/'+long
    console.log(url);
    return this.urlFetch(url, options);
  };
  getLocalesOrden = (lat,long,orden) => {
    let url = mecanico.getQueryURL(this.version);
    let options = {};
    url += '/getAlllocalesrepuestos/'+lat+'/'+long+'/'+orden
    return this.urlFetch(url, options);
  };
  getLocalesTipo = (lat,long,idtipo) => {
    let url = mecanico.getQueryURL(this.version);
    let options = {};
    url += '/getAlllocalestipo/'+lat+'/'+long+'/'+idtipo
    return this.urlFetch(url, options);
  };

  getMotoInsignia = (lat,long,idmarca) => {
    let url = mecanico.getQueryURL(this.version);
    let options = {};
    url += '/getlocalinsignia/'+lat+'/'+long+'/'+idmarca
    console.log(url);
    return this.urlFetch(url, options);
  };
  updatetoken = (idlocal,tocken) => {
    let url = mecanico.getQueryURL(this.version);
    let options = {};
    url += '/updatetoken/'+idlocal+'/'+tocken
    return this.urlFetch(url, options);
  };
  


  respuestaCliente = params => {
    let url = mecanico.getQueryURL(this.version);
    let options = {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(params),
    };
    url += '/respuestacliente';
    return this.urlFetch(url, options);
  };

  
  respuestaclienteEstado = params => {
    let url = mecanico.getQueryURL(this.version);
    let options = {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(params),
    };
    url += '/respuestaclienteEstado';
    return this.urlFetch(url, options);
  };

  createReserva = params => {
    let url = mecanico.getQueryURL(this.version);
    let options = {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(params),
    };
    url += '/createreservas';
    return this.urlFetch(url, options);
  };


  comprarRepuesto = params => {
    let url = mecanico.getQueryURL(this.version);
    let options = {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(params),
    };
    url += '/comprarrepuesto';
    return this.urlFetch(url, options);
  };


  getMarcas = () => {
    let url = mecanico.getQueryURL(this.version);
    let options = {};
    url += '/getmarca'
    return this.urlFetch(url, options);
  };

  getReservasUser = (idLocal,idMarca) => {
    let url = mecanico.getQueryURL(this.version);
    let options = {};
    url += '/getreservauser/'+idLocal+'/'+idMarca;
    return this.urlFetch(url, options);
  };



}

