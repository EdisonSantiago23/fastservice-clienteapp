import React from 'react';
import {Text} from 'react-native';

export default class NiftyText extends React.Component {
  render() {
    return (
      <Text
        style={[
          {
            fontFamily: this.props.font
              ? 'CenturyGothic-' + this.props.font
              : 'CenturyGothic',
          },
          this.props.others,
        ]}>
        {this.props.children}
      </Text>
    );
  }
}
