import React from 'react';
import {Image, StyleSheet, View} from 'react-native';
import * as Progress from 'react-native-progress';
import NiftyText from './NiftyText';

export default function LoadingProgress(props) {
  return (
    <View style={styles.containerLoader}>
    <Image
      source={require('../assets/png/lg.png')}
      style={{width: 150, height: 150}}
    />
    <Progress.Bar
      style={{alignSelf: 'center',marginTop:'5%'}}
      indeterminate={true}
      color={'#FF9100'}
      unfilledColor={'#D4D7DD'}
      size={100}
    />
    <NiftyText others={styles.detalle}>Cargando...</NiftyText>
  </View>
  );
}
const styles = StyleSheet.create({
  containerLoader: {
    width: '100%',
    height: '100%',
    alignContent: 'center',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
