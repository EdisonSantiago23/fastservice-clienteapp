import React from 'react';
import {View, Dimensions, StyleSheet} from 'react-native';
import * as Animatable from 'react-native-animatable';

const width = Dimensions.get('window').width;

export default function SplashScreen() {
  return (
    <View
      style={{
        flex: 1,
        alignItems: 'center',
        backgroundColor: '#fff',
        justifyContent: 'center',
      }}>
      <SplashScreenAnimated
        style={{width: '90%'}}
        resizeMode="contain"
        resizeMethod="resize"
      />
    </View>
  );
}

class SplashScreenAnimated extends React.Component {
  handleViewRef = ref => (this.logoExoSkill = ref);
  handleLetter = ref => (this.logoLetra = ref);

  constructor() {
    super();
  }


  render() {
    return (
      <View
        style={{
          width: '100%',
          alignItems: 'center',
          alignContent: 'center',
          justifyContent: 'center',
          // flexDirection: 'row',
        }}>
        <Animatable.Image
          ref={this.handleLetter}
          source={require('./../assets/png/lg2.png')}
          style={[
            styles.animatedImage,
            {width: '45%', backgroundColor: '#fff'},
          ]}
          resizeMode="contain"
          resizeMethod="resize"
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  animatedImage: {
    position: 'absolute',
  },
});
