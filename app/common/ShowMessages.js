import {showMessage} from 'react-native-flash-message';
export function showDangerMsgWarPayph(message, title) {
  showMessage({
    duration: 8000,
    message: title ? title : '',
    description: message,
    backgroundColor: '#FBDC00',
  });
}
export function showSuccesMsg(message, title) {
  showMessage({
    duration: 6000,
    message: title ? title : '',
    description: message,
    backgroundColor:  '#FBDC00',
  });
}
export function showDangerMsg(message, title) {
  showMessage({
    duration: 3000,
    message: title ? title : '',
    description: message,
    backgroundColor: '#ff3345',
  });
}
export function showDangerMsgWar(message, title) {
  showMessage({
    duration: 3000,
    message: title ? title : '',
    description: message,
    backgroundColor: '#FBDC00',
  });

}
