import React from 'react';
import Login from '../Registro/Login';
import RegistroFormulario from '../Registro/RegistroFormulario';
import Terminos from '../Registro/Terminos';
import {createStackNavigator} from '@react-navigation/stack';
import Splash from '../Splash/Splash';

const Stack = createStackNavigator();

export function LoginStack() {
  return (
    <Stack.Navigator
      screenOptions={{
        cardStyle: {
          backgroundColor: '#fff',
        },
      }}>
      <Stack.Screen
        name="Splash"
        component={Splash}
        options={{
          headerShown: false,
        }}
      />
      <Stack.Screen
        name="Login"
        component={Login}
        options={{
          headerShown: false,
        }}
      />
      <Stack.Screen
        name="RegistroFormulario"
        component={RegistroFormulario}
        options={{
          title: 'Registro',
          headerStyle: {
            backgroundColor: '#FF9100',
          },
          headerTintColor: '#fff',
          headerTitleAlign: 'center',
          headerTitleStyle: {
            fontWeight: 'bold',
            textAlign: 'center',
          },
        }}
      />
      <Stack.Screen name="Terminos" component={Terminos} />
    </Stack.Navigator>
  );
}
