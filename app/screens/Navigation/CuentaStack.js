import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import ConfiguracionMenu from '../Configuracion/ConfiguracionMenu';
import Informacion from '../Inicio/Informacion/Informacion';
import Detalle from '../Inicio/Detalle/Detalle';
const Stack = createStackNavigator();
export default function CuentaStack() {
  return (
    <Stack.Navigator
      screenOptions={{
        cardStyle: {backgroundColor: '#fff'},
      }}>
      <Stack.Screen
        name="ConfiguracionMenu"
        component={ConfiguracionMenu}
        options={{
          title: 'Menú',
          headerStyle: {
            backgroundColor: '#FF9100',
          },
          headerTintColor: '#fff',
          headerTitleAlign: 'center',
          headerTitleStyle: {
            fontWeight: 'bold',
            textAlign: 'center',
          },
        }}
      />
      <Stack.Screen
        name="Informacion"
        component={Informacion}
        options={{
          title: 'Informacion',
          headerStyle: {
            backgroundColor: '#FF9100',
          },
          headerTintColor: '#fff',
          headerTitleAlign: 'center',
          headerTitleStyle: {
            fontWeight: 'bold',
            textAlign: 'center',
          },
        }}
      />
      
      <Stack.Screen
        name="Detalle"
        component={Detalle}
        options={{
          title: 'Detalle',
          headerStyle: {
            backgroundColor: '#FF9100',
          },
          headerTintColor: '#fff',
          headerTitleAlign: 'center',
          headerTitleStyle: {
            fontWeight: 'bold',
            textAlign: 'center',
          },
        }}
      />
    </Stack.Navigator>
  );
}
