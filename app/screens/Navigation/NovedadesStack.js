import React from 'react';
import Novedades from '../Novedades/Novedades';
import {createStackNavigator} from '@react-navigation/stack';

const Stack = createStackNavigator();
export default function NovedadesStack() {
  return (
    <Stack.Navigator
      screenOptions={{
        cardStyle: {
          backgroundColor: '#fff',
        },
      }}>
      <Stack.Screen
        name="Novedades"
        component={Novedades}
        options={{
          title: 'Novedades',
          headerStyle: {
            backgroundColor: '#FF9100',
          },
          headerTintColor: '#fff',
          headerTitleAlign: 'center',
          headerTitleStyle: {
            fontWeight: 'bold',
            textAlign: 'center',
          },
        }}
      />
    </Stack.Navigator>
  );
}
