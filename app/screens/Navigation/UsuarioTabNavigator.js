import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {createStackNavigator} from '@react-navigation/stack';
import Entypo from 'react-native-vector-icons/FontAwesome5';
import FastText from '../Common/Components/FastText';

import InicioStack from './InicioStack';
import NovedadesStack from './NovedadesStack';
import Pedidos from './PedidosStack';
import CuentaStack from './CuentaStack';

const Tab = createBottomTabNavigator();
const Stack = createStackNavigator();

export function UsuarioTabNavigator() {
  return (
    <Tab.Navigator>
      <Stack.Screen
        name="InicioStack"
        component={InicioStack}
        options={tabBarIconOption('home', 'Inicio')}
      />
      <Stack.Screen
        name="Pedidos"
        component={Pedidos}
        options={tabBarIconOption('exclamation-triangle', 'Pedidos')}
      />
      <Stack.Screen
        name="NovedadesStack"
        component={NovedadesStack}
        options={tabBarIconOption('newspaper', 'Novedades')}
      />
      <Stack.Screen
        name="Configuracion"
        component={CuentaStack}
        options={tabBarIconOption('cogs', 'Menú')}
      />
    </Tab.Navigator>
  );
}
function tabBarIconOption(icon, name) {
  return {
    tabBarLabel: ({focused}) => {
      return focused ? (
        <FastText
          others={{
            fontSize: 11,
            color: '#FF9100',
            textAlign: 'center',
          }}>
          {name}
        </FastText>
      ) : (
        <FastText
          others={{
            fontSize: 10,
            color: '#9b9b9b',
            textAlign: 'center',
          }}>
          {name}
        </FastText>
      );
    },
    tabBarIcon: ({focused}) =>
      focused ? (
        <Entypo
          disabled={true}
          color={'#FF9100'}
          size={18}
          name={icon}
          style={{position: 'absolute'}}
        />
      ) : (
        <Entypo
          color={'#9b9b9b'}
          size={15}
          name={icon}
          style={{position: 'absolute'}}
        />
      ),
  };
}
