import React from 'react';
import Notificaciones from '../Notificaciones/CotizacionesRepuestos';
import DetalleCotizacionesRepuestos from '../Notificaciones/DetalleCotizacionesRepuestos';
import NotificacionesEmergencias from '../Notificaciones/NotificacionesEmergencias';
import NotificacionesMantenimiento from '../Notificaciones/NotificacionesMantenimiento';
import NotificacionesMenu from '../Notificaciones/NotificacionesMenu';
import NotificacionesMoto from '../Notificaciones/NotificacionesMoto';
import NotificacionesRepuestos from '../Notificaciones/NotificacionesRepuestos';
import {createStackNavigator} from '@react-navigation/stack';

const Stack = createStackNavigator();
export default function PedidosStack() {
  return (
    <Stack.Navigator
      screenOptions={{
        cardStyle: {
          backgroundColor: '#fff',
        },
      }}>
      <Stack.Screen
        name="NotificacionesMenu"
        component={NotificacionesMenu}
        options={{
          title: 'Menú',
          headerStyle: {
            backgroundColor: '#FF9100',
          },
          headerTintColor: '#fff',
          headerTitleAlign: 'center',
          headerTitleStyle: {
            fontWeight: 'bold',
            textAlign: 'center',
          },
        }}
      />
      <Stack.Screen
        name="Notificaciones"
        component={Notificaciones}
        options={{
          title: 'Notificaciones',
          headerStyle: {
            backgroundColor: '#FF9100',
          },
          headerTintColor: '#fff',
          headerTitleAlign: 'center',
          headerTitleStyle: {
            fontWeight: 'bold',
            textAlign: 'center',
          },
        }}
      />
      <Stack.Screen
        name="DetalleCotizacionesRepuestos"
        component={DetalleCotizacionesRepuestos}
        options={{
          title: 'Detalle',
          headerStyle: {
            backgroundColor: '#FF9100',
          },
          headerTintColor: '#fff',
          headerTitleAlign: 'center',
          headerTitleStyle: {
            fontWeight: 'bold',
            textAlign: 'center',
          },
        }}
      />
      <Stack.Screen
        name="NotificacionesEmergencias"
        component={NotificacionesEmergencias}
        options={{
          title: 'Emergencias',
          headerStyle: {
            backgroundColor: '#FF9100',
          },
          headerTintColor: '#fff',
          headerTitleAlign: 'center',
          headerTitleStyle: {
            fontWeight: 'bold',
            textAlign: 'center',
          },
        }}
      />
      <Stack.Screen
        name="NotificacionesMantenimiento"
        component={NotificacionesMantenimiento}
        options={{
          title: 'Mantenimiento',
          headerStyle: {
            backgroundColor: '#FF9100',
          },
          headerTintColor: '#fff',
          headerTitleAlign: 'center',
          headerTitleStyle: {
            fontWeight: 'bold',
            textAlign: 'center',
          },
        }}
      />

      <Stack.Screen
        name="NotificacionesMoto"
        component={NotificacionesMoto}
        options={{
          title: 'Moto',
          headerStyle: {
            backgroundColor: '#FF9100',
          },
          headerTintColor: '#fff',
          headerTitleAlign: 'center',
          headerTitleStyle: {
            fontWeight: 'bold',
            textAlign: 'center',
          },
        }}
      />
      <Stack.Screen
        name="NotificacionesRepuestos"
        component={NotificacionesRepuestos}
        options={{
          title: 'Repuestos',
          headerStyle: {
            backgroundColor: '#FF9100',
          },
          headerTintColor: '#fff',
          headerTitleAlign: 'center',
          headerTitleStyle: {
            fontWeight: 'bold',
            textAlign: 'center',
          },
        }}
      />
    </Stack.Navigator>
  );
}
