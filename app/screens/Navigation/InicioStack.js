import React from 'react';
import {TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

import home from '../Inicio/Home/home';
import Mecanico from '../Inicio/Emergencias/Mecanico';
import MenuEmergencia from '../Inicio/Emergencias/MenuEmergencia';


import FastText from '../Common/Components/FastText';

import MapaRepuestos from '../Inicio/Repuestos/MapaRepuestos';
import MenuRepuestos from '../Inicio/Repuestos/MenuRepuestos';
import Repuesto from '../Inicio/Repuestos/Repuesto';

import ConfirmarMantenimineto from '../Inicio/mantenimiento/ConfirmarMantenimineto';
import MapaMantenimiento from '../Inicio/mantenimiento/MapaMantenimiento';
import MecanicoMantenimiento from '../Inicio/mantenimiento/MecanicoMantenimiento';
import detallemantenimiento from '../Inicio/mantenimiento/detallemantenimiento';
import mantenimiento from '../Inicio/mantenimiento/mantenimiento';
import tiposmantenimiento from '../Inicio/mantenimiento/tiposmantenimiento';

import Cita from '../Inicio/nuevamoto/Cita';
import ConfirmarMoto from '../Inicio/nuevamoto/ConfirmarMoto';
import Garantias from '../Inicio/nuevamoto/Garantias';
import MapaMotos from '../Inicio/nuevamoto/MapaMotos';
import MecanicoMoto from '../Inicio/nuevamoto/MecanicoMoto';
import MenuNuevaMoto from '../Inicio/nuevamoto/MenuNuevaMoto';
import MisCitas from '../Inicio/nuevamoto/MisCitas';
import Moto from '../Inicio/nuevamoto/Moto';
import detallemoto from '../Inicio/nuevamoto/detallemoto';

import Mapa from '../General/Mapa';
import Confirmar from '../General/Confirmar';
import Calificar from '../Pagos/Calificar';
import PayPhone from '../Pagos/PayPhone';
import Pagos from '../Pagos/Pagos';

import {createStackNavigator} from '@react-navigation/stack';

const Stack = createStackNavigator();

export default function InicioStack() {
  return (
    <Stack.Navigator
      screenOptions={{
        cardStyle: {
          backgroundColor: '#fff',
        },
      }}>
      <Stack.Screen
        name="home"
        component={home}
        options={{
          title: 'Servicios',
          headerStyle: {
            backgroundColor: '#FF9100',
          },
          headerTintColor: '#fff',
          headerTitleAlign: 'center',
          headerTitleStyle: {
            fontWeight: 'bold',
            textAlign: 'center',
          },
        }}
      />
      <Stack.Screen
        name="Mapa"
        component={Mapa}
        options={{
          title: 'Mapa',
          headerStyle: {
            backgroundColor: '#FF9100',
          },
          headerTintColor: '#fff',
          headerTitleAlign: 'center',
          headerTitleStyle: {
            fontWeight: 'bold',
            textAlign: 'center',
          },
        }}
      />
      <Stack.Screen
        name="Confirmar"
        component={Confirmar}
        options={{
          title: 'Confirmar',
          headerStyle: {
            backgroundColor: '#FF9100',
          },
          headerTintColor: '#fff',
          headerTitleAlign: 'center',
          headerTitleStyle: {
            fontWeight: 'bold',
            textAlign: 'center',
          },
        }}
      />
      <Stack.Screen
        name="Calificar"
        component={Calificar}
        options={{
          title: 'Calificar',
          headerStyle: {
            backgroundColor: '#FF9100',
          },
          headerTintColor: '#fff',
          headerTitleAlign: 'center',
          headerTitleStyle: {
            fontWeight: 'bold',
            textAlign: 'center',
          },
        }}
      />
      <Stack.Screen
        name="PayPhone"
        component={PayPhone}
        options={{
          title: 'PayPhone',
          headerStyle: {
            backgroundColor: '#FF9100',
          },
          headerTintColor: '#fff',
          headerTitleAlign: 'center',
          headerTitleStyle: {
            fontWeight: 'bold',
            textAlign: 'center',
          },
        }}
      />
      <Stack.Screen
        name="Pagos"
        component={Pagos}
        options={{
          title: 'Pagos',
          headerStyle: {
            backgroundColor: '#FF9100',
          },
          headerTintColor: '#fff',
          headerTitleAlign: 'center',
          headerTitleStyle: {
            fontWeight: 'bold',
            textAlign: 'center',
          },
        }}
      />

      <Stack.Screen
        name="Mecanico"
        component={Mecanico}
        options={{
          title: 'Mecanico',
          headerStyle: {
            backgroundColor: '#FF9100',
          },
          headerTintColor: '#fff',
          headerTitleAlign: 'center',
          headerTitleStyle: {
            fontWeight: 'bold',
            textAlign: 'center',
          },
        }}
      />
      <Stack.Screen
        name="MenuEmergencia"
        component={MenuEmergencia}
        options={{
          title: 'Emergencias',
          headerStyle: {
            backgroundColor: '#FF9100',
          },
          headerTintColor: '#fff',
          headerTitleAlign: 'center',
          headerTitleStyle: {
            fontWeight: 'bold',
            textAlign: 'center',
          },
        }}
      />

      <Stack.Screen
        name="MapaRepuestos"
        component={MapaRepuestos}
        options={{
          title: 'Repuestos',
          headerStyle: {
            backgroundColor: '#FF9100',
          },
          headerTintColor: '#fff',
          headerTitleAlign: 'center',
          headerTitleStyle: {
            fontWeight: 'bold',
            textAlign: 'center',
          },
        }}
      />
      <Stack.Screen
        name="MenuRepuestos"
        component={MenuRepuestos}
        options={{
          title: 'Repuestos',
          headerStyle: {
            backgroundColor: '#FF9100',
          },
          headerTintColor: '#fff',
          headerTitleAlign: 'center',
          headerTitleStyle: {
            fontWeight: 'bold',
            textAlign: 'center',
          },
        }}
      />
      <Stack.Screen
        name="Repuesto"
        component={Repuesto}
        options={{
          title: 'Repuesto',
          headerStyle: {
            backgroundColor: '#FF9100',
          },
          headerTintColor: '#fff',
          headerTitleAlign: 'center',
          headerTitleStyle: {
            fontWeight: 'bold',
            textAlign: 'center',
          },
        }}
      />
      <Stack.Screen
        name="ConfirmarMantenimineto"
        component={ConfirmarMantenimineto}
        options={{
          title: 'Confirmmar',
          headerStyle: {
            backgroundColor: '#FF9100',
          },
          headerTintColor: '#fff',
          headerTitleAlign: 'center',
          headerTitleStyle: {
            fontWeight: 'bold',
            textAlign: 'center',
          },
        }}
      />
      <Stack.Screen
        name="MapaMantenimiento"
        component={MapaMantenimiento}
        options={{
          title: 'Mapa',
          headerStyle: {
            backgroundColor: '#FF9100',
          },
          headerTintColor: '#fff',
          headerTitleAlign: 'center',
          headerTitleStyle: {
            fontWeight: 'bold',
            textAlign: 'center',
          },
        }}
      />

      <Stack.Screen
        name="MecanicoMantenimiento"
        component={MecanicoMantenimiento}
        options={{
          title: 'Mecánico',
          headerStyle: {
            backgroundColor: '#FF9100',
          },
          headerTintColor: '#fff',
          headerTitleAlign: 'center',
          headerTitleStyle: {
            fontWeight: 'bold',
            textAlign: 'center',
          },
        }}
      />
      <Stack.Screen
        name="detallemantenimiento"
        component={detallemantenimiento}
        options={{
          title: 'Detalle',
          headerStyle: {
            backgroundColor: '#FF9100',
          },
          headerTintColor: '#fff',
          headerTitleAlign: 'center',
          headerTitleStyle: {
            fontWeight: 'bold',
            textAlign: 'center',
          },
        }}
      />
      <Stack.Screen
        name="mantenimiento"
        component={mantenimiento}
        options={{
          title: 'Mantenimiento',
          headerStyle: {
            backgroundColor: '#FF9100',
          },
          headerTintColor: '#fff',
          headerTitleAlign: 'center',
          headerTitleStyle: {
            fontWeight: 'bold',
            textAlign: 'center',
          },
        }}
      />
      <Stack.Screen
        name="tiposmantenimiento"
        component={tiposmantenimiento}
        options={{
          title: 'Tipos',
          headerStyle: {
            backgroundColor: '#FF9100',
          },
          headerTintColor: '#fff',
          headerTitleAlign: 'center',
          headerTitleStyle: {
            fontWeight: 'bold',
            textAlign: 'center',
          },
        }}
      />
      <Stack.Screen
        name="Cita"
        component={Cita}
        options={{
          title: 'Cita',
          headerStyle: {
            backgroundColor: '#FF9100',
          },
          headerTintColor: '#fff',
          headerTitleAlign: 'center',
          headerTitleStyle: {
            fontWeight: 'bold',
            textAlign: 'center',
          },
        }}
      />

      <Stack.Screen
        name="ConfirmarMoto"
        component={ConfirmarMoto}
        options={{
          title: 'Confirmar',
          headerStyle: {
            backgroundColor: '#FF9100',
          },
          headerTintColor: '#fff',
          headerTitleAlign: 'center',
          headerTitleStyle: {
            fontWeight: 'bold',
            textAlign: 'center',
          },
        }}
      />
      <Stack.Screen
        name="Garantias"
        component={Garantias}
        options={{
          title: 'Garantias',
          headerStyle: {
            backgroundColor: '#FF9100',
          },
          headerTintColor: '#fff',
          headerTitleAlign: 'center',
          headerTitleStyle: {
            fontWeight: 'bold',
            textAlign: 'center',
          },
        }}
      />
      <Stack.Screen
        name="MapaMotos"
        component={MapaMotos}
        options={{
          title: 'Mapa',
          headerStyle: {
            backgroundColor: '#FF9100',
          },
          headerTintColor: '#fff',
          headerTitleAlign: 'center',
          headerTitleStyle: {
            fontWeight: 'bold',
            textAlign: 'center',
          },
        }}
      />
      <Stack.Screen
        name="MecanicoMoto"
        component={MecanicoMoto}
        options={{
          title: 'Mecánico',
          headerStyle: {
            backgroundColor: '#FF9100',
          },
          headerTintColor: '#fff',
          headerTitleAlign: 'center',
          headerTitleStyle: {
            fontWeight: 'bold',
            textAlign: 'center',
          },
        }}
      />
      <Stack.Screen
        name="MenuNuevaMoto"
        component={MenuNuevaMoto}
        options={{
          title: 'Moto',
          headerStyle: {
            backgroundColor: '#FF9100',
          },
          headerTintColor: '#fff',
          headerTitleAlign: 'center',
          headerTitleStyle: {
            fontWeight: 'bold',
            textAlign: 'center',
          },
        }}
      />

      <Stack.Screen
        name="MisCitas"
        component={MisCitas}
        options={{
          title: 'Citas',
          headerStyle: {
            backgroundColor: '#FF9100',
          },
          headerTintColor: '#fff',
          headerTitleAlign: 'center',
          headerTitleStyle: {
            fontWeight: 'bold',
            textAlign: 'center',
          },
        }}
      />
      <Stack.Screen
        name="Moto"
        component={Moto}
        options={{
          title: 'Moto',
          headerStyle: {
            backgroundColor: '#FF9100',
          },
          headerTintColor: '#fff',
          headerTitleAlign: 'center',
          headerTitleStyle: {
            fontWeight: 'bold',
            textAlign: 'center',
          },
        }}
      />
      <Stack.Screen
        name="detallemoto"
        component={detallemoto}
        options={{
          title: 'Detalle',
          headerStyle: {
            backgroundColor: '#FF9100',
          },
          headerTintColor: '#fff',
          headerTitleAlign: 'center',
          headerTitleStyle: {
            fontWeight: 'bold',
            textAlign: 'center',
          },
        }}
      />
    </Stack.Navigator>
  );
}
