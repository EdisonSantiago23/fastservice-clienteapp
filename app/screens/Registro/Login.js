import React, {Component} from 'react';
import {
  Text,
  StyleSheet,
  TextInput,
  View,
  TouchableOpacity,
  ImageBackground,
  StatusBar,
} from 'react-native';
import mecanicoApi from '../../api/mecanico.js';
import LoadingProgress from '../../common/LoadingProgress';
import AsyncStorage from '@react-native-community/async-storage';
import Constants from '../../common/Constants';
import * as Animatable from 'react-native-animatable';
import {AuthContext} from '../../../app/api/AuthContext';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Feather from 'react-native-vector-icons/Feather';
import LinearGradient from 'react-native-linear-gradient';
import {showDangerMsg, showSuccesMsg} from '../../common/ShowMessages';

export default class Login extends Component {
  static contextType = AuthContext;

  constructor(props) {
    super(props);
    this.mecanicoApi = new mecanicoApi();

    this.state = {
      loading: false,
      userFeatures: {
        correo: '',
        password: '',
        check_textInputChange: false,
        secureTextEntry: true,
        isValidUser: true,
        isValidPassword: true,
      },
    };
    this.loginvalid();
  }
  loginvalid = async () => {
    if (await AsyncStorage.getItem(Constants.STORE_ID_USER)) {
    }
  };
  loguin() {
    this.setState({loading: true});

    try {
      var params = {
        correo: this.state.userFeatures.correo,
        password: this.state.userFeatures.password,
        tipo: 0,
      };

      this.mecanicoApi.loginUser(params).then(res => {
        console.log(res);
        if (!res.status) {
          AsyncStorage.setItem(
            Constants.STORE_ID_USER,
            JSON.stringify(res.idLocal),
          );
          AsyncStorage.setItem(
            Constants.STORE_ID_TOKEN,
            JSON.stringify(res.tocken),
          );
          AsyncStorage.setItem(
            Constants.STORE_NAME_USER,
            JSON.stringify(res.nombre),
          );
          AsyncStorage.setItem(Constants.STORE_USER, JSON.stringify(res));

          this.setState({loading: false});

          let {signIn} = this.context;
          signIn(res);
        } else {
          if (res.data.error) {
            showDangerMsg('Usuario no registrado.','Autenticación',);

            this.setState({loading: false});
          } else {
            this.setState({loading: false});
            showDangerMsg('Vuelve a iniciar sesión.','rror inesperado',);

          }
        }
      });
    } catch (err) {
      this.setState({loading: false});
      showDangerMsg('Vuelve a iniciar sesión.','rror inesperado',);
    }
  }

  updateStatusFeature(param, value) {
    let userFeatConst = this.state.userFeatures;
    userFeatConst[param] = value;
    this.setState({userFeatures: userFeatConst});
    if (value.trim().length >= 4) {
      this.state.userFeatures.check_textInputChange = true;
      this.state.userFeatures.isValidUser = true;
    } else {
      this.state.userFeatures.check_textInputChange = false;
      this.state.userFeatures.isValidUser = false;
    }
  }
  contrasena(param, value) {
    let userFeatConst = this.state.userFeatures;
    userFeatConst[param] = value;
    this.setState({userFeatures: userFeatConst});
    if (value.trim().length >= 8) {
      this.state.userFeatures.isValidPassword = true;
    } else {
      this.state.userFeatures.isValidPassword = false;
    }
  }
  updateSecureTextEntry(param, value) {
    let userFeatConst = this.state.userFeatures;
    userFeatConst[param] = value;
    this.setState({userFeatures: userFeatConst});
  }
  handleValidUser(val) {
    if (val.trim().length >= 4) {
      this.state.userFeatures.isValidUser = true;
    } else {
      this.state.userFeatures.isValidUser = false;
    }
  }

  render() {
    if (this.state.loading) {
      return <LoadingProgress />;
    }
    if (!this.state.loading) {
      return (
        <View style={styles.container}>
          <StatusBar backgroundColor="#FF9100" barStyle="light-content" />
          <View style={styles.header}>
            <View
              style={{
                alignItems: 'center',
                width: '100%',
                alignContent: 'center',
              }}>
              <ImageBackground
                source={require('../../assets/png/lg.png')}
                style={{
                  width: 75,
                  height: 75,
                  marginTop: '20%',
                  borderWidth: 0,
                }}
              />

              <Text
                style={{
                  fontSize: 20,
                  color: '#617792',
                  fontWeight: 'bold',
                  textAlign: 'center',
                }}>
                MECÁNICA EXPRESS
              </Text>
              <Text
                style={{
                  fontSize: 20,
                  color: '#617792',
                  fontWeight: 'bold',
                  textAlign: 'center',
                }}>
                Bienvenido
              </Text>
            </View>
          </View>
          <Animatable.View animation="fadeInUpBig" style={[styles.footer, {}]}>
            <Text style={[styles.text_footer, {}]}>Correo</Text>

            <View style={styles.action}>
              <FontAwesome name="user-o" size={20} />
              <TextInput
                placeholder="Tu Correo"
                placeholderTextColor="#666666"
                style={[styles.textInput, {}]}
                autoCapitalize="none"
                onChangeText={val => this.updateStatusFeature('correo', val)}
                onEndEditing={e => this.handleValidUser(e.nativeEvent.text)}
              />
              {this.state.userFeatures.check_textInputChange ? (
                <Animatable.View animation="bounceIn">
                  <Feather name="check-circle" color="green" size={20} />
                </Animatable.View>
              ) : null}
            </View>

            {this.state.userFeatures.isValidUser ? null : (
              <Animatable.View animation="fadeInLeft" duration={500}>
                <Text style={styles.errorMsg}>Ingresa un correo válido.</Text>
              </Animatable.View>
            )}

            <Text
              style={[
                styles.text_footer,
                {
                  marginTop: 35,
                },
              ]}>
              Contraseña
            </Text>

            <View style={styles.action}>
              <Feather name="lock" size={20} />
              <TextInput
                placeholder="Tu Contraseña"
                placeholderTextColor="#666666"
                secureTextEntry={
                  this.state.userFeatures.secureTextEntry ? true : false
                }
                style={[styles.textInput]}
                autoCapitalize="none"
                onChangeText={val => this.contrasena('password', val)}
              />
              <TouchableOpacity
                onPress={() =>
                  this.updateSecureTextEntry(
                    'secureTextEntry',
                    !this.state.userFeatures.secureTextEntry,
                  )
                }>
                {this.state.userFeatures.secureTextEntry ? (
                  <Feather name="eye-off" color="grey" size={20} />
                ) : (
                  <Feather name="eye" color="grey" size={20} />
                )}
              </TouchableOpacity>
            </View>
            {this.state.userFeatures.isValidPassword ? null : (
              <Animatable.View animation="fadeInLeft" duration={500}>
                <Text style={styles.errorMsg}>
                  Contraseña minimo 6 caracteres
                </Text>
              </Animatable.View>
            )}
            <View style={styles.button}>
              <TouchableOpacity
                style={styles.signIn}
                onPress={() => {
                  this.loguin();
                }}>
                <LinearGradient
                  colors={['#FF9100', '#FFBF6D']}
                  style={styles.signIn}>
                  <Text
                    style={[
                      styles.textSign,
                      {
                        color: '#fff',
                      },
                    ]}>
                    Ingresar
                  </Text>
                </LinearGradient>
              </TouchableOpacity>

              <TouchableOpacity
                onPress={() =>
                  this.props.navigation.navigate('RegistroFormulario')
                }
                style={[
                  styles.signIn,
                  {
                    borderColor: '#617792',
                    borderWidth: 1,
                    marginTop: 15,
                  },
                ]}>
                <Text
                  style={[
                    styles.textSign,
                    {
                      color: '#617792',
                    },
                  ]}>
                  Registrar
                </Text>
              </TouchableOpacity>
            </View>
          </Animatable.View>
        </View>
      );
    }
  }
}

const styles = StyleSheet.create({
  btnVerEnSitio: {
    backgroundColor: '#F5F6F7',
    borderRadius: 20,
    alignItems: 'center',
    width: '70%',
    padding: '3%',
    color: 'blue',
    borderWidth: 1,
    marginTop: '5%',
    borderColor: '#4982e6',
  },
  action: {
    flexDirection: 'row',
    marginTop: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#f2f2f2',
    paddingBottom: 5,
  },

  inputSearch: {
    borderBottomWidth: 1,
    width: '90%',
  },
  imagen: {
    width: 100,
    height: 100,
  },
  container: {
    flex: 1,
    backgroundColor: '#EAE1D3',
  },
  header: {
    flex: 1,
    justifyContent: 'flex-end',
    paddingHorizontal: 20,
  },
  footer: {
    flex: 3,
    backgroundColor: '#fff',
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    paddingHorizontal: 20,
    paddingVertical: 30,
  },
  text_header: {
    color: '#fff',
    fontWeight: 'bold',
    fontSize: 30,
  },
  text_footer: {
    color: '#05375a',
    fontSize: 18,
  },
  action: {
    flexDirection: 'row',
    marginTop: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#f2f2f2',
    paddingBottom: 5,
  },
  actionError: {
    flexDirection: 'row',
    marginTop: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#FF0000',
    paddingBottom: 5,
  },
  textInput: {
    flex: 1,
    marginTop: Platform.OS === 'ios' ? 0 : -12,
    paddingLeft: 10,
    color: '#05375a',
  },
  errorMsg: {
    color: '#FF0000',
    fontSize: 14,
  },
  button: {
    alignItems: 'center',
    marginTop: 50,
  },
  signIn: {
    width: '100%',
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
  },
  textSign: {
    fontSize: 18,
    fontWeight: 'bold',
  },
});
