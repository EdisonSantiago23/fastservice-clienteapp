import React, {Component} from 'react';
import {
  Text,
  Image,
  ScrollView,
  StyleSheet,
  View,
  TouchableOpacity,
  StatusBar,
  ActivityIndicator,
} from 'react-native';
import mecanicoApi from '../../api/mecanico.js';
import LoadingProgress from '../../common/LoadingProgress';
import AsyncStorage from '@react-native-community/async-storage';
import FastImput from '../Common/Components/FastImput';
import PhoneInput from 'react-native-phone-number-input';
import {showDangerMsg, showSuccesMsg} from '../../common/ShowMessages';
import Constants from '../../common/Constants';
import {SCLAlert, SCLAlertButton} from 'react-native-scl-alert';

export default class RegistroFormulario extends Component {
  constructor(props) {
    super(props);

    this.mecanicoApi = new mecanicoApi();
    this.state = {
      provincias: [],
      ciudades: [],
      tocken: '',

      setPhone: '',
      userId: '',
      numerocelular: '',
      loadingBtn: false,
      show: false,
      phone: '',
      invalidPhone: false,
      imageFileData: '',
      checked: false,
      loading: true,
      tiposlocal: [],
      userFeatures: {
        idLocal: 0,
        nombre: '',
        calificacion: 1,
        latitud: 0,
        longitud: 0,
        direccion: '',
        ciudad_idCiudad: '',
        representante: '',
        telefono: '',
        correo: '',
        estado: 0,
        password: '',
        cedula: '',
        url: '',
        localtype: '',
        tocken: '',
      },
    };
    this.getProvincia();
    this.getTiposLocal();
    AsyncStorage.removeItem(Constants.STORE_ID_USER);
    AsyncStorage.removeItem(Constants.STORE_ID_TOKEN);
  }

  onChangeText = (key, val) => {
    this.setState({[key]: val});
  };

  updateStatusFeature(param, value) {
    let userFeatConst = this.state.userFeatures;
    userFeatConst[param] = value;
    this.setState({userFeatures: userFeatConst});
    console.log(this.state.userFeatures);
  }

  validador() {
    if (
      this.validateEmail(this.state.userFeatures.correo) &&
      this.validarContrasena() &&
      this.validateCedula(this.state.userFeatures.cedula) &&
      this.validadorCampo() && this.validadorcelular()
    ) {
      return true;
    } else {
      return false;
    }
  }
 
  validadorcelular() {
    if (
      this.state.invalidPhone == true
    ) {
      return true;
    } else {

      showDangerMsg('Ingrese un celular válido','Campos erróneos');

      return false;
    }
  }
  validadorCampo() {
    if (
      this.state.userFeatures.nombre != '' &&
      this.state.userFeatures.password != '' &&
      this.state.userFeatures.telefono != ''
    ) {
      return true;
    } else {
      showDangerMsg('No debe existir campos vacios','Campos erróneos');
      return false;
    }
  }
  validarContrasena() {
    if (this.state.userFeatures.password.length >= 6) {
      return true;
    } else {

      showDangerMsg('La contraseña debe tener mínimo 6 caracteres','Campos erróneos');

      return false;
    }
  }
  handleOpen = () => {
    this.setState({show: true});
  };

  handleClose = () => {
    this.setState({show: false});
    this.props.navigation.navigate('Login')
  };
  createLocal = async () => {
    try {
      if (this.validador()) {
        this.setState({loadingBtn: true});
        this.state.userFeatures.url = '//';
        this.state.userFeatures.localtype = 3;
        this.state.userFeatures.tocken = 0;
        this.state.userFeatures.latitud = 0;
        this.state.userFeatures.longitud = 0;
        this.mecanicoApi.createLocal(this.state.userFeatures).then(res => {
          if (res.idLocal) {
            AsyncStorage.setItem(
              Constants.STORE_ID_USER,
              JSON.stringify(res.idLocal),
            );
            AsyncStorage.setItem(
              Constants.STORE_ID_TOKEN,
              JSON.stringify(res.tocken),
            );
            AsyncStorage.setItem(
              Constants.STORE_NAME_USER,
              JSON.stringify(res.nombre),
            );

            this.setState({loadingBtn: false, show: true});
          } else {
            if (res.status && res.status == 'error') {
              showDangerMsg( res.message,'Error');

              this.setState({loadingBtn: false});
            }
          }
        });
      }
    } catch (err) {
      showDangerMsg( err.message,'Error');
    }
  };

  validateEmail = email => {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (!re.test(email)) {
 
      showDangerMsg('Ingresa un correo válido','Correo Erróneo');

    }
    return re.test(email);
  };
  validateCedula = cedula => {
    var cedula = cedula;
    //Preguntamos si la cedula consta de 10 digitos
    if (cedula.length == 10) {
      //Obtenemos el digito de la region que sonlos dos primeros digitos
      var digito_region = cedula.substring(0, 2);

      //Pregunto si la region existe ecuador se divide en 24 regiones
      if (digito_region >= 1 && digito_region <= 24) {
        // Extraigo el ultimo digito
        var ultimo_digito = cedula.substring(9, 10);

        //Agrupo todos los pares y los sumo
        var pares =
          parseInt(cedula.substring(1, 2)) +
          parseInt(cedula.substring(3, 4)) +
          parseInt(cedula.substring(5, 6)) +
          parseInt(cedula.substring(7, 8));

        //Agrupo los impares, los multiplico por un factor de 2, si la resultante es > que 9 le restamos el 9 a la resultante
        var numero1 = cedula.substring(0, 1);
        var numero1 = numero1 * 2;
        if (numero1 > 9) {
          var numero1 = numero1 - 9;
        }

        var numero3 = cedula.substring(2, 3);
        var numero3 = numero3 * 2;
        if (numero3 > 9) {
          var numero3 = numero3 - 9;
        }

        var numero5 = cedula.substring(4, 5);
        var numero5 = numero5 * 2;
        if (numero5 > 9) {
          var numero5 = numero5 - 9;
        }

        var numero7 = cedula.substring(6, 7);
        var numero7 = numero7 * 2;
        if (numero7 > 9) {
          var numero7 = numero7 - 9;
        }

        var numero9 = cedula.substring(8, 9);
        var numero9 = numero9 * 2;
        if (numero9 > 9) {
          var numero9 = numero9 - 9;
        }

        var impares = numero1 + numero3 + numero5 + numero7 + numero9;

        //Suma total
        var suma_total = pares + impares;

        //extraemos el primero digito
        var primer_digito_suma = String(suma_total).substring(0, 1);

        //Obtenemos la decena inmediata
        var decena = (parseInt(primer_digito_suma) + 1) * 10;

        //Obtenemos la resta de la decena inmediata - la suma_total esto nos da el digito validador
        var digito_validador = decena - suma_total;

        //Si el digito validador es = a 10 toma el valor de 0
        if (digito_validador == 10) var digito_validador = 0;

        //Validamos que el digito validador sea igual al de la cedula
        if (digito_validador == ultimo_digito) {
          return true;
        } else {
     
          showDangerMsg('la cedula:' + cedula + ' es incorrecta','Cédula Errónea');

          return false;
        }
      } else {

        showDangerMsg('Esta cedula no pertenece a ninguna region','Cédula Errónea');

        return false;
      }
    } else {

      showDangerMsg('Esta cedula tiene menos de 10 Digitos','Cédula Errónea');

      return false;
    }
  };
  render() {
    if (this.state.loading) {
      return <LoadingProgress />;
    }
    if (!this.state.loading) {
      return (
        <ScrollView>
          <SCLAlert
            theme="success"
            show={this.state.show}
            title="Correcto.!"
            onRequestClose={this.handleClose}
            cancellable={true}
            subtitle="Registro exitoso">
            <SCLAlertButton theme="success" onPress={this.handleClose}>
              Ok
            </SCLAlertButton>
          </SCLAlert>
      
          <StatusBar backgroundColor="#FF9100" barStyle="light-content" />

          <View style={styles.container}>
            <View style={{alignItems: 'center', textAlign: 'center'}}>
              <Image
                source={require('../../assets/png/lg.png')}
                style={styles.imagen}
              />
              <View>
                <Text
                  style={{
                    fontSize: 15,
                    color: '#617792',
                    fontWeight: 'bold',
                    textAlign: 'center',
                  }}>
                  {' '}
                  MECÁNICA EXPRESS
                </Text>
              </View>
            </View>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                marginLeft: '30%',
              }}
            />

            <View style={styles.ellanoteama}>
            <Text style={{ fontSize: 15}}>
                  Celular
                </Text>
              <PhoneInput
                style={styles.phoneInput}
                ref={ref => (this.state.phone = ref)}
                onPressFlag={this.onPressFlag}
                defaultCode={'EC'}
                allowZeroAfterCountryCode={false}
                placeholder={'0987932320'}
                layout="first"
                onChangeFormattedText={text => {
                  this.state.invalidPhone = this.state.phone.isValidNumber(
                    text,
                  );
                  this.updateStatusFeature('telefono', text);
                }}
                codeTextStyle={{marginLeft: -20}}
                textContainerStyle={{backgroundColor: '#FFFFFF'}}
              />
              {this.state.invalidPhone == false && (
                <Text style={{color: 'red', fontSize: 12}}>
                  Número incorrecto
                </Text>
              )}
              <FastImput
                label={'Nombre'}
                multiline={true}
                value={this.state.userFeatures?.nombre}
                max={150}
                onChangeText={value =>
                  this.updateStatusFeature('nombre', value)
                }
              />
              <FastImput
                label={'Cédula'}
                value={this.state.userFeatures?.cedula}
                maxLength={10}
                multiline={true}
                keyboardType={'phone-pad'}
                onChangeText={value =>
                  this.updateStatusFeature('cedula', value)
                }
              />
              <FastImput
                label={'Contraseña'}
                secureTextEntry={true}
                multiline={true}
                value={this.state.userFeatures?.password}
                maxLength={10}
                onChangeText={value =>
                  this.updateStatusFeature('password', value)
                }
              />
              <FastImput
                label={'Correo'}
                multiline={true}
                value={this.state.userFeatures?.correo}
                onChangeText={value =>
                  this.updateStatusFeature('correo', value.toLowerCase().trim())
                }
              />

              {this.renderBtn()}
            </View>
          </View>
        </ScrollView>
      );
    }
  }
  onPressFlag() {
    setVisible(true);
  }
  renderBtn() {
    if (!this.state.loadingBtn) {
      return (
        <View>
          <TouchableOpacity
            onPress={() => {
              this.createLocal();
            }}
            style={styles.btnStyle}>
            <Text style={{fontSize: 14, textAlign: 'center', color: '#1F4997'}}>
              GUARDAR
            </Text>
          </TouchableOpacity>
        </View>
      );
    } else {
      return (
        <TouchableOpacity style={styles.btnStyle}>
          <ActivityIndicator size="small" color={'#1F4997'} />
        </TouchableOpacity>
      );
    }
  }
  getTiposLocal() {
    this.mecanicoApi.getTiposLocal().then(res => {
      this.setState({
        tiposlocal: res,
      });
      if (res != undefined) {
        this.setState({
          loading: false,
        });
      }
    });
  }
  getProvincia() {
    this.mecanicoApi.getProvincias().then(res => {
      this.setState({
        provincias: res,
      });
      if (res != undefined) {
        this.setState({
          loading: false,
        });
      }
    });
  }
  getCiudades(provinciaId) {
    this.mecanicoApi.getCiudades(provinciaId).then(res => {
      if (res.ciudades != undefined) {
        this.setState({
          ciudades: res.ciudades,
        });
      } else {
        this.setState({
          ciudades: [],
        });
      }
    });
  }
}

const rnpickerStyles = StyleSheet.create({
  labelStyle: {textAlign: 'left', minWidth: 60},
  labelStyle2: {textAlign: 'center', minWidth: 60},
  pickerContainer: {
    borderBottomWidth: 1,
    borderBottomColor: '#FFFF',
    flex: 1,
  },
});

const styles = StyleSheet.create({
  btnVerEnSitio: {
    backgroundColor: '#F5F6F7',
    borderRadius: 20,
    alignItems: 'center',
    width: '70%',
    padding: '3%',
    color: 'blue',
    borderWidth: 1,
    marginTop: '5%',
    borderColor: '#4982e6',
  },
  phoneInput: {
    width: '100%',
    height: 50,
    borderBottomWidth: 0.8,
    borderBottomColor: '#ccc',
  },
  map: {
    width: 180,
    height: 180,
  },
  btnVerEnSitio3: {
    backgroundColor: '#F5F6F7',
    borderRadius: 20,
    alignItems: 'center',
    width: '90%',
    padding: '3%',
    color: 'blue',
    borderWidth: 1,
    marginTop: '5%',
    borderColor: '#4982e6',
  },
  btnVerEnSitio2: {
    backgroundColor: '#F5F6F7',
    borderRadius: 20,
    alignItems: 'center',
    width: '70%',
    padding: '3%',
    color: 'blue',
    borderWidth: 1,
    marginTop: '-10%',
    borderColor: '#4982e6',
  },
  descriptionTag: {
    color: '#502A18',
  },
  btnTextStyle: {
    color: '#FFFFFF',
    fontSize: 22,
    alignItems: 'center',
    textAlign: 'center',
  },
  btnStyle: {
    alignItems: 'center',
    borderWidth: 1,
    borderColor: '#4982e6',
    borderRadius: 20,
    justifyContent: 'center',
    backgroundColor: '#F5F6F7',
    height: 50,
    marginTop: 20,
    marginBottom: 80,
  },
  images: {
    width: 150,
    height: 150,
    borderColor: 'black',
    borderWidth: 1,
  },
  fotoContainer: {
    borderColor: '#424244',
    borderWidth: 1,
    borderStyle: 'dotted',
    backgroundColor: '#F9FCFD',
  },
  container: {
    flex: 1,
    alignItems: 'center',
    padding: 15,
    backgroundColor: '#fff',
  },
  ellanoteama: {
    minWidth: '100%',
    height: '100%',
    backgroundColor: '#fff',
  },
  createAccount: {
    fontSize: 30,
    textAlign: 'left',
    marginVertical: 15,
  },
  inputSearch: {
    borderBottomWidth: 1,
  },
  imagen: {
    width: 100,
    height: 100,
    marginLeft: '15%',
  },
  imagen2: {
    width: 155,
    height: 130,
  },
});
