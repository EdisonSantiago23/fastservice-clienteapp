import React, {Component} from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  StatusBar,
  View,
  Image,
  FlatList,
  Text,
  Dimensions,
} from 'react-native';
import mecanicoApi from '../../api/mecanico.js';
import Icon from 'react-native-vector-icons/FontAwesome';
import Geolocation from '@react-native-community/geolocation';
import MapView, {Marker} from 'react-native-maps';
import frelance from '../../assets/free.png';
import local from '../../assets/car.png';
var {height, width} = Dimensions.get('window');
import Constants from '../../common/Constants';
import {SCLAlert, SCLAlertButton} from 'react-native-scl-alert';
const CARD_HEIGHT = height / 3.5;
const CARD_WIDTH = CARD_HEIGHT - 50;
import LoadingProgress from '../../common/LoadingProgress';
export default class Mapa extends Component {
  constructor(props) {
    super(props);
    this.mecanicoApi = new mecanicoApi();

    this.state = {
      isVisible: false,
      locales: [],
      loading: true,
      emergencia: this.props.route.params.emergencia,
      show: false,

      poi: {
        latitude: '',
        longitude: '',
      },

      region: {
        latitude: 0,
        longitude: 0,
        latitudeDelta: 0,
        longitudeDelta: 0.04,
      },
      markers: [],
    };
    this.getLocationUser();
  }
  getLocationUser = async () => {
    await Geolocation.getCurrentPosition(
      position => {
        const {latitude, longitude} = position.coords;
        this.state.poi.latitude = position.coords.latitude;
        this.state.poi.longitude = position.coords.longitude;
        this.setState({
          region: {
            longitude: longitude,
            latitude: latitude,
            latitudeDelta: 0.029213524352655895,
            longitudeDelta: (width / height) * 0.029213524352655895,
          },
        });
        this.setState({loading: false});
        this.getLocales(latitude, longitude);
      },
      error => {
        console.log(error);
      },
    );
  };

  getLocales(lat, long) {
    this.mecanicoApi.getLocales(lat, long).then(res => {
      var aux = Object.values(res).sort(function(a, b) {
        return a.distancia - b.distancia;
      });
      console.log(aux);

      if (res.code===404) {
        this.setState({show: true, loading: false});
      } else {
        this.setState({
          locales: aux,
        });
      }
    });
  }
  mecanico(local) {
    if (local.localtype == 1) {
      this.props.navigation.navigate('Confirmar', {
        mecanico: local,
        emergencia: this.state.emergencia,
      });
    } else {
      this.props.navigation.navigate('Mecanico', {
        local: local,
        emergencia: this.state.emergencia,
      });
    }
  }
  _renderItem(item) {
    return (
      <View style={styles.card}>
        <Text style={{fontSize: 15, color: '#000000', textAlign: 'center'}}>
          {item.nombre}
        </Text>

        <View
          style={{
            width: '100%',
            flexDirection: 'row',
            justifyContent: 'space-between',
          }}>
          <Text style={{fontSize: 15, color: '#000000'}}>Dirección:</Text>
          <Text style={{fontSize: 15, color: '#000000'}}>{item.direccion}</Text>
        </View>
        <View
          style={{
            width: '100%',
            flexDirection: 'row',
            justifyContent: 'space-between',
          }}>
          <Text style={{fontSize: 15, color: '#000000'}}>Tiempo:</Text>
          <Text style={{color: '#FF9100'}}>{item.tiempo}</Text>
        </View>
        <View style={{width: '100%', height: '100%', flexDirection: 'row'}}>
          <Image
            source={{uri: Constants.BASEURI + item.imagen}}
            style={styles.cardImage}
            resizeMode="cover"
          />
          <TouchableOpacity
            style={styles.buy}
            onPress={() => this.mecanico(item)}>
            <Icon
              name="arrow-right"
              type="Ionicons"
              size={30}
              color="#FF9100"
            />
          </TouchableOpacity>
        </View>
      </View>
    );
  }
  renderParkings() {
    return (
      <FlatList
        horizontal
        scrollEnabled
        centerContent
        showsHorizontalScrollIndicator={false}
        style={[styles.parkings, styles.shadow]}
        data={this.state.locales}
        keyExtractor={item => item.id.toString()}
        renderItem={({item}) => this._renderItem(item)}
      />
    );
  }
  handleClose = () => {
    this.setState({show: false});
  };
  render() {
    if (this.state.loading) {
      return <LoadingProgress />;
    }
    if (!this.state.loading) {
      return (
        <View style={{height: '100%'}}>
          <SCLAlert
            theme="danger"
            show={this.state.show}
            title="Lo sentimos"
            onRequestClose={this.handleClose}
            cancellable={true}
            subtitle="No existen locales por mostrar">
            <SCLAlertButton theme="danger" onPress={this.handleClose}>
              Atras
            </SCLAlertButton>
          </SCLAlert>
        
          <StatusBar backgroundColor="#FF9100" barStyle="light-content" />

          <View style={{width: '100%', height: '100%'}}>
            <MapView
              style={styles.map}
              mapType={'standard'}
              initialCamera={{
                center: {
                  latitude: this.state.region.latitude,
                  longitude: this.state.region.longitude,
                },
                pitch: 45,
                heading: 90,
                altitude: 1000,
                zoom: 17,
              }}
              loadingEnabled
              loadingIndicatorColor="#666666"
              loadingBackgroundColor="#eeeeee"
              showsIndoors
              showsIndoorLevelPicker
              zoomEnabled={true}
              pitchEnabled={true}
              showsUserLocation={true}
              followsUserLocation={true}
              showsCompass={true}
              showsBuildings={true}
              showsTraffic={true}
              showsIndoors={true}
              initialRegion={this.state.region}
              zoomEnabled={true}>
              {this.state.locales.map(marker => (
                <Marker
                  title={marker.nombre}
                  image={marker.localtype == 0 ? local : frelance}
                  coordinate={marker.coordinate[0]}
                />
              ))}
            </MapView>
          </View>
          {this.renderParkings()}
        </View>
      );
    }
  }
}

const styles = StyleSheet.create({
  textContent: {
    flex: 1,
  },
  buy: {
    flexDirection: 'row',
    borderRadius: 9,
    height: '90%',
    marginLeft: '5%',
    alignContent: 'center',
    alignSelf: 'center',
  },
  shadow: {
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 12,
    },
    shadowOpacity: 0.58,
    shadowRadius: 16.0,
    elevation: 24,
  },
  parking: {
    flexDirection: 'row',
    backgroundColor: 'white',
    borderRadius: 6,
    padding: 12,
    marginHorizontal: 12,
    width: width - 12 * 2,
  },
  parkings: {
    position: 'absolute',
    right: 0,
    left: 0,
    bottom: 0,
    paddingBottom: 24,
  },
  cardTitle: {
    fontSize: 13,
    marginTop: 5,
    fontWeight: 'bold',
  },
  cardDescription: {
    fontSize: 12,
    color: '#444',
  },
  card: {
    backgroundColor: 'white',
    borderRadius: 6,
    padding: 12,
    marginHorizontal: 12,
    width: width - 50 * 2,
    height: CARD_HEIGHT,
  },
  scrollView: {
    position: 'absolute',
    bottom: 20,
    left: 0,
    right: 0,
    paddingVertical: 10,
  },
  carouselStyle: {
    padding: 15,
  },
  contImage: {
    marginTop: 3,
    justifyContent: 'center',
    borderWidth: 2,
    width: '100%',
    borderColor: '#4982e6',
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
  },
  endPadding: {
    paddingRight: width - CARD_WIDTH,
  },
  title: {
    fontFamily: 'vagron',
    fontSize: 20,
    color: '#1F4997',
    backgroundColor: 'transparent',
    textAlign: 'center',
    marginVertical: 5,
  },
  text: {
    color: '#73221C',
    backgroundColor: 'transparent',
    textAlign: 'center',
    fontSize: 15,
    paddingHorizontal: 10,
    marginBottom: 10,
  },
  map: {
    width: '100%',
    height: '100%',
  },
  carousel: {
    position: 'absolute',
    bottom: 0,
    marginBottom: 48,
  },
  imagenauxlio: {
    width: 70,
    height: 70,
  },
  imagen: {
    width: 45,
    height: 30,
  },
  cardContainer: {
    backgroundColor: 'rgba(0, 0, 0, 0.6)',
    height: 200,
    width: 300,
    padding: 24,
    borderRadius: 24,
  },
  cardImage: {
    width: '80%',
    height: '60%',
  },
});
