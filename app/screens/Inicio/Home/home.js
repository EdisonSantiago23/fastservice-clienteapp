import React, {Component} from 'react';
import {
  Text,
  View,
  Dimensions,
  TouchableOpacity,
  StatusBar,
  Linking,
  ActivityIndicator,
} from 'react-native';
import OneSignal from 'react-native-onesignal'; // Import package from node modules
import AsyncStorage from '@react-native-community/async-storage';
import mecanicoApi from '../../../api/mecanico.js';
import Swiper from 'react-native-swiper';
var {width} = Dimensions.get('window');
import {Image} from 'react-native-elements';
import style from './styles';
import Constants from '../../../common/Constants';
import {SCLAlert, SCLAlertButton} from 'react-native-scl-alert';
export default class home extends Component {
  constructor(props) {
    super(props);
    this.mecanicoApi = new mecanicoApi();

    OneSignal.setLogLevel(6, 0);

    // Replace 'YOUR_ONESIGNAL_APP_ID' with your OneSignal App ID.
    OneSignal.init('efc303b2-694d-4e89-9944-99c0644602fa', {
      kOSSettingsKeyAutoPrompt: false,
      kOSSettingsKeyInAppLaunchURL: false,
      kOSSettingsKeyInFocusDisplayOption: 2,
    });
    OneSignal.inFocusDisplaying(2); // Controls what should happen if a notification is received while the app is open. 2 means that the notification will go directly to the device's notification center.

    // The promptForPushNotifications function code will show the iOS push notification prompt. We recommend removing the following code and instead using an In-App Message to prompt for notification permission (See step below)
    //  OneSignal.promptForPushNotificationsWithUserResponse(myiOSPromptCallback);

    OneSignal.addEventListener('received', this.onReceived);
    OneSignal.addEventListener('opened', this.onOpened);
    OneSignal.addEventListener('ids', this.onIds);

    this.state = {
      initSplash: true,
      loginSpl: true,
      visible: false,
      show: false,
      banner: [],
      tocken: '',
      userId: '',
    };
    this.salir = this.salir.bind(this);
    this.cancelar = this.cancelar.bind(this);
    this.onIds = this.onIds.bind(this);
    this.getSplash();
  }
  componentWillUnmount() {
    OneSignal.removeEventListener('received', this.onReceived);
    OneSignal.removeEventListener('opened', this.onOpened);
    OneSignal.removeEventListener('ids', this.onIds);
  }

  onReceived(notification) {
    console.log('Notification received: ', notification);
  }

  onOpened(openResult) {
    console.log('Message: ', openResult.notification.payload.body);
    console.log('Data: ', openResult.notification.payload.additionalData);
    console.log('isActive: ', openResult.notification.isAppInFocus);
    console.log('openResult: ', openResult);
  }

  onIds = async device => {
    var tokenlocal = JSON.parse(
      await AsyncStorage.getItem(Constants.STORE_ID_TOKEN),
    );
    if (tokenlocal != device.userId) {
      AsyncStorage.removeItem(Constants.STORE_ID_TOKEN);
      var user = await AsyncStorage.getItem(Constants.STORE_ID_USER);
      this.mecanicoApi.updatetoken(user, device.userId).then(res => {
        if (res) {
          AsyncStorage.setItem(
            Constants.STORE_ID_TOKEN,
            JSON.stringify(res.tocken),
          );
        }
      });
    }
  };

  salir() {
    this.setState({visible: !this.state.visible});
    AsyncStorage.removeItem(Constants.STORE_ID_USER);
    AsyncStorage.removeItem(Constants.STORE_ID_TOKEN);
    this.props.navigation.reset();
  }
  cancelar() {
    this.setState({visible: !this.state.visible});
  }
  getSplash() {
    this.setState({
      loading: true,
    });
    this.mecanicoApi.getSplash(1).then(res => {
      this.setState({
        banner: res,
        loading: false,
      });
      if (res != undefined && res.status != 'fail') {
        this.setState({
          loading: false,
        });
      }
    });
  }
  golink(item) {
    if (item?.url) {
      Linking.openURL(item.url);
    }
  }
  _renderBanner(itembann) {
    console.log(itembann.file);
    return (
      <Image
        onPress={() => this.golink(itembann)}
        placeholderStyle={{backgroundColor: 'trasparent'}}
        PlaceholderContent={
          <ActivityIndicator size={'large'} color={'#FFCD33'} />
        }
        source={{uri: itembann.file}}
        style={style.imageBanner}
        resizeMode="contain"
      />
    );
  }
  render() {
    const {panel2, panel2x} = style;

    return (
      <View style={style.container}>
        <StatusBar backgroundColor="#FF9100" barStyle="light-content" />
        <SCLAlert
          theme="warning"
          show={this.state.visible}
          title="MECÁNICA EXPRESS"
          onRequestClose={this.cancelar}
          cancellable={true}
          subtitle="Deseas salir de la app?">
          <SCLAlertButton theme="danger" onPress={this.salir}>
            Aceptar
          </SCLAlertButton>
          <SCLAlertButton theme="info" onPress={this.cancelar}>
            Cancelar
          </SCLAlertButton>
        </SCLAlert>

        <View style={[{flex: 1}, style.elementsContainer]}>
          <View style={{flex: 2}}>
            <View
              style={{
                width: '90%',
                height: '100%',
                alignContent: 'center',
                alignSelf: 'center',
                alignItems: 'center',
              }}>
              {this.state.banner.length > 0 ? (
                <Swiper
                  style={{
                    height: width / 2.5,
                    alignItems: 'center',
                  }}
                  showsButtons={false}
                  autoplay={true}
                  autoplayTimeout={5}>
                  {this.state.banner.map(itembann =>
                    this._renderBanner(itembann),
                  )}
                </Swiper>
              ) : null}
            </View>
          </View>

          <View style={{flex: 5}}>
            <View style={panel2}>
              <TouchableOpacity
                style={[
                  panel2x,
                  {
                    marginTop: 4,
                    marginRight: 4,
                    marginBottom: 4,
                    marginLeft: 8,
                  },
                ]}
                onPress={() =>
                  this.props.navigation.navigate('MenuEmergencia')
                }>
                <Image
                  style={{height: '70%', width: '100%', resizeMode: 'contain'}}
                  source={require('../../../assets/png/home/emergencia.png')}
                  resizeMode="contain"
                  resizeMethod="resize"
                />
                <Text
                  style={{
                    fontSize: 20,
                    color: '#FF9100',
                    fontWeight: 'bold',
                    textAlign: 'center',
                  }}>
                  EMERGENCIA
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={[
                  panel2x,
                  {
                    marginTop: 4,
                    marginRight: 8,
                    marginBottom: 4,
                    marginLeft: 4,
                  },
                ]}
                onPress={_ => this.props.navigation.navigate('MenuRepuestos')}>
                <Image
                  style={{height: '70%', width: '100%', resizeMode: 'contain'}}
                  resizeMode="contain"
                  resizeMethod="resize"
                  source={require('../../../assets/png/home/mantenimiento.png')}
                />
                <Text
                  style={{
                    fontSize: 20,
                    color: '#FF9100',
                    fontWeight: 'bold',
                    textAlign: 'center',
                  }}>
                  REPUESTOS
                </Text>
              </TouchableOpacity>
            </View>

            <View style={panel2}>
              <TouchableOpacity
                style={[
                  panel2x,
                  {
                    marginTop: 4,
                    marginRight: 4,
                    marginBottom: 4,
                    marginLeft: 8,
                  },
                ]}
                onPress={_ => this.props.navigation.navigate('mantenimiento')}>
                <Image
                  style={{height: '70%', width: '100%', resizeMode: 'contain'}}
                  resizeMode="contain"
                  resizeMethod="resize"
                  source={require('../../../assets/png/home/repuesto.png')}
                />
                <Text
                  style={{
                    fontSize: 18,
                    color: '#FF9100',
                    fontWeight: 'bold',
                    textAlign: 'center',
                  }}>
                  MANTENIMIENTO
                </Text>
              </TouchableOpacity>

              <TouchableOpacity
                style={[
                  panel2x,
                  {
                    marginTop: 4,
                    marginRight: 8,
                    marginBottom: 4,
                    marginLeft: 4,
                  },
                ]}
                onPress={_ => this.props.navigation.navigate('MenuNuevaMoto')}>
                <Text
                  style={{
                    fontSize: 20,
                    color: '#FF9100',
                    fontWeight: 'bold',
                    textAlign: 'center',
                  }}>
                  SERVICIO
                </Text>
                <Image
                  style={{height: '70%', width: '100%', resizeMode: 'contain'}}
                  resizeMode="contain"
                  resizeMethod="resize"
                  source={require('../../../assets/png/home/nuevamoto.png')}
                />
                <Text
                  style={{
                    fontSize: 20,
                    color: '#FF9100',
                    fontWeight: 'bold',
                    textAlign: 'center',
                  }}>
                  AUTORIZADO
                </Text>
              </TouchableOpacity>
            </View>

          </View>
        </View>
      </View>
    );
  }
}
