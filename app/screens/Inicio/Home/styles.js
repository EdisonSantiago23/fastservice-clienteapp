import {StyleSheet} from 'react-native';
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  imageBanner: {
    height: '90%',
    width: '100%',
    borderRadius: 10,
  },

  elementsContainer: {
    backgroundColor: '#fff',
    margin: 1,
  },

  panel2: {
    flex: 1,
    flexDirection: 'row',
  },
  panel2x: {
    flex: 1,
    justifyContent: 'center',
    elevation: 6,
    borderRadius: 10,
    backgroundColor: '#ffffff',
    padding: 10,
    marginHorizontal: 10,
    marginVertical: 10,
  },
  touchable: {
    flex: 1,
  },
});
export default styles;
