import React, { Component } from 'react';
import { StyleSheet, View, TouchableOpacity, TextInput, Alert, Image, ScrollView } from 'react-native';
import LoadingProgress from '../../../common/LoadingProgress';
import mecanicoApi from '../../../api/mecanico';
import { Block, Text, theme } from "galio-framework";
import { Header } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import Constants from '../../../common/Constants';
import {
    SCLAlert,
    SCLAlertButton
} from 'react-native-scl-alert'
import DateTimePicker from '@react-native-community/datetimepicker';
import Feather from 'react-native-vector-icons/Feather';
import AsyncStorage from '@react-native-community/async-storage';

export default class Cita extends Component {


    constructor(props) {
        super(props);
        this.mecanicoApi = new mecanicoApi();


        this.state = {
            initSplash: true,
            loading: false,
            idmarca: this.props.route.params.idmarca,
            show: false,
            date: new Date(),
            mode: 'date',
            horario: 0,
            fecha: '',
            hora: '',
            mfecha: '',
            mhora: '',
            showconfig: false,
            userFeatures: {
                marca_idmarca: 0,
                placa: '',
                fecha: '',
                hora: '',
                idusuario: '',
                estado: 0,
                idlocal: 0,

            },
            loginSpl: true,
            marcas: [],

        };
    }


    getMarcas(idmarca) {
        this.mecanicoApi.GetGarantias(idmarca).then(res => {

            if (res.status) {
                this.setState({ show: true, loading: false, })

            }
            this.setState({
                marcas: res,
            });
            if (res != undefined && res.status != 'fail') {
                this.setState({
                    loading: false,
                });
            }
        });
    }

    onChangeText = (key, val) => {
        this.setState({ [key]: val });
    };



    updateStatusFeature(param, value) {
        let userFeatConst = this.state.userFeatures;
        userFeatConst[param] = value;
        this.setState({ userFeatures: userFeatConst });
    }

    onChangeFecha = (event, selectedDate) => {
        const aux = selectedDate.toString().substring(4, 16);
        this.setState({
            fecha: selectedDate,
            mfecha: aux,
            horario: 0

        });
    };
    onChangeHora = (event, selectedDate) => {
        const aux = selectedDate.toString().substring(16, 24);
        this.setState({
            hora: selectedDate,
            mhora: aux,
            horario: 0
        });
    };
    handleCloseinfo = () => {
        this.setState({ showconfig: false })
        this.props.navigation.navigate('MenuNuevaMoto');

    }
    handleClose = () => {
        this.setState({ show: false })
        this.props.navigation.navigate('MenuNuevaMoto');

    }
    estado = async (estado) => {
        try {
            this.setState({ loading: true });
            this.state.userFeatures.estado = estado;
            this.state.userFeatures.marca_idmarca = this.state.idmarca;
            this.state.userFeatures.fecha = this.state.fecha;
            this.state.userFeatures.hora = this.state.hora;
            this.state.userFeatures.idusuario = await AsyncStorage.getItem(Constants.STORE_ID_USER);
            this.mecanicoApi.createReserva(this.state.userFeatures).then(res => {
                if (res) {

                    if (estado == 5) {
                        this.setState({ loading: false, showconfigfin: true });

                    } else {
                        this.setState({ loading: false, showconfig: true });
                        this.getReservas();

                    }
                } else {
                    if (res.status && res.status == 'error') {
                        Alert.alert('Error', res.message, [{ text: 'Cerrar' }]);
                        this.setState({ loading: false });
                    }
                }
            });

        } catch (err) {
            Alert.alert('Error', err.message, [{ text: 'Cerrar' }]);
        }


    }
    render() {

        if (this.state.loading) {
            return <LoadingProgress />;
        }
        if (!this.state.loading) {
            if (!this.state.marcas.status) {

                return (

                    <View style={{ height: '100%', width: '100%' }} >
              
                        <SCLAlert
                            theme="success"
                            show={this.state.showconfig}
                            title="Auxlio Mecánico"
                            onRequestClose={this.handleCloseinfo}
                            cancellable={true}

                            subtitle="Reservación creada"
                        >
                            <SCLAlertButton theme="success" onPress={this.handleCloseinfo}>Ok</SCLAlertButton>
                        </SCLAlert>
                        <View style={{ flexDirection: 'row', alignItems: 'center', marginLeft: '30%' }}>
                            <View>
                                <Image
                                    source={require('../../../assets/png/lg.png')}
                                    style={styles.imagen} />
                                <View>
                                    <Text style={{ fontSize: 18, color: '#617792', fontWeight: 'bold' }}> MECÁNICA EXPRESS</Text>
                                </View>
                            </View>
                        </View>
                        <ScrollView style={{ height: '80%' }}>
                            <View style={styles.ellanoteama}>
                               <TextInput
                                    placeholder="Placa"
                                    onChangeText={val => this.updateStatusFeature('placa', val)}
                                    style={styles.inputSearch}
                                    value={this.state.userFeatures.placa}/>
                                <View style={{ flexDirection: 'row' }}>

                                    <TouchableOpacity
                                        onPress={() =>
                                            this.setState({ horario: 2 })

                                        }
                                        style={styles.btnVerEnSitio}>
                                        <View style={{ flexDirection: 'row' }}>

                                            <Feather
                                                name="calendar"
                                                color="green"
                                                size={20}
                                            />
                                            <Text style={{ fontSize: 15, fontWeight: 'bold', textAlign: 'center' }}>Fecha</Text>
                                        </View>

                                    </TouchableOpacity>
                                    <Text style={{ fontSize: 15, fontWeight: 'bold', textAlign: 'center', marginTop: '7%' }}>{this.state.mfecha}</Text>

                                </View>

                                <View style={{ flexDirection: 'row' }}>

                                    <TouchableOpacity
                                        onPress={() =>
                                            this.setState({ horario: 1 })

                                        }
                                        style={styles.btnVerEnSitio}
                                    >
                                        <View style={{ flexDirection: 'row' }}>
                                            <Feather
                                                name="clock"
                                                color="green"
                                                size={20}
                                            />
                                            <Text style={{ fontSize: 15, fontWeight: 'bold', textAlign: 'center' }}>Hora</Text>

                                        </View>

                                    </TouchableOpacity>
                                    <Text style={{ fontSize: 15, fontWeight: 'bold', textAlign: 'center', marginTop: '7%' }}>{this.state.mhora}</Text>

                                </View>

                                {this.hora()}



                            </View>
                            {this.renderBtn()}

                        </ScrollView>
                    </View >
                )
            } else {
                return (
                    <View style={styles.container}>
                        <SCLAlert
                            theme="danger"
                            show={this.state.show}
                            title="Lo sentimos"
                            onRequestClose={this.handleClose}
                            cancellable={true}

                            subtitle="No existen servicios por mostrar"
                        >
                            <SCLAlertButton theme="danger" onPress={this.handleClose}>Atras</SCLAlertButton>
                        </SCLAlert>
                    </View>
                )
            }
        }
    }

    hora() {
        if (this.state.horario == 1) {
            return (
                <View>
                    <DateTimePicker
                        testID="dateTimePicker"
                        timeZoneOffsetInMinutes={0}
                        minuteInterval={this.state.interval}
                        value={this.state.date}
                        mode="time"
                        is24Hour
                        display={this.state.display}
                        onChange={this.onChangeHora}
                        style={styles.iOsPicker}
                    />

                </View>
            );
        }
        if (this.state.horario == 2) {
            return (
                <View>
                    <DateTimePicker
                        testID="dateTimePicker"
                        timeZoneOffsetInMinutes={0}
                        minuteInterval={this.state.interval}
                        value={this.state.date}
                        mode="date"
                        is24Hour
                        display={this.state.display}
                        onChange={this.onChangeFecha}
                        style={styles.iOsPicker}
                    />

                </View>
            );
        }
    }


    renderBtn() {
        if (!this.state.loadingBtn) {
            return (
                <View>
                    <TouchableOpacity
                        onPress={() => {
                            this.estado(0);
                        }}
                        style={styles.btnStyle}>
                        <Text style={{ fontSize: 14, textAlign: 'center', color: '#1F4997' }}>
                            GENERAR CITA
                        </Text>
                    </TouchableOpacity>

                </View>
            );
        } else {
            return (
                <TouchableOpacity style={styles.btnStyle}>
                    <ActivityIndicator size="small" color={'#1F4997'} />
                </TouchableOpacity>
            );
        }
    }
    _renderItem = ({ item, dimensions }) => (
        <View>
            <Image
                style={styles.image}
                source={item.image} />
        </View>
    );
}

const styles = StyleSheet.create({
    image: {
        width: 400,
        height: 200,

    },
    inputSearch: {
        borderBottomWidth: 1,
    },
    btnVerEnSitio: {
        backgroundColor: '#F5F6F7',
        borderRadius: 20,
        width: '50%',
        padding: '3%',
        color: 'blue',
        borderWidth: 1,
        borderColor: '#4982e6',
        marginTop: '5%'
    },
    btnStyle: {
        alignItems: 'center',
        borderWidth: 1,
        borderColor: '#4982e6',
        borderRadius: 20,
        justifyContent: 'center',
        backgroundColor: '#F5F6F7',
        height: 50,
        marginTop: 20,
        marginBottom: 80,

    },
    iconContainer: {
        flexDirection: "row",
        justifyContent: "space-evenly",
        width: 80
    },
    itemName: {
        fontSize: 40,
        color: '#617792',
        fontWeight: '600',
        fontWeight: 'bold'
    },
    touchable: {
        flex: 1,
        flexDirection: 'row',
    },
    imagen: {
        width: 75,
        height: 75,
        marginLeft: '25%'

    },

    itemContainer: {
        justifyContent: 'flex-end',
        alignItems: 'center',
        borderRadius: 5,
        padding: 10,
        height: 150,
    },
    container: {
        flex: 1,
        alignItems: 'center', width: '100%', alignContent: 'center'
    },
    contImage: {
        flex: 1,
        marginTop: 3,
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: 2,
        height: 300,
        width: '100%',
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        borderBottomLeftRadius: 20,
        borderBottomRightRadius: 20,
    },
    btnImage: {
        flex: 1,
        justifyContent: 'center',
        paddingBottom: 10,
        alignItems: 'center',
        textAlign: 'center',
    },
});
