import React, { Component } from 'react';
import { StyleSheet, View, TouchableOpacity, ScrollView, Image, Content } from 'react-native';
import LoadingProgress from '../../../common/LoadingProgress';
import mecanicoApi from '../../../api/mecanico';
import { Block, Text, theme } from "galio-framework";
import { SectionGrid } from 'react-native-super-grid';
import { Header } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import Constants from '../../../common/Constants';
export default class detallemoto extends Component {


  constructor(props) {
    super(props);
    this.mecanicoApi = new mecanicoApi();

    this.state = {
      initSplash: true,
      loading: true,
      garantia: this.props.route.params.garantia,

      loginSpl: true,
      detalle: [],



    };
    this.GetDetalle(this.state.garantia.idgarantia);
  }


  GetDetalle(idtipoMantenimiento) {
    this.mecanicoApi.getdetallegarantiamoto(idtipoMantenimiento).then(res => {
      if (res.status == 'fail') {
        this.setState({
          loading: false,

        });
      }else{
        this.setState({
          loading: false,
          detalle: res,

        });
      }
    });
  }


  render() {

    if (this.state.loading) {
      return <LoadingProgress />;
    }
    if (!this.state.loading) {
      return (
        <View>
        

          <ScrollView style={{ height: '90%' }}>

            <View style={{ flexDirection: 'row', alignItems: 'center', marginLeft: '30%' }}>
              <View>
                <Image
                  source={require('../../../assets/png/lg.png')}
                  style={styles.imagen} />
                <View>
                  <Text style={{ fontSize: 18, color: '#617792', fontWeight: 'bold' }}> MECÁNICA EXPRESS</Text>
                </View>
              </View>
            </View>

            <View style={styles.container2}>
              <View style={styles.containerdtn}>
                <TouchableOpacity style={styles.btnVerEnSitioL}
                  onPress={() => this.props.navigation.navigate('MapaMotos', { idtipo: 6, detalle: this.state.garantia })}>
                  <View style={styles.containerdtn}>
                    <Icon
                      raised
                      name='search'
                      color='#617792'
                      size={20}
                    />
                    <Text style={{ textAlign: 'center', marginLeft: '10%' }}>
                      A DOMICILIO
                        </Text>
                  </View>

                </TouchableOpacity>
                <TouchableOpacity style={styles.btnVerEnSitioL}
                  onPress={() => this.props.navigation.navigate('MapaMotos', { idtipo: 7, detalle: this.state.garantia })}>
                  <View style={styles.containerdtn}>
                    <Icon
                      raised
                      name='search'
                      color='#617792'
                      size={20}
                    />
                    <Text style={{ textAlign: 'center', marginLeft: '10%' }}>
                      TALLERES
                        </Text>
                  </View>

                </TouchableOpacity>

              </View>
              <Text style={styles.text}>RECUERDA  EL SERVICIO A DOMICILIO NO TIENE NINGUN COSTO EXTRA</Text>

              <SectionGrid
                itemDimension={130}
                sections={[
                  {
                    data: this.state.detalle,
                  },
                ]}
                renderItem={({ item }) => (

                  <View style={[styles.itemContainer, { backgroundColor: '#f1c40f' }]}>
                    <Icon
                      raised
                      name='wrench'
                      color='#617792'
                      size={40}
                    />
                    <Text style={styles.itemName}>{item.nombre}</Text>
                  </View>
                )}
              />
            </View>
            <Text style={styles.title}>Recuerda que solo debes pagar por el filtro y el aceite</Text>
            <View style={styles.container2}>
              <Image
                source={{ uri: this.state.garantia.url }}
                style={styles.avatar}
              />
              <Text style={styles.text}>$ {this.state.garantia.precio}</Text>
            </View>
          </ScrollView>

        </View >
      );
    }
  }
  _renderItem = ({ item, dimensions }) => (
    <View>
      <Image
        style={styles.image}
        source={item.image} />
    </View>
  );
}

const styles = StyleSheet.create({
  image: {
    width: 400,
    height: 200,

  },
  container2: {
    flex: 1,
    alignItems: 'center',
    padding: '3%',
  },
  containerdtn: {
    bottom: 0,
    alignItems: 'center',
    flexDirection: "row",

  },
  avatar: {
    width: 150,
    height: 150,
    borderWidth: 0,
    alignItems: 'center',

  },
  btnVerEnSitioL: {
    backgroundColor: '#F5F6F7',
    borderRadius: 20,
    alignItems: 'center',
    width: '50%',
    padding: '3%',
    color: 'blue',
    borderWidth: 1,
    marginTop: '5%',
    borderColor: '#4982e6',

  },
  btnVerEnSitioR: {

    backgroundColor: '#F5F6F7',
    borderRadius: 20,
    alignItems: 'center',
    width: '50%',
    padding: '3%',
    color: 'blue',
    borderWidth: 1,
    marginTop: '5%',
    borderColor: '#4982e6',

  },
  title: {
    fontFamily: 'vagron',
    fontSize: 24,
    color: '#1F4997',
    backgroundColor: 'transparent',
    textAlign: 'center',
    marginVertical: 5,
  },
  text: {
    color: '#73221C',
    backgroundColor: 'transparent',
    textAlign: 'center',
    fontSize: 25,
    paddingHorizontal: 10,
    marginBottom: 10,
    marginTop: '8%'
  },
  gridView: {
    marginTop: 10,
    flex: 1,
  },
  itemContainerloco: {
    justifyContent: 'flex-end',
    borderRadius: 5,
    padding: 10,
    height: 150,
  },

  itemContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5,
    padding: 10,
    height: 150,
  },
  itemName: {
    fontSize: 16,
    color: '#617792',
    fontWeight: '600',
    fontWeight: 'bold'
  },
  itemCode: {
    fontWeight: '600',
    fontSize: 12,
    color: '#fff',
  },
  imagen: {
    width: 75,
    height: 75,
    marginLeft: '25%'

  },
  iconContainer: {
    flexDirection: "row",
    justifyContent: "space-evenly",
    width: 80
  },
  touchable: {
    flex: 1,
    flexDirection: 'row',
  },

  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  contImage: {
    flex: 1,
    marginTop: 3,
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: 2,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
  },
  btnImage: {
    flex: 1,
    justifyContent: 'center',
    paddingBottom: 10,
    alignItems: 'center',
    textAlign: 'center',
  },
});
