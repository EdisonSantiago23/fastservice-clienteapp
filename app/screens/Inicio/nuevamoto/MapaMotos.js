import React, { Component } from 'react';
import { StyleSheet, TouchableOpacity, Dimensions, Alert, View, Image, ActivityIndicator, ScrollView } from 'react-native';
import mecanicoApi from '../../../api/mecanico.js';
import { Block, Text, theme } from "galio-framework";

import Geolocation from '@react-native-community/geolocation';
import MapView, { Marker, ProviderPropType } from 'react-native-maps';
import frelance from '../../../assets/free.png';
import local from '../../../assets/car.png';
import AsyncStorage from '@react-native-community/async-storage';
import Constants from '../../../common/Constants';
import LoadingProgress from '../../../common/LoadingProgress';
import { List, ListItem } from 'react-native-elements'
import TouchableScale from 'react-native-touchable-scale'; // https://github.com/kohver/react-native-touchable-scale
import LinearGradient from 'react-native-linear-gradient'; // Only if no expo
import { Header } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
var { height, width } = Dimensions.get('window');
import {
    SCLAlert,
    SCLAlertButton
} from 'react-native-scl-alert'

export default class MapaMotos extends Component {

    constructor(props) {
        super(props);
        this.mecanicoApi = new mecanicoApi();

        this.state = {
            isVisible: false,
            locales: [],
            tockens: [],
            loading: true,
            detalle: this.props.route.params.detalle,
            idtipo: this.props.route.params.idtipo,
            poi: {
                latitude: '',
                longitude: '',
            },
            show: false,
            show2: true,

            region: {
                latitude: 0,
                longitude: 0,
                latitudeDelta: 0,
                longitudeDelta: 0.04,
            },
            markers: [],

        };
        this.getLocationUser();
    }
    handleOpen = () => {
        this.setState({ show: true })
    }

    handleClose = () => {
        this.setState({ show: false })
        this.props.navigation.navigate('MenuAuxilioMotos')

    }

    handleOpen2 = () => {
        this.setState({ show2: true })
    }

    handleClose2 = () => {
        this.setState({ show2: false })
        this.props.navigation.navigate('MenuAuxilioMotos')
        this.props.navigation.navigate('Moto', { garantia: this.state.detalle });

    }

    getLocationUser = async () => {
        await Geolocation.getCurrentPosition(
            position => {
                const { latitude, longitude } = position.coords;
                this.state.poi.latitude = position.coords.latitude;
                this.state.poi.longitude = position.coords.longitude;
                this.setState({
                    region: {
                        longitude: longitude,
                        latitude: latitude,
                        latitudeDelta: 0.029213524352655895,
                        longitudeDelta: (width / height) * 0.029213524352655895,
                    },
                });
                this.getLocales(latitude, longitude, this.state.detalle.marca_idmarca);
            },
            error => {
                console.log(error);
            },
        );
    };



    createReserva = async () => {
        try {
            this.setState({ loadingBtn: true });
            this.state.repuesto.idUsuario = await AsyncStorage.getItem(Constants.STORE_ID_USER);
            this.state.repuesto.latitud = this.state.region.latitude;
            this.state.repuesto.longitud = this.state.region.longitude;
            this.mecanicoApi.crearReservaRepuesto(this.state.repuesto).then(res => {
                if (res.idReservaRepuesto) {
                    this.setState({ loadingBtn: false, show: true });

                } else {
                    if (res.status && res.status == 'error') {
                        Alert.alert('Error', res.message, [{ text: 'Cerrar' }]);
                        this.setState({ loadingBtn: false });
                    }
                }
            });

        } catch (err) {
            Alert.alert('Error', err.message, [{ text: 'Cerrar' }]);
        }
    };



    getLocales(lat, long, tipo) {
        this.mecanicoApi.getMotoInsignia(lat, long, tipo).then(res => {
            console.log('holaaa');
            console.log(res);
            var aux = Object.values(res).sort(function (a, b) {
                return a.distancia - b.distancia;
            });
            this.setState({
                locales: aux,
            });
            if (res != undefined && res.status != 'fail') {
                this.setState({
                    loading: false,
                });
            }
        });
    }

    render() {
        if (this.state.loading) {
            return <LoadingProgress />;
        }
        if (!this.state.loading) {
            if (!this.state.locales.status) {
                return (
                    <View style={{ height: '100%' }} >
                     
                        <View style={{ alignItems: 'center', flexDirection: 'column' }}>
                            <Image
                                source={require('../../../assets/png/lg.png')}
                                style={styles.imagenauxlio}
                            />
                            <Text style={{ fontSize: 17, color: '#617792', fontWeight: 'bold', textAlign: 'center' }}>MECÁNICA EXPRESS</Text>

                        </View>
                        <SCLAlert
                            theme="success"
                            show={this.state.show}
                            title="MECÁNICA EXPRESS"
                            onRequestClose={this.handleClose}
                            cancellable={true}

                            subtitle="Reserva exitosa"
                        >
                            <SCLAlertButton theme="success" onPress={this.handleClose}>Ok</SCLAlertButton>
                        </SCLAlert>



                        <View >
                            <View style={{ height: '100%' }}>
                                <Text style={styles.title}>SELECCIONA EL SERVICIO</Text>
                                <Text style={styles.text}>DISPONIBLE</Text>
                                <View >
                                    <MapView
                                        style={styles.map}
                                        mapType={"standard"}
                                        initialCamera={{
                                            center: {
                                                latitude: this.state.region.latitude,
                                                longitude: this.state.region.longitude,
                                            },
                                            pitch: 45,
                                            heading: 90,
                                            altitude: 1000,
                                            zoom: 17,
                                        }}
                                        loadingEnabled
                                        loadingIndicatorColor="#666666"
                                        loadingBackgroundColor="#eeeeee"
                                        showsIndoors
                                        showsIndoorLevelPicker
                                        zoomEnabled={true}
                                        pitchEnabled={true}
                                        showsUserLocation={true}
                                        followsUserLocation={true}
                                        showsCompass={true}
                                        showsBuildings={true}
                                        showsTraffic={true}
                                        showsIndoors={true}
                                        initialRegion={this.state.region}
                                        zoomEnabled={true}
                                    >
                                        {this.state.locales.map(marker => (
                                            <Marker
                                                title={marker.nombre}
                                                image={marker.localtype == 0 ? (
                                                    local
                                                ) : (
                                                        frelance)}

                                                coordinate={marker.coordinate[0]}
                                            />

                                        ))}

                                    </MapView>

                                    <View style={{ flex: 1 }}>
                                        <ScrollView style={{ height: '100%' }}>
                                            {this.state.locales.map(marker => (
                                                <ListItem
                                                    style={{
                                                        paddingTop: '1%',
                                                    }}
                                                    badge={marker.distancia >= 1 ? (
                                                        {
                                                            value: marker.distancia.toFixed(0) + ' Km',
                                                            textStyle: { color: 'white', fontWeight: 'bold', fontSize: 15 }, containerStyle: { color: 'orange' }
                                                        }
                                                    ) : (
                                                            {
                                                                value: (marker.distancia * 1000).toFixed(0) + ' m',
                                                                textStyle: { color: 'white', fontWeight: 'bold', fontSize: 15 }, containerStyle: { color: 'orange' }
                                                            }
                                                        )}
                                                    Component={TouchableScale}
                                                    friction={90} //
                                                    tension={100} // These props are passed to the parent component (here TouchableScale)
                                                    activeScale={0.95} //
                                                    linearGradientProps={{
                                                        colors: ['#BCBCCE', '#617792'],
                                                        start: { x: 1, y: 0 },
                                                        end: { x: 0.2, y: 0 },
                                                    }}
                                                    ViewComponent={LinearGradient} // Only if no expo
                                                    onPress={() => this.props.navigation.navigate('MecanicoMoto', { local: marker, tipopedido: this.state.idtipo, emergencia: this.state.detalle })
                                                    }


                                                    leftAvatar={{ rounded: true, source: local }}
                                                    title={marker.nombre}
                                                    titleStyle={{ color: 'white', fontWeight: 'bold' }}
                                                    subtitle={marker.tiempo}

                                                    subtitleStyle={{ color: 'white' }}

                                                    bottomDivider
                                                    chevron={{ color: '#617792' }}
                                                />
                                            ))}
                                        </ScrollView>

                                    </View>

                                </View>

                            </View>
                        </View >

                    </View >
                );
            } else {
                return (
                    <View>
                        <SCLAlert
                            theme="danger"
                            show={this.state.show2}
                            title="MECÁNICA EXPRESS"
                            onRequestClose={this.handleClose2}
                            cancellable={true}

                            subtitle="Sin loclaes con insignias"
                        >
                            <SCLAlertButton theme="danger" onPress={this.handleClose2}>Ok</SCLAlertButton>
                        </SCLAlert>
                    </View>
                )
            }
        }
    }
    renderBtn() {
        if (!this.state.loadingBtn) {
            return (
                <View>
                    <TouchableOpacity
                        onPress={() => {
                            this.createReserva()
                        }}
                        style={styles.btnStyle}>
                        <Text style={{ fontSize: 14, textAlign: 'center', color: '#1F4997' }}>
                            SOLICITAR COTIZACIÓN
                        </Text>
                    </TouchableOpacity>

                </View>
            );
        } else {
            return (
                <TouchableOpacity style={styles.btnStyle}>
                    <ActivityIndicator size="small" color={'#1F4997'} />
                </TouchableOpacity>
            );
        }
    }
}

const styles = StyleSheet.create({

    contImage: {
        marginTop: 3,
        justifyContent: 'center',
        borderWidth: 2,
        width: '100%',
        borderColor: '#4982e6',
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        borderBottomLeftRadius: 20,
        borderBottomRightRadius: 20,
    },
    title: {
        fontFamily: 'vagron',
        fontSize: 24,
        color: '#1F4997',
        backgroundColor: 'transparent',
        textAlign: 'center',
        marginVertical: 5,
    },
    text: {
        color: '#73221C',
        backgroundColor: 'transparent',
        textAlign: 'center',
        fontSize: 18,
        paddingHorizontal: 10,
        marginBottom: 10,
    },
    btnStyle: {
        alignItems: 'center',
        borderWidth: 1,
        borderColor: '#4982e6',
        borderRadius: 20,
        justifyContent: 'center',
        backgroundColor: '#F5F6F7',
        height: 50,

    },
    map: {
        width: '100%',
        height: '65%',
    },
    carousel: {
        position: 'absolute',
        bottom: 0,
        marginBottom: 48
    },
    imagenauxlio: {
        width: 70,
        height: 70,
    },
    imagen: {
        width: 45,
        height: 30,
    },
    cardContainer: {
        backgroundColor: 'rgba(0, 0, 0, 0.6)',
        height: 200,
        width: 300,
        padding: 24,
        borderRadius: 24
    },
    cardImage: {
        height: 120,
        width: 300,
        bottom: 0,
        position: 'absolute',
        borderBottomLeftRadius: 24,
        borderBottomRightRadius: 24
    },
    cardTitle: {
        color: 'white',
        fontSize: 22,
        marginTop: -30,
        alignSelf: 'center'
    }
});


