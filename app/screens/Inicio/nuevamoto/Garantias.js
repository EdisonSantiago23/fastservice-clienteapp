import React, { Component } from 'react';
import { StyleSheet, View, TouchableOpacity, ImageBackground, Image, ScrollView } from 'react-native';
import LoadingProgress from '../../../common/LoadingProgress';
import mecanicoApi from '../../../api/mecanico';
import { Block, Text, theme } from "galio-framework";
import { Header } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import { SectionGrid } from 'react-native-super-grid';
import {
    SCLAlert,
    SCLAlertButton
} from 'react-native-scl-alert'
import Feather from 'react-native-vector-icons/Feather';

export default class Garantias extends Component {


    constructor(props) {
        super(props);
        this.mecanicoApi = new mecanicoApi();

        this.state = {
            initSplash: true,
            loading: true,
            idmarca: this.props.route.params.idmarca,
            show: false,

            loginSpl: true,
            marcas: [],

        };
        this.getMarcas(this.state.idmarca);
    }


    getMarcas(idmarca) {
        this.mecanicoApi.GetGarantias(idmarca).then(res => {

            if (res.status) {
                this.setState({ show: true, loading: false, })

            }
            this.setState({
                marcas: res,
            });
            if (res != undefined && res.status != 'fail') {
                this.setState({
                    loading: false,
                });
            }
        });
    }


    handleClose = () => {
        this.setState({ show: false })
        this.props.navigation.navigate('MenuNuevaMoto');

    }
    render() {
        if (this.state.loading) {
            return <LoadingProgress />;
        }
        if (!this.state.loading) {
            if (!this.state.marcas.status) {

                return (

                    <View style={{ height: '100%', width: '100%' }} >
                
                        <View style={{ flexDirection: 'row', alignItems: 'center', marginLeft: '30%' }}>
                            <View>
                                <Image
                                    source={require('../../../assets/png/lg.png')}
                                    style={styles.imagen} />
                                <View>
                                    <Text style={{ fontSize: 18, color: '#617792', fontWeight: 'bold' }}> MECÁNICA EXPRESS</Text>
                                </View>
                            </View>

                        </View>
                        <View style={{ flexDirection: 'row', alignItems: 'center'}}>
                            <TouchableOpacity
                                onPress={() => {
                                    this.props.navigation.navigate('MisCitas',{idmarca:this.state.idmarca})
                                }}
                                style={styles.btnVerEnSitio}>
                                <View style={{ flexDirection: 'row' }}>
                                    <Feather
                                        name="calendar"
                                        color="green"
                                        size={20}
                                    />
                                    <Text style={{ fontSize: 14, textAlign: 'center', color: '#1F4997' }}>
                                        Mis Reservas
                        </Text>
                                </View>

                            </TouchableOpacity>
                            <TouchableOpacity
                                onPress={() => {
                                    this.props.navigation.navigate('Cita', { idmarca: this.state.idmarca })
                                }}
                                style={styles.btnVerEnSitio}>
                                <View style={{ flexDirection: 'row' }}>

                                    <Feather
                                        name="clock"
                                        color="green"
                                        size={20}
                                    />

                                    <Text style={{ fontSize: 14, textAlign: 'center', color: '#1F4997' }}>
                                        Crear una Reservas</Text>
                                </View>

                            </TouchableOpacity>

                        </View>
                        <ScrollView style={{ height: '80%' }}>
                            <SectionGrid
                                itemDimension={130}
                                sections={[
                                    {
                                        data: this.state.marcas,
                                    },
                                ]}
                                renderItem={({ item }) => (

                                    <View style={[styles.itemContainer, { backgroundColor: '#f1c40f' }]}>
                                        <TouchableOpacity onPress={() => {
                                            this.props.navigation.navigate('Moto', { garantia: item, idmarca: this.state.idmarca });
                                        }}>

                                            <Icon
                                                raised
                                                name='wrench'
                                                color='#617792'
                                                size={40}
                                            />
                                            <Text style={styles.itemName}>{item.nombre}km</Text>
                                        </TouchableOpacity>

                                    </View>
                                )}
                            />

                        </ScrollView>
                    </View >
                )
            } else {
                return (
                    <View style={styles.container}>
                        <SCLAlert
                            theme="danger"
                            show={this.state.show}
                            title="Lo sentimos"
                            onRequestClose={this.handleClose}
                            cancellable={true}

                            subtitle="No existen servicios por mostrar"
                        >
                            <SCLAlertButton theme="danger" onPress={this.handleClose}>Atras</SCLAlertButton>
                        </SCLAlert>
                    </View>
                )
            }
        }
    }
    _renderItem = ({ item, dimensions }) => (
        <View>
            <Image
                style={styles.image}
                source={item.image} />
        </View>
    );
}

const styles = StyleSheet.create({
    image: {
        width: 400,
        height: 200,

    },
    btnVerEnSitio: {
        backgroundColor: '#F5F6F7',
        borderRadius: 20,
        width: '50%',
        padding: '3%',
        color: 'blue',
        borderWidth: 1,
        borderColor: '#4982e6',
    },
    iconContainer: {
        flexDirection: "row",
        justifyContent: "space-evenly",
        width: 80
    },
    itemName: {
        fontSize: 40,
        color: '#617792',
        fontWeight: '600',
        fontWeight: 'bold'
    },
    touchable: {
        flex: 1,
        flexDirection: 'row',
    },
    imagen: {
        width: 75,
        height: 75,
        marginLeft: '25%'

    },

    itemContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 5,
        padding: 10,
        height: 150,
    },
    container: {
        flex: 1,
        alignItems: 'center', width: '100%', alignContent: 'center'
    },
    contImage: {
        flex: 1,
        marginTop: 3,
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: 2,
        height: 300,
        width: '100%',
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        borderBottomLeftRadius: 20,
        borderBottomRightRadius: 20,
    },
    btnImage: {
        flex: 1,
        justifyContent: 'center',
        paddingBottom: 10,
        alignItems: 'center',
        textAlign: 'center',
    },
});
