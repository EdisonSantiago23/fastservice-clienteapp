import React, { Component } from 'react';
import {
    StyleSheet, Dimensions, StatusBar, View, SafeAreaView
    , ScrollView, ImageBackground, Image, TouchableOpacity
} from 'react-native';
import { Block, Text, theme } from "galio-framework";
const { width, height } = Dimensions.get("screen");
const StatusHeight = StatusBar.currentHeight;
const HeaderHeight = (theme.SIZES.BASE * 3.5 + (StatusHeight || 0));
const thumbMeasure = (width - 48 - 32) / 3;
import mecanicoApi from '../../../api/mecanico.js';
import { Rating, AirbnbRating } from 'react-native-ratings';
import LoadingProgress from '../../../common/LoadingProgress';
var moment = require('moment'); // require
import Constants from '../../../common/Constants';
import { Header } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import {
    SCLAlert,
    SCLAlertButton
} from 'react-native-scl-alert'
export default class MecanicoMoto extends Component {
    constructor(props) {
        super(props);
        this.mecanicoApi = new mecanicoApi();

        this.state = {
            initSplash: true,
            loginSpl: true,
            loading: true,
            local: this.props.route.params.local,
            emergencia: this.props.route.params.emergencia,
            tipopedido: this.props.route.params.tipopedido,
            show: true,

            mecanicos: [],

        };
        this.GetMecanicos(this.state.local.id);
    }
    handleOpen = () => {
        this.setState({ show: true })
    }

    handleClose = () => {
        this.setState({ show: false })
        this.props.navigation.navigate('MenuAuxilioMotos')

    }
    GetMecanicos(idLocal) {
        this.mecanicoApi.GetMecanicos(idLocal).then(res => {
            this.setState({
                mecanicos: res,
            });
            if (res != undefined) {
                this.setState({
                    loading: false,
                });
            }
        });
    }
    render() {
        if (this.state.loading) {
            return <LoadingProgress />;
        }
        if (!this.state.loading) {
            if (!this.state.mecanicos.status) {
                return (
                    <View>
                
                        <ImageBackground
                            style={styles.profileContainer}
                            imageStyle={styles.profileBackground}>

                            <Block middle style={styles.nameInfo}>
                                <Text bold size={40} color="#32325D">
                                    {this.state.local.nombre}
                                </Text>
                                <Text bold size={20} color="#32325D">
                                    Mecánicos disponibles
                              </Text>
                            </Block>
                            <View style={{ flexDirection: 'row', alignItems: 'center', marginLeft: '5%' }}>
                                <SafeAreaView >
                                    <ScrollView horizontal={true}>
                                        {this.state.mecanicos.map((item, index) => (
                                            <TouchableOpacity
                                                keyExtractor={(item, index) => index.toString()}
                                                style={styles.fotoContainer}
                                                onPress={() => this.props.navigation.navigate('ConfirmarMoto', { mecanico: item, tipo: this.state.tipopedido, emergencia: this.state.emergencia })
                                                }>
                                                {this.renderFileData(item, index)}

                                            </TouchableOpacity>

                                        ))}
                                    </ScrollView>
                                </SafeAreaView>
                            </View>
                        </ImageBackground>
                    </View>

                )
            } else {
                return (
                    <View style={styles.container}>
                        <SCLAlert
                            theme="danger"
                            show={this.state.show}
                            title="Lo sentimos"
                            onRequestClose={this.handleClose}
                            cancellable={true}

                            subtitle="Nuestros mecánicos se encuentran ocupados"
                        >
                            <SCLAlertButton theme="danger" onPress={this.handleClose}>Atras</SCLAlertButton>
                        </SCLAlert>
                    </View>
                );
            }
        }
    }


    renderFileData(mecanico, index) {
        return (

            <View style={{ width: '100%', height: '90%', alignContent: 'center' }}>
                <Block flex style={styles.profileCard}>
                    <Block middle style={styles.avatarContainer}>
                        <Image
                            source={{ uri: Constants.BASEURI + mecanico.url }}
                            style={styles.avatar}
                        />
                    </Block>
                    <Block style={styles.info}>
                        <Block
                            middle
                            row
                            space="evenly"
                            style={{ marginTop: 20, paddingBottom: 24 }}
                        >

                        </Block>
                        <Block middle style={styles.nameInfo}>
                            <Text bold size={28} color="#32325D">
                                {mecanico.nombre}</Text>


                        </Block>
                        <AirbnbRating
                            count={5}
                            isDisabled={true}
                            style={{ marginTop: '5%' }}
                            reviews={["Malo", "Regular", "Normal", "Bueno", "Excelente"]}
                            defaultRating={mecanico.calificacion}
                            size={30}
                        />

                    </Block>

                    <Block
                        row
                        space="between"
                        style={{ paddingVertical: 10, alignItems: "baseline" }}
                    >
                        <Block middle style={{ paddingHorizontal: 10 }} >
                            <Text bold
                                size={16}
                                color="#525F7F"
                                style={{ marginBottom: 4 }}
                            >
                                {mecanico.experiencia}
                            </Text>
                            <Text size={16}>Experiencia</Text>
                        </Block>
                        <Block middle style={{ paddingHorizontal: 10 }} >
                            <Text
                                bold
                                color="#525F7F"
                                size={16}
                                style={{ marginBottom: 4 }}
                            >
                                {mecanico.calificacion}

                            </Text>

                            <Text size={16}>Calificación</Text>
                        </Block>
                        <Block middle style={{ paddingHorizontal: 10 }} >
                            <Text
                                bold
                                color="#525F7F"
                                size={16}
                                style={{ marginBottom: 4 }}
                            >
                                {moment().diff(mecanico.fechanacimiento, 'years')}
                            </Text>
                            <Text size={16}>Edad</Text>
                        </Block>
                    </Block>

                    <Block flex>

                        <Block middle style={{ marginTop: 30, marginBottom: 16 }}>
                            <Block style={styles.divider} />
                        </Block>


                        <Block
                            row
                            style={{ paddingVertical: 14, alignItems: "baseline" }}
                        >
                            <Block middle>
                                <Text bold size={16}
                                    color="#525F7F"
                                    style={{ textAlign: "center", marginTop: -80 }}>
                                    Capacitado en:
</Text>

                            </Block>
                            <Text style={{ textAlign: 'center', marginTop: -100, color: "#525F7F", fontSize: 16 }}   >
                                {mecanico.capacitaciones}
                            </Text>

                        </Block>
                        <Block
                            row
                            style={{ paddingVertical: 14, alignItems: "baseline" }}
                        >
                            <Block middle>
                                <Text bold size={16}
                                    color="#525F7F"
                                    style={{ textAlign: "center", marginTop: -80 }}>
                                    Especialiasta en:
</Text>

                            </Block>
                            <Text style={{ textAlign: 'center', marginTop: -100, color: "#525F7F", fontSize: 16 }}   >
                                {mecanico.especialiasta}
                            </Text>

                        </Block>


                    </Block>

                </Block>

            </View>

        );

    }
}


const styles = StyleSheet.create({
    container: {
        ...StyleSheet.absoluteFillObject
    },
    profile: {
        marginTop: Platform.OS === "android" ? -HeaderHeight : 0,
        // marginBottom: -HeaderHeight * 2,
        flex: 1
    },
    containeraler: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center'
    },
    map: {
        ...StyleSheet.absoluteFillObject
    },
    headerContainer: {
        flexDirection: 'row',
        padding: 5,
        alignItems: 'flex-start',
    },
    btnVerEnSitio: {
        backgroundColor: '#F5F6F7',
        borderRadius: 20,
        width: '100%',
        padding: '3%',
        color: 'blue',
        borderWidth: 1,
        borderColor: '#4982e6',
    },
    carousel: {
        position: 'absolute',
        bottom: 0,
        marginBottom: 48
    },
    cardContainer: {
        alignSelf: 'center',
        backgroundColor: '#617792',
        height: 600,
        width: 350,
        padding: 50,
        borderRadius: 24
    },
    cardImage: {
        height: 120,
        width: 300,
        bottom: 0,
        position: 'absolute',
        borderBottomLeftRadius: 24,
        borderBottomRightRadius: 24
    },
    button: {
        alignSelf: 'center',

        fontSize: 16,
        color: '#fff',
        backgroundColor: '#617792',
        paddingVertical: 10,
        paddingHorizontal: 15,
        textAlign: "center",
        height: 50,
        width: 350,
        padding: 50,
        borderRadius: 24
    },
    cardTitle: {
        color: 'white',
        fontSize: 22,
        alignSelf: 'center'
    },
    profile: {
        marginTop: Platform.OS === "android" ? -HeaderHeight : 0,
        // marginBottom: -HeaderHeight * 2,
        flex: 1
    },
    profileContainer: {
        width: width,
        height: height,
        color: '#617792',
        padding: 0,
        zIndex: 1,
        marginTop: '10%'
    },
    profileBackground: {
        width: width,
        height: height / 2
    },
    profileCard: {
        // position: "relative",
        padding: theme.SIZES.BASE,
        marginHorizontal: theme.SIZES.BASE,
        marginTop: 65,
        borderTopLeftRadius: 6,
        borderTopRightRadius: 6,
        backgroundColor: theme.COLORS.WHITE,
        shadowColor: "black",
        shadowOffset: { width: 0, height: 0 },
        shadowRadius: 8,
        shadowOpacity: 0.2,
        zIndex: 2,
        width:350
    },
    info: {
        paddingHorizontal: 40,
        alignItems:'center'
    },
    avatarContainer: {
        position: "relative",
        marginTop: -80
    },
    avatar: {
        width: 124,
        height: 124,
        borderRadius: 62,
        borderWidth: 0
    },
    nameInfo: {
        marginTop: -40,
        padding:'5%'
        
    },
    divider: {
        width: "90%",
        borderWidth: 1,
        borderColor: "#E9ECEF"
    },
    thumb: {
        borderRadius: 4,
        marginVertical: 4,
        alignSelf: "center",
        width: thumbMeasure,
        height: thumbMeasure
    }
});

