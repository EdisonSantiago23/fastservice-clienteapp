import React, { Component } from 'react';
import { StyleSheet, View, TouchableOpacity, TextInput, Image, Dimensions,ScrollView } from 'react-native';
import LoadingProgress from '../../../common/LoadingProgress';
import mecanicoApi from '../../../api/mecanico';
import { Block, Text, theme } from "galio-framework";
import { Header } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {
    SCLAlert,
    SCLAlertButton
} from 'react-native-scl-alert'

const { width, height } = Dimensions.get("window");
const CARD_HEIGHT = 350;
const CARD_WIDTH = width * 0.9;
const SPACING_FOR_CARD_INSET = width * 0.1 - 10;
export default class Moto extends Component {


    constructor(props) {
        super(props);
        this.mecanicoApi = new mecanicoApi();

        this.state = {
            initSplash: true,
            loading: false,
            garantia: this.props.route.params.garantia,
            idmarca: this.props.route.params.idmarca,

            

            show: false,
            existe: false,

            moto: false,
            loginSpl: true,
            marcas: [],
            userFeatures: {
                placa: '',

            },
        };
    }
    updateStatusFeature(param, value) {
        let userFeatConst = this.state.userFeatures;
        userFeatConst[param] = value;
        this.setState({ userFeatures: userFeatConst });
    }



    buscarmoto = () => {
        if (this.state.userFeatures.placa !== '') {
            this.setState({ loading: true });
            this.mecanicoApi.getMotoPlaca(this.state.userFeatures.placa).then(res => {
                if (res.status== 'error') {
                    this.setState({ moto: true, loading: false, })
                }

                if (res != undefined && res.status != 'error') {
                    this.setState({
                        loading: false,
                        marcas: res[0],
                        existe: true
                    });
                }
            });
        }
    }
    handleClose = () => {
        this.setState({ moto: false })
    }
    estadodelagarantia = () => {
        var rango = parseInt(this.state.garantia.nombre) -1000;
        if (parseInt(this.state.marcas.km) >=rango && parseInt(this.state.marcas.km) <= this.state.garantia.nombre) {
            return true
        } else {
            return false
        }
    }
    render() {
        if (this.state.loading) {
            return <LoadingProgress />;
        }
        if (!this.state.loading) {
            return (

                <View style={{ height: '100%', width: '100%', flex: 1, }} >
         
                    <View style={styles.container}>
                        <SCLAlert
                            theme="danger"
                            show={this.state.moto}
                            title="Lo sentimos"
                            onRequestClose={this.handleClose}
                            cancellable={true}

                            subtitle="No se encontro la moto"
                        >
                            <SCLAlertButton theme="danger" onPress={this.handleClose}>Nueva busqueda</SCLAlertButton>
                        </SCLAlert>
                    </View>
                    <View style={{ flexDirection: 'row', alignItems: 'center', marginLeft: '30%' }}>
                        <View>
                            <Image
                                source={require('../../../assets/png/lg.png')}
                                style={styles.imagen} />
                            <View>
                                <Text style={{ fontSize: 18, color: '#617792', fontWeight: 'bold' }}> MECÁNICA EXPRESS</Text>
                            </View>
                        </View>
                    </View>
                    <View style={styles.searchBox}>
                        <TextInput
                            placeholder="Ingresa tu placa"
                            placeholderTextColor="#000"
                            autoCapitalize="none"
                            onChangeText={val => this.updateStatusFeature('placa', val)}

                            style={{ flex: 1, padding: 0 }}
                        />
                        <TouchableOpacity onPress={() => {
                            this.buscarmoto();
                        }}>
                            <Ionicons name="ios-search" size={20} />
                        </TouchableOpacity>
                    </View>
                    {this.state.existe ?
                        <ScrollView style={{ height: '100%' }}>

                            <View style={styles.card} >
                                <View style={styles.textContent}>
                                    <View style={styles.chipsItem}>
                                        <Text numberOfLines={1} style={styles.cardtitle}>Propietario:</Text>
                                        <Text numberOfLines={1} style={styles.cardDescription}>{this.state.marcas.propietario}</Text>
                                    </View>
                                    <View style={styles.chipsItem}>
                                        <Text numberOfLines={1} style={styles.cardtitle}>Fecha de compra:</Text>
                                        <Text numberOfLines={1} style={styles.cardDescription}>{this.state.marcas.fechacompra}</Text>
                                    </View>
                                    <View style={styles.chipsItem}>
                                        <Text numberOfLines={1} style={styles.cardtitle}>Placa:</Text>
                                        <Text numberOfLines={1} style={styles.cardDescription}>{this.state.marcas.placa}</Text>
                                    </View>
                                    <View style={styles.chipsItem}>
                                        <Text numberOfLines={1} style={styles.cardtitle}>Kilometros:</Text>
                                        <Text numberOfLines={1} style={styles.cardDescription}>{this.state.marcas.km}</Text>
                                    </View>
                                    {this.estadodelagarantia() ?
                                        <View>

                                            <View style={styles.button}>
                                                <TouchableOpacity
                                                    onPress={() => { }}
                                                    style={[styles.signIn, {
                                                        borderColor: '#3AF052',
                                                        borderWidth: 1
                                                    }]}
                                                >
                                                    <Text style={[styles.textSign, {
                                                        color: '#3AF052'
                                                    }]}>Activa</Text>
                                                </TouchableOpacity>
                                                <Text style={[styles.textSign, {
                                                    fontSize: 14,
                                                    fontWeight: 'bold'
                                                }]}>Estado de la garantía</Text>
                                            </View>

                                            <View style={styles.button}>
                                                <TouchableOpacity
                                                    onPress={() => {
                                                        this.props.navigation.navigate('detallemoto', { garantia: this.state.garantia });

                                                    }}
                                                    style={[styles.signIn, {
                                                        borderColor: '#617792',
                                                        borderWidth: 1
                                                    }]}
                                                >
                                                    <Text style={[styles.textSign, {
                                                        color: '#617792'
                                                    }]}>Continuar</Text>
                                                </TouchableOpacity>
                                            </View>
                                        </View> :

                                        <View>

                                            <View style={styles.button}>
                                                <TouchableOpacity
                                                    onPress={() => { }}
                                                    style={[styles.signIn, {
                                                        borderColor: '#FF6347',
                                                        borderWidth: 1
                                                    }]}
                                                >
                                                    <Text style={[styles.textSign, {
                                                        color: '#FF6347'
                                                    }]}>Inactiva</Text>
                                                </TouchableOpacity>
                                                <Text style={[styles.textSign, {
                                                    fontSize: 14,
                                                    fontWeight: 'bold'
                                                }]}>Estado de la garantía</Text>
                                            </View>

                                        </View>
                                    }
                                </View>
                            </View>
                      
                        </ScrollView>

                        : null}
                </View >
            )
        }
    }
    _renderItem = ({ item, dimensions }) => (
        <View>
            <Image
                style={styles.image}
                source={item.image} />
        </View>
    );
}

const styles = StyleSheet.create({
    image: {
        width: 400,
        height: 200,

    },
    button: {
        alignItems: 'center',
        marginTop: 15
    },
    textSign: {
        fontSize: 14,
        fontWeight: 'bold'
    },
    signIn: {
        width: '100%',
        padding: 5,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 3
    },
    chipsItem: {
        flexDirection: "row",
        backgroundColor: '#fff',
        borderRadius: 20,
        padding: 8,
        paddingHorizontal: 20,
        marginHorizontal: 10,
        height: 35,
        shadowColor: '#ccc',
        shadowOffset: { width: 0, height: 3 },
        shadowOpacity: 0.5,
        shadowRadius: 5,
        elevation: 10,
        marginTop: '5%'
    },
    cardtitle: {
        fontSize: 12,
        // marginTop: 5,
        fontWeight: "bold",
    },
    cardDescription: {
        fontSize: 12,
        color: "#444",
    },
    searchBox: {
        marginTop: Platform.OS === 'ios' ? 40 : 20,
        flexDirection: "row",
        backgroundColor: '#fff',
        width: '90%',
        alignSelf: 'center',
        borderRadius: 5,
        padding: 10,
        shadowColor: '#ccc',
        shadowOffset: { width: 0, height: 3 },
        shadowOpacity: 0.5,
        shadowRadius: 5,
        elevation: 10,
    },
    textContent: {
        flex: 2,
        padding: 10,
    },
    card: {
        padding: 10,
        alignSelf: 'center',
        elevation: 2,
        backgroundColor: "#FFF",
        borderTopLeftRadius: 5,
        borderTopRightRadius: 5,
        marginHorizontal: 10,
        shadowColor: "#000",
        shadowRadius: 5,
        shadowOpacity: 0.3,
        shadowOffset: { x: 2, y: -2 },
        height: CARD_HEIGHT,
        width: CARD_WIDTH,
        overflow: "hidden",
        marginTop: '20%'
    },
    iconContainer: {
        flexDirection: "row",
        justifyContent: "space-evenly",
        width: 80
    },
    itemName: {
        fontSize: 40,
        color: '#617792',
        fontWeight: '600',
        fontWeight: 'bold'
    },
    touchable: {
        flex: 1,
        flexDirection: 'row',
    },
    imagen: {
        width: 75,
        height: 75,
        marginLeft: '25%'

    },

    itemContainer: {
        justifyContent: 'flex-end',
        alignItems: 'center',
        borderRadius: 5,
        padding: 10,
        height: 150,
    },
    container: {
        alignItems: 'center', width: '100%', alignContent: 'center'
    },
    contImage: {
        flex: 1,
        marginTop: 3,
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: 2,
        height: 300,
        width: '100%',
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        borderBottomLeftRadius: 20,
        borderBottomRightRadius: 20,
    },
    btnImage: {
        flex: 1,
        justifyContent: 'center',
        paddingBottom: 10,
        alignItems: 'center',
        textAlign: 'center',
    },
});
