import React, { Component } from 'react';
import { StyleSheet, View, TouchableOpacity, ImageBackground, StatusBar, Image, ScrollView } from 'react-native';
import LoadingProgress from '../../../common/LoadingProgress';
import mecanicoApi from '../../../api/mecanico';
import { Block, Text, theme } from "galio-framework";
import { Header } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import {
    SCLAlert,
    SCLAlertButton
} from 'react-native-scl-alert'
import AsyncStorage from '@react-native-community/async-storage';
import Constants from '../../../common/Constants';
export default class MenuNuevaMoto extends Component {


    constructor(props) {
        super(props);
        this.mecanicoApi = new mecanicoApi();

        this.state = {
            initSplash: true,
            loading: true,
            show: false,
            nombre:'',
            loginSpl: true,
            marcas: [],

        };
        this.getMarcas();
        this.getname();

    }
    getname = async () => {
        var nomb=await AsyncStorage.getItem(Constants.STORE_NAME_USER);
        nomb=nomb.substring(1, nomb.length-1)
        this.setState({
            nombre: nomb,
        });
    }
    handleOpen = () => {
        this.setState({ show: true })
    }

    handleClose = () => {
        this.setState({ show: false })
        this.props.navigation.navigate('MenuAuxilioMotos');

    }
    getMarcas() {
        this.mecanicoApi.getMarcas().then(res => {

            if (res.status) {
                this.setState({ show: true, loading: false, })

            }
            this.setState({
                marcas: res,
            });
            if (res != undefined && res.status != 'fail') {
                this.setState({
                    loading: false,
                });
            }
        });
    }


    render() {
        if (this.state.loading) {
            return <LoadingProgress />;
        }
        if (!this.state.loading) {
            if (!this.state.marcas.status) {
                return (
                    <View style={{ height: '100%', width: '100%' }} >
              
                        <StatusBar backgroundColor='#FF9100' barStyle="light-content" />

                        <ScrollView style={{ height: '90%', width: '100%' }}>
                            {this.state.marcas.map(emergencia => (
                                <View style={styles.contImage}>

                                    <ImageBackground source={{
                                        uri: emergencia.url
                                    }}

                                        imageStyle={{ borderRadius: 20 }} style={{
                                            width: '100%', height: '100%',

                                        }}>
                                        <TouchableOpacity
                                            onPress={() =>
                                                this.props.navigation.navigate('Garantias', { idmarca: emergencia.idmarca })
                                            }
                                            style={styles.btnImage}
                                        >
                                            <Text style={{ fontSize: 40, fontWeight: 'bold', textAlign: 'center' }}>{emergencia.nombre}</Text>

                                        </TouchableOpacity>
                                    </ImageBackground>

                                </View>

                            ))}
                        </ScrollView>

                    </View >
                )
            } else {
                return (
                    <View style={styles.container}>
                        <SCLAlert
                            theme="danger"
                            show={this.state.show}
                            title="Lo sentimos"
                            onRequestClose={this.handleClose}
                            cancellable={true}

                            subtitle="No existen servicios por mostrar"
                        >
                            <SCLAlertButton theme="danger" onPress={this.handleClose}>Atras</SCLAlertButton>
                        </SCLAlert>
                    </View>
                )
            }
        }
    }
    _renderItem = ({ item, dimensions }) => (
        <View>
            <Image
                style={styles.image}
                source={item.image} />
        </View>
    );
}

const styles = StyleSheet.create({
    image: {
        width: 400,
        height: 200,

    },
    iconContainer: {
        flexDirection: "row",
        justifyContent: "space-evenly",
        width: 80
    },
    touchable: {
        flex: 1,
        flexDirection: 'row',
    },

    container: {
        flex: 1,
        alignItems: 'center',
        padding: '3%',
    },
    contImage: {
        flex: 1,
        marginTop: 3,
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: 2,
        height: 300,
        width: '100%',
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        borderBottomLeftRadius: 20,
        borderBottomRightRadius: 20,
    },
    btnImage: {
        flex: 1,
        justifyContent: 'center',
        paddingBottom: 10,
        alignItems: 'center',
        textAlign: 'center',
    },
});
