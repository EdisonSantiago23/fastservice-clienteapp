import React, {Component} from 'react';
import {
  Text,
  SafeAreaView,
  Image,
  ScrollView,
  StyleSheet,
  View,
  TouchableOpacity,
  StatusBar,
} from 'react-native';
import mecanicoApi from '../../../api/mecanico.js';
import ImagePicker from 'react-native-image-picker';
import FeatherIcon from 'react-native-vector-icons/Feather';
import AntDesign from 'react-native-vector-icons/AntDesign';
import AsyncStorage from '@react-native-community/async-storage';
import Constants from '../../../common/Constants';
import FastText from '../../Common/Components/FastImput';
import {showDangerMsg, showSuccesMsg} from '../../../common/ShowMessages';

export default class MenuRepuestos extends Component {
  constructor(props) {
    super(props);
    this.mecanicoApi = new mecanicoApi();
    this.state = {
      imageFileData: '',
      loading: true,
      nombre: '',
      arrFotos: [
        {
          id: 0,
          fileData: '',
        },
        {
          id: 1,
          fileData: '',
        },
        {
          id: 2,
          fileData: '',
        },
      ],
      userFeatures: {
        idUsuario: '',
        nombre: '',
        marca: '',
        latitud: '',
        longitud: '',
        estado: 0,

        tocken: '',
        headings: 'Repuesto',
        content: 'Necesito una cotización',
        imgpequena: '',
        imggrande: '/',
        imggrande1: '/',
        imggrande2: '/',
        orden: 0,
      },
    };
    this.getname();
  }
  getname = async () => {
    var nomb = await AsyncStorage.getItem(Constants.STORE_NAME_USER);
    nomb = nomb.substring(1, nomb.length - 1);
    this.setState({
      nombre: nomb,
    });
  };
  onChangeText = (key, val) => {
    this.setState({[key]: val});
  };

  updateStatusFeature(param, value) {
    let userFeatConst = this.state.userFeatures;
    userFeatConst[param] = value;
    this.setState({userFeatures: userFeatConst});
  }

  validadorCampo() {
    if (
      this.state.userFeatures.nombre != '' &&
      this.state.userFeatures.marca != '' &&

      this.state.userFeatures.imggrande != ''
    ) {
      return true;
    } else {

      showDangerMsg('No debe existir campos vacios','Campos erróneos');

      return false;
    }
  }

  Mapa = async orde => {
    this.state.userFeatures.imggrande = this.state.arrFotos[0].fileData;
    this.state.userFeatures.imggrande1 = this.state.arrFotos[1].fileData;
    this.state.userFeatures.imggrande2 = this.state.arrFotos[2].fileData;
    try {
      if (this.validadorCampo()) {
        this.props.navigation.navigate('MapaRepuestos', {
          repuesto: this.state.userFeatures,
          orde: orde,
        });
      }
    } catch (err) {
      showDangerMsg(err.message,'Error');

    }
  };

  render() {
    return (
      <View style={{height: '100%'}}>
  
        <StatusBar backgroundColor="#FF9100" barStyle="light-content" />

        <ScrollView style={{height: '80%'}}>
          <View style={styles.container}>
            <View style={{alignItems: 'center'}}>
              <Image
                source={require('../../../assets/png/lg.png')}
                style={styles.imagen}
              />
              <View>
                <Text
                  style={{
                    fontSize: 17,
                    color: '#617792',
                    fontWeight: 'bold',
                    textAlign: 'center',
                  }}>
                  MECÁNICA EXPRESS
                </Text>
              </View>
            </View>
            <View>
              <Text
                style={{
                  fontSize: 15,
                  color: '#617792',
                  marginTop: '10%',
                  fontWeight: 'bold',
                  textAlign: 'center',
                }}>
                BUSCAR
              </Text>
            </View>
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <SafeAreaView style={styles.container2}>
                <ScrollView horizontal={true}>
                  {this.state.arrFotos.map((item, index) => (
                    <TouchableOpacity
                      keyExtractor={(item, index) => index.toString()}
                      style={styles.fotoContainer}
                      onPress={() => this.chooseImage(index)}>
                      {this.renderFileData(item, index)}
                    </TouchableOpacity>
                  ))}
                </ScrollView>
              </SafeAreaView>
            </View>
            <View>
              <Text
                style={{
                  fontSize: 17,
                  color: '#617792',
                  fontWeight: 'bold',
                  textAlign: 'center',
                }}>
                FOTOS DEL REPUESTO
              </Text>
            </View>

            <View style={styles.ellanoteama}>
              <FastText
                label={'Nombre'}
                multiline={true}
                value={this.state.userFeatures?.nombre}
                max={150}
                onChangeText={value => this.updateStatusFeature('nombre', value)}
              />
              <FastText
                label={'Marca'}
                multiline={true}
                value={this.state.userFeatures?.marca}
                max={150}
                onChangeText={value => this.updateStatusFeature('marca', value)}
              />
     
              {this.renderBtn()}
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
  renderBtn() {
    return (
      <View>
        <TouchableOpacity
          onPress={() => {
            this.Mapa(1);
          }}
          style={styles.btnStyle}>
          <Text style={{fontSize: 14, textAlign: 'center', color: '#1F4997'}}>
            CERCA DE MI
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            this.Mapa(2);
          }}
          style={styles.btnStyle}>
          <Text style={{fontSize: 14, textAlign: 'center', color: '#1F4997'}}>
            QUITO
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            this.Mapa(3);
          }}
          style={styles.btnStyle}>
          <Text style={{fontSize: 14, textAlign: 'center', color: '#1F4997'}}>
            DISTRIBUIDORAS
          </Text>
        </TouchableOpacity>
      </View>
    );
  }

  chooseImage(index) {
    let options = {
      title: 'Seleccionar imagen',
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
      cancelButtonTitle: 'Cancelar',
      takePhotoButtonTitle: 'Tomar Foto',
      chooseFromLibraryButtonTitle: 'Elegir Foto de la Galería',
      mediaType: 'photo',
      index: index,
    };
    ImagePicker.showImagePicker(options, response => {
      if (response.didCancel) {
      } else if (response.error) {
      } else if (response.customButton) {
      } else {
        let ministatus = this.state.arrFotos;
        ministatus[options.index].fileData =
          'data:image/jpg;base64,' + response.data;
        this.setState({
          arrFotos: ministatus,
        });
      }
    });
  }

  renderFileData(item, index) {
    if (this.state.arrFotos[index].fileData) {
      return (
        <View>
          <Image
            source={{
              uri: this.state.arrFotos[index].fileData,
            }}
            style={styles.images}
          />

          <TouchableOpacity
            style={{position: 'absolute', top: 15, right: 5}}
            onPress={() => {
              let copy = this.state.arrFotos;
              copy[index].fileData = null;
              this.setState({arrFotos: copy});
            }}>
            <CloseIcon />
          </TouchableOpacity>
        </View>
      );
    } else {
      return (
        <FeatherIcon
          name="camera"
          size={50}
          style={{margin: 50}}
          color="#000"
        />
      );
    }
  }
}
const CloseIcon = () => {
  return (
    <AntDesign
      name={'close'}
      size={12}
      color={'#000'}
      style={{backgroundColor: '#D4D7DD', borderRadius: 50}}
    />
  );
};
const rnpickerStyles = StyleSheet.create({
  labelStyle: {textAlign: 'left', minWidth: 60},
  labelStyle2: {textAlign: 'center', minWidth: 60},
  pickerContainer: {
    borderBottomWidth: 1,
    borderBottomColor: '#FFFF',
    flex: 1,
  },
});

const styles = StyleSheet.create({
  btnVerEnSitio: {
    backgroundColor: '#F5F6F7',
    borderRadius: 20,
    alignItems: 'center',
    width: '70%',
    padding: '3%',
    color: 'blue',
    borderWidth: 1,
    marginTop: '5%',
    borderColor: '#4982e6',
  },
  fotoContainer: {
    borderColor: '#424244',
    borderWidth: 1,
    borderStyle: 'dotted',
    backgroundColor: '#F9FCFD',
  },
  container2: {
    flex: 1,
    alignItems: 'center',
    padding: 15,
    backgroundColor: '#fff',
  },
  map: {
    width: 180,
    height: 180,
  },

  btnVerEnSitio3: {
    backgroundColor: '#F5F6F7',
    borderRadius: 20,
    alignItems: 'center',
    width: '90%',
    padding: '3%',
    color: 'blue',
    borderWidth: 1,
    marginTop: '5%',
    borderColor: '#4982e6',
  },
  fotoContainerfree: {
    width: 300,
    height: 300,
    borderColor: 'black',
    borderWidth: 1,
  },
  imagesfrelance: {
    width: 300,
    height: 300,
    borderColor: 'black',
    borderWidth: 1,
  },
  container: {
    flex: 1,
    alignItems: 'center',
    height: '100%',
    padding: '3%',
    backgroundColor: '#fff',
  },
  btnVerEnSitio2: {
    backgroundColor: '#F5F6F7',
    borderRadius: 20,
    alignItems: 'center',
    width: '70%',
    padding: '3%',
    color: 'blue',
    borderWidth: 1,
    marginTop: '-10%',
    borderColor: '#4982e6',
  },
  descriptionTag: {
    color: '#502A18',
  },
  btnTextStyle: {
    color: '#FFFFFF',
    fontSize: 22,
    alignItems: 'center',
    textAlign: 'center',
  },
  btnStyle: {
    alignItems: 'center',
    borderWidth: 1,
    borderColor: '#4982e6',
    borderRadius: 20,
    justifyContent: 'center',
    backgroundColor: '#F5F6F7',
    height: 50,
    marginTop: 20,
  },
  images: {
    width: 150,
    height: 150,
    borderColor: 'black',
    borderWidth: 1,
  },
  fotoContainer: {
    borderColor: '#424244',
    borderWidth: 1,
    borderStyle: 'dotted',
    backgroundColor: '#F9FCFD',
  },

  ellanoteama: {
    minWidth: '80%',
    backgroundColor: '#fff',
  },
  createAccount: {
    fontSize: 30,
    textAlign: 'left',
    marginVertical: 15,
  },
  inputSearch: {
    borderBottomWidth: 1,
  },
  imagen: {
    width: 70,
    height: 70,
  },
  imagen2: {
    width: 155,
    height: 130,
  },
});
