import React, {Component} from 'react';
import {
  StyleSheet,
  Dimensions,
  StatusBar,
  ScrollView,
  ImageBackground,
  Image,
  TouchableOpacity,
} from 'react-native';
import {Block, Text, theme} from 'galio-framework';
const {width, height} = Dimensions.get('screen');
const StatusHeight = StatusBar.currentHeight;
const HeaderHeight = theme.SIZES.BASE * 3.5 + (StatusHeight || 0);
const thumbMeasure = (width - 48 - 32) / 3;
export default class Repuesto extends Component {
  constructor(props) {
    super(props);
    this.state = {
      initSplash: true,
      loginSpl: true,
    };
  }

  render() {
    return (
      <ImageBackground
        style={styles.profileContainer}
        imageStyle={styles.profileBackground}>
        <ScrollView
          showsVerticalScrollIndicator={false}
          style={{width, marginTop: '25%'}}>
          <Block flex style={styles.profileCard}>
            <Block middle style={styles.avatarContainer}>
              <Image
                source={require('../../../assets/png/foto1.png')}
                style={styles.avatar}
              />
            </Block>
            <Block style={styles.info}>
              <Block
                middle
                row
                space="evenly"
                style={{marginTop: 20, paddingBottom: 24}}></Block>
              <Block row space="between">
                <Block middle>
                  <Text
                    bold
                    size={12}
                    color="#525F7F"
                    style={{marginBottom: 4}}>
                    10 Años
                  </Text>
                  <Text size={12}>Experiencia</Text>
                </Block>
                <Block middle>
                  <Text
                    bold
                    color="#525F7F"
                    size={12}
                    style={{marginBottom: 4}}>
                    10
                  </Text>
                  <Text size={12}>Calificacion</Text>
                </Block>
                <Block middle>
                  <Text
                    bold
                    color="#525F7F"
                    size={12}
                    style={{marginBottom: 4}}>
                    20 Años
                  </Text>
                  <Text size={12}>Edad</Text>
                </Block>
              </Block>
            </Block>
            <Block flex>
              <Block middle style={styles.nameInfo}>
                <Text bold size={28} color="#32325D">
                  Mauricio Salazar, 27
                </Text>
                <Text size={16} color="#32325D" style={{marginTop: 10}}>
                  Quito, ECUADOR
                </Text>
              </Block>
              <Block middle style={{marginTop: 30, marginBottom: 16}}>
                <Block style={styles.divider} />
              </Block>
              <Block middle>
                <Text size={16} color="#525F7F" style={{textAlign: 'center'}}>
                  Especialista:
                </Text>
              </Block>
              <Block row style={{paddingVertical: 14, alignItems: 'baseline'}}>
                <Text bold size={16} color="#525F7F">
                  Motos de alto y bajo cilindraje.
                </Text>
              </Block>
              <TouchableOpacity
                style={styles.btnVerEnSitio}
                onPress={() => {
                  this.props.navigation.navigate('Pagos');
                }}>
                <Text
                  style={{fontSize: 14, textAlign: 'center', color: '#1F4997'}}>
                  Seleccionar
                </Text>
              </TouchableOpacity>
            </Block>
          </Block>
        </ScrollView>
      </ImageBackground>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
  },
  profile: {
    marginTop: Platform.OS === 'android' ? -HeaderHeight : 0,
    // marginBottom: -HeaderHeight * 2,
    flex: 1,
  },
  map: {
    ...StyleSheet.absoluteFillObject,
  },
  headerContainer: {
    flexDirection: 'row',
    padding: 5,
    alignItems: 'flex-start',
  },
  btnVerEnSitio: {
    backgroundColor: '#F5F6F7',
    borderRadius: 20,
    width: '100%',
    padding: '3%',
    color: 'blue',
    borderWidth: 1,
    borderColor: '#4982e6',
  },
  carousel: {
    position: 'absolute',
    bottom: 0,
    marginBottom: 48,
  },
  cardContainer: {
    alignSelf: 'center',
    backgroundColor: '#617792',
    height: 600,
    width: 350,
    padding: 50,
    borderRadius: 24,
  },
  cardImage: {
    height: 120,
    width: 300,
    bottom: 0,
    position: 'absolute',
    borderBottomLeftRadius: 24,
    borderBottomRightRadius: 24,
  },
  button: {
    alignSelf: 'center',

    fontSize: 16,
    color: '#fff',
    backgroundColor: '#617792',
    paddingVertical: 10,
    paddingHorizontal: 15,
    textAlign: 'center',
    height: 50,
    width: 350,
    padding: 50,
    borderRadius: 24,
  },
  cardTitle: {
    color: 'white',
    fontSize: 22,
    alignSelf: 'center',
  },
  profile: {
    marginTop: Platform.OS === 'android' ? -HeaderHeight : 0,
    // marginBottom: -HeaderHeight * 2,
    flex: 1,
  },
  profileContainer: {
    width: width,
    height: height,
    color: '#617792',
    padding: 0,
    zIndex: 1,
  },
  profileBackground: {
    width: width,
    height: height / 2,
  },
  profileCard: {
    // position: "relative",
    padding: theme.SIZES.BASE,
    marginHorizontal: theme.SIZES.BASE,
    marginTop: 65,
    borderTopLeftRadius: 6,
    borderTopRightRadius: 6,
    backgroundColor: theme.COLORS.WHITE,
    shadowColor: 'black',
    shadowOffset: {width: 0, height: 0},
    shadowRadius: 8,
    shadowOpacity: 0.2,
    zIndex: 2,
  },
  info: {
    paddingHorizontal: 40,
  },
  avatarContainer: {
    position: 'relative',
    marginTop: -80,
  },
  avatar: {
    width: 124,
    height: 124,
    borderRadius: 62,
    borderWidth: 0,
  },
  nameInfo: {
    marginTop: 35,
  },
  divider: {
    width: '90%',
    borderWidth: 1,
    borderColor: '#E9ECEF',
  },
  thumb: {
    borderRadius: 4,
    marginVertical: 4,
    alignSelf: 'center',
    width: thumbMeasure,
    height: thumbMeasure,
  },
});
