import React, {Component} from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  Text,
  FlatList,
  View,
  Image,
  ActivityIndicator,
  Dimensions,
} from 'react-native';
import mecanicoApi from '../../../api/mecanico.js';
import Geolocation from '@react-native-community/geolocation';
import MapView, {Marker} from 'react-native-maps';
import frelance from '../../../assets/free.png';
import local from '../../../assets/car.png';
import AsyncStorage from '@react-native-community/async-storage';
import Constants from '../../../common/Constants';
import {CommonActions} from '@react-navigation/native';
import LoadingProgress from '../../../common/LoadingProgress';
const {width, height} = Dimensions.get('screen');
const CARD_HEIGHT = height / 3.5;
import {SCLAlert, SCLAlertButton} from 'react-native-scl-alert';
import {showDangerMsg, showSuccesMsg} from '../../../common/ShowMessages';

export default class MapaRepuestos extends Component {
  constructor(props) {
    super(props);
    this.mecanicoApi = new mecanicoApi();

    this.state = {
      isVisible: false,
      locales: [],
      tockens: [],
      loading: true,
      show: false,
      showtrue: false,

      repuesto: this.props.route.params.repuesto,
      orde: this.props.route.params.orde,

      region: {
        latitude: 0,
        longitude: 0,
        latitudeDelta: 0,
        longitudeDelta: 0.04,
      },
      poi: {
        latitude: '',
        longitude: '',
      },
      markers: [],
    };
    this.getLocationUser();
  }
  handleOpen = () => {
    this.setState({show: true});
  };

  handleClose = () => {
    this.setState({show: false});
    this.reset();
  };
  handleClosetrue = () => {
    this.setState({showtrue: false});
    this.props.navigation.navigate('MenuAuxilioMotos');
  };
  getLocationUser = async () => {
    await Geolocation.getCurrentPosition(
      (position) => {
        const {latitude, longitude} = position.coords;
        this.state.poi.latitude = position.coords.latitude;
        this.state.poi.longitude = position.coords.longitude;
        this.setState({
          region: {
            longitude: longitude,
            latitude: latitude,
            latitudeDelta: 0.029213524352655895,
            longitudeDelta: (width / height) * 0.029213524352655895,
          },
        });
        this.getLocales(latitude, longitude);
      },
      (error) => {
        console.log(error);
      },
    );
  };
  createReserva = async () => {
    try {
      this.setState({loading: true});
      this.state.repuesto.idUsuario = await AsyncStorage.getItem(
        Constants.STORE_ID_USER,
      );
      this.state.repuesto.latitud = this.state.region.latitude;
      this.state.repuesto.longitud = this.state.region.longitude;
      this.state.repuesto.orden = this.state.orde;
      this.mecanicoApi.crearReservaRepuesto(this.state.repuesto).then((res) => {
        if (res.idReservaRepuesto) {
          this.setState({loading: false, show: true});
        } else {
          if (res.status && res.status == 'error') {
            showDangerMsg(res.message,'Error');

            this.setState({loading: false});
          }
        }
      });
    } catch (err) {
      showDangerMsg(err.message,'Error');

    }
  };
  getLocales(lat, long) {
    this.mecanicoApi.getLocalesOrden(lat, long, this.state.orde).then((res) => {
      if (res.status === 'error') {
        this.setState({
          loading: false,
          showtrue: true,
        });
      } else {
        var aux = Object.values(res).sort(function (a, b) {
          return a.distancia - b.distancia;
        });
        this.setState({
          locales: aux,
          loading: false,
        });
      }
    });
  }
  reset() {
    return this.props.navigation.dispatch(
      CommonActions.reset({index: 0, routes: [{name: 'home'}]}),
    );
  }

  render() {
    if (this.state.loading) {
      return <LoadingProgress />;
    }
    if (!this.state.loading) {
      return (
        <View>
          <SCLAlert
            theme="success"
            show={this.state.show}
            title="Correcto"
            onRequestClose={this.handleClose}
            cancellable={true}
            subtitle="Cotización enviada exitosamente">
            <SCLAlertButton theme="success" onPress={this.handleClose}>
              Ok
            </SCLAlertButton>
          </SCLAlert>
          <SCLAlert
            theme="danger"
            show={this.state.showtrue}
            title="Lo sentimos"
            onRequestClose={this.handleClosetrue}
            cancellable={true}
            subtitle="Locales no disponibles">
            <SCLAlertButton theme="danger" onPress={this.handleClosetrue}>
              Atras
            </SCLAlertButton>
          </SCLAlert>

          <View style={{width: '100%', height: '100%'}}>
            <MapView
              style={styles.map}
              mapType={'standard'}
              initialCamera={{
                center: {
                  latitude: this.state.region.latitude,
                  longitude: this.state.region.longitude,
                },
                pitch: 45,
                heading: 90,
                altitude: 1000,
                zoom: 17,
              }}
              loadingEnabled
              loadingIndicatorColor="#666666"
              loadingBackgroundColor="#eeeeee"
              showsIndoors
              showsIndoorLevelPicker
              zoomEnabled={true}
              pitchEnabled={true}
              showsUserLocation={true}
              followsUserLocation={true}
              showsCompass={true}
              showsBuildings={true}
              showsTraffic={true}
              showsIndoors={true}
              initialRegion={this.state.region}
              zoomEnabled={true}>
              {this.state.locales.map((marker) => (
                <Marker
                  title={marker.nombre}
                  image={marker.localtype == 0 ? local : frelance}
                  coordinate={marker.coordinate[0]}
                />
              ))}
            </MapView>
          </View>
          {this.renderBtn()}
          {this.renderParkings()}
        </View>
      );
    }
  }
  renderParkings() {
    return (
      <FlatList
        horizontal
        scrollEnabled
        centerContent
        showsHorizontalScrollIndicator={false}
        style={[styles.parkings, styles.shadow]}
        data={this.state.locales}
        keyExtractor={(item) => item.id.toString()}
        renderItem={({item}) => this._renderItem(item)}
      />
    );
  }
  _renderItem(item) {
    console.log(item);
    return (
      <View style={styles.card}>
        <Text style={{fontSize: 15, color: '#000000', textAlign: 'center'}}>
          {item.nombre}
        </Text>

        <View
          style={{
            width: '100%',
            flexDirection: 'row',
            justifyContent: 'space-between',
          }}>
          <Text style={{fontSize: 15, color: '#000000'}}>Dirección:</Text>
          <Text style={{fontSize: 15, color: '#000000'}}>{item.direccion}</Text>
        </View>
        <View
          style={{
            width: '100%',
            flexDirection: 'row',
            justifyContent: 'space-between',
          }}>
          <Text style={{fontSize: 15, color: '#000000'}}>Tiempo:</Text>
          <Text style={{color: '#FF9100'}}>{item.tiempo}</Text>
        </View>
        <View style={{width: '100%', height: '100%', flexDirection: 'row'}}>
          <Image
            source={{uri: Constants.BASEURI + item.imagen}}
            style={styles.cardImage}
            resizeMode="cover"
          />
        </View>
      </View>
    );
  }
  renderBtn() {
    if (!this.state.loadingBtn) {
      return (
        <View style={styles.btnStyle}>
          <TouchableOpacity
            onPress={() => {
              this.createReserva();
            }}>
            <Text style={{fontSize: 14, textAlign: 'center', color: '#1F4997'}}>
              SOLICITAR COTIZACIÓN
            </Text>
          </TouchableOpacity>
        </View>
      );
    } else {
      return (
        <TouchableOpacity style={styles.btnStyle}>
          <ActivityIndicator size="small" color={'#1F4997'} />
        </TouchableOpacity>
      );
    }
  }
}

const styles = StyleSheet.create({
  container: {},
  card: {
    backgroundColor: 'white',
    borderRadius: 6,
    padding: 12,
    marginHorizontal: 12,
    width: width - 50 * 2,
    height: CARD_HEIGHT,
  },
  shadow: {
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 12,
    },
    shadowOpacity: 0.58,
    shadowRadius: 16.0,
    elevation: 24,
  },
  parkings: {
    position: 'absolute',
    right: 0,
    left: 0,
    bottom: 0,
    paddingBottom: 24,
  },
  contImage: {
    marginTop: 3,
    justifyContent: 'center',
    borderWidth: 2,
    width: '100%',
    borderColor: '#4982e6',
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
  },
  btnStyle: {
    alignItems: 'center',
    textAlign: 'center',
    borderWidth: 1,
    borderColor: '#FF9100',
    borderRadius: 10,
    justifyContent: 'center',
    backgroundColor: '#F5F6F7',
    height: 50,
    marginLeft:'50%',
    position: 'absolute',
  },
  map: {
    width: '100%',
    height: '100%',
  },
  carousel: {
    position: 'absolute',
    bottom: 0,
    marginBottom: 48,
  },
  imagenauxlio: {
    width: 70,
    height: 70,
  },
  imagen: {
    width: 45,
    height: 30,
  },
  cardContainer: {
    backgroundColor: 'rgba(0, 0, 0, 0.6)',
    height: 200,
    width: 300,
    padding: 24,
    borderRadius: 24,
  },
  cardImage: {
    width: '80%',
    height: '60%',
  },
  cardTitle: {
    color: 'white',
    fontSize: 22,
    marginTop: -30,
    alignSelf: 'center',
  },
});
