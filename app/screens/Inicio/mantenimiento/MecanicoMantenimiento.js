import React, {Component} from 'react';
import {
  StyleSheet,
  Dimensions,
  StatusBar,
  View,
  TouchableOpacity,
} from 'react-native';
import {Block,} from 'galio-framework';
const {width, height} = Dimensions.get('screen');
import Feathers from 'react-native-vector-icons/Feather';
const thumbMeasure = (width - 48 - 32) / 3;
import mecanicoApi from '../../../api/mecanico.js';
import LoadingProgress from '../../../common/LoadingProgress';
var moment = require('moment'); // require
import Constants from '../../../common/Constants';
import {Avatar} from 'react-native-elements';
import FastText from '../../Common/Components/FastText';
import { CustomDividerpracs} from '../../Common/MiniComponents';
import { CommonActions } from '@react-navigation/native';

export default class MecanicoMantenimiento extends Component {
  constructor(props) {
    super(props);
    this.mecanicoApi = new mecanicoApi();

    this.state = {
      initSplash: true,
      loginSpl: true,
      loading: true,
      local: this.props.route.params.local,
      emergencia: this.props.route.params.emergencia,

      mecanicos: [],
    };
    this.GetMecanicos(this.state.local.id);
  }
  GetMecanicos(idLocal) {
    this.mecanicoApi.GetMecanicos(idLocal).then((res) => {
      this.setState({
        mecanicos: res,
      });
      if (res != undefined) {
        this.setState({
          loading: false,
        });
      }
    });
  }
  render() {
    if (this.state.loading) {
      return <LoadingProgress />;
    }
    if (!this.state.loading) {
      return (
        <View>
          <StatusBar backgroundColor="#FF9100" barStyle="light-content" />

          <View style={styles.profileContainer}>
            <Block middle style={styles.nameInfo}>
              <FastText bold size={40} color="#32325D">
                {this.state.local.nombre}
              </FastText>
              <FastText bold size={20} color="#32325D">
                Mecánicos disponibles
              </FastText>
            </Block>
            {this.state.mecanicos.map((item, index) =>
              this.renderFileData(item, index),
            )}
          </View>
        </View>
      );
    }
  }
  reset() { return this.props.navigation.dispatch(CommonActions.reset({ index: 0, routes: [{ name: 'home' },], })); }

  renderFileData(item, index) {
    return (
      <TouchableOpacity
        keyExtractor={(index) => index.toString()}
        onPress={() =>
          this.props.navigation.navigate('ConfirmarMantenimineto', {
            mecanico: item,
            emergencia: this.state.emergencia,
          })
        }>
        <View style={styles.cardresult}>
          <View style={{width: '100%', height: '87%'}}>
            <View style={{flexDirection: 'row', width: '100%'}}>
              <View
                style={{
                  width: '30%',
                  alignItems: 'center',
                  alignContent: 'center',
                  alignSelf: 'center',
                }}>
                <Avatar
                  rounded
                  imageProps={{resizeMode: 'contain'}}
                  source={{uri: Constants.BASEURI + item.imagen}}
                  size="large"
                />
                <View
                  style={{
                    borderRadius: 5,
                    textAlign: 'center',
                    height: 25,
                    marginVertical: 10,
                    flexDirection: 'row',
                    alignItems: 'center',
                    alignSelf: 'center',
                    alignContent: 'center',
                    backgroundColor: '#C6DBF8',
                    width: 100,
                  }}>
                  <View
                    style={{
                      width: 40,
                      alignItems: 'center',
                      alignSelf: 'center',
                      alignContent: 'center',
                    }}>
                    <Feathers size={20} color={'#0367F2'} name={'star'} />
                  </View>
                  <View
                    style={{
                      width: 50,
                      alignItems: 'center',
                      alignSelf: 'center',
                      alignContent: 'center',
                    }}>
                    <FastText others={styles.namber}>
                      {item.calificacion}{' '}
                    </FastText>
                  </View>
                </View>
              </View>
              <View style={{width: '70%'}}>
                <FastText others={styles.profesiontitle}>
                  {item.nombre}
                </FastText>
                <FastText others={styles.profesions}>
                  Experiencia: {item.experiencia} años{' '}
                </FastText>
                <FastText others={styles.profesions}>
                  Edad: {moment().diff(item.fechanacimiento, 'years')}
                  años{' '}
                </FastText>
                <FastText others={styles.profesions}>
                  Capacitado: {item.capacitaciones}{' '}
                </FastText>
              </View>
            </View>
          </View>
          <View style={{width: '100%'}}>
            <CustomDividerpracs />
            <View style={{flexDirection: 'row', width: '100%'}}>
              <FastText others={styles.profesion}>
                Especialiasta: {item.especialiasta}{' '}
              </FastText>
            </View>
          </View>
        </View>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
  },
  profesion: {
    color: '#A4A4A4',
    fontSize: 12,
    textAlign: 'center',
  },
  namber: {
    fontSize: 15,
    color: '#0367F2',
    fontWeight: 'bold',
  },
  profesiontitle: {
    color: '#0367F2',
    fontSize: 18,
    marginLeft: 6,
  },
  profesions: {
    color: 'black',
    fontSize: 15,
    marginLeft: 6,
  },
  cardresult: {
    elevation: 10,
    borderRadius: 10,
    backgroundColor: '#ffffff',
    padding: 10,
    marginHorizontal: 10,
    marginVertical: 10,
    width: '95%',
    height: 170,
  },
  subcot: {
    alignItems: 'center',
    justifyContent: 'center',
  },

  containeraler: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  map: {
    ...StyleSheet.absoluteFillObject,
  },
  headerContainer: {
    flexDirection: 'row',
    padding: 5,
    alignItems: 'flex-start',
  },
  btnVerEnSitio: {
    backgroundColor: '#F5F6F7',
    borderRadius: 20,
    width: '100%',
    padding: '3%',
    color: 'blue',
    borderWidth: 1,
    borderColor: '#4982e6',
  },
  carousel: {
    position: 'absolute',
    bottom: 0,
    marginBottom: 48,
  },
  cardContainer: {
    alignSelf: 'center',
    backgroundColor: '#617792',
    height: 600,
    width: 350,
    padding: 50,
    borderRadius: 24,
  },
  cardImage: {
    height: 120,
    width: 300,
    bottom: 0,
    position: 'absolute',
    borderBottomLeftRadius: 24,
    borderBottomRightRadius: 24,
  },
  button: {
    alignSelf: 'center',

    fontSize: 16,
    color: '#fff',
    backgroundColor: '#617792',
    paddingVertical: 10,
    paddingHorizontal: 15,
    textAlign: 'center',
    height: 50,
    width: 350,
    padding: 50,
    borderRadius: 24,
  },
  cardTitle: {
    color: 'white',
    fontSize: 22,
    alignSelf: 'center',
  },

  profileContainer: {
    color: '#617792',
    padding: 0,
    zIndex: 1,
  },
  profileBackground: {
    width: width,
    height: height / 2,
  },
  profileCard: {
    elevation: 6,
    borderRadius: 10,
    backgroundColor: '#ffffff',
    padding: 10,
    marginHorizontal: 10,
    marginVertical: 10,
  },
  info: {
    paddingHorizontal: 40,
    alignItems: 'center',
  },
  avatarContainer: {
    position: 'relative',
  },
  avatar: {
    width: 124,
    height: 124,
    borderRadius: 62,
    borderWidth: 0,
  },
  nameInfo: {alignItems: 'center', justifyContent: 'center'},
  divider: {
    width: '100%',
    borderWidth: 1,
    borderColor: '#E9ECEF',
  },
  thumb: {
    borderRadius: 4,
    marginVertical: 4,
    alignSelf: 'center',
    width: thumbMeasure,
    height: thumbMeasure,
  },
});
