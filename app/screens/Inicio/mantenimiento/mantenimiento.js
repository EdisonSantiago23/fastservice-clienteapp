import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  ScrollView,
  Text,
  StatusBar,
  Image,
} from 'react-native';
import LoadingProgress from '../../../common/LoadingProgress';
import mecanicoApi from '../../../api/mecanico';
import {Block, theme} from 'galio-framework';
import {List, ListItem} from 'react-native-elements';
import TouchableScale from 'react-native-touchable-scale'; // https://github.com/kohver/react-native-touchable-scale
import LinearGradient from 'react-native-linear-gradient'; // Only if no expo
import {Header} from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import AsyncStorage from '@react-native-community/async-storage';
import Constants from '../../../common/Constants';
export default class mantenimiento extends Component {
  constructor(props) {
    super(props);
    this.mecanicoApi = new mecanicoApi();

    this.state = {
      initSplash: true,
      loading: true,
      nombre: '',
      loginSpl: true,
      mantenimineto: [],
    };
    this.getMantenimiento();
    this.getname();
  }

  getname = async () => {
    var nomb = await AsyncStorage.getItem(Constants.STORE_NAME_USER);
    nomb = nomb.substring(1, nomb.length - 1);
    this.setState({
      nombre: nomb,
    });
  };
  getMantenimiento() {
    this.mecanicoApi.getMantenimiento().then(res => {
      this.setState({
        mantenimineto: res,
      });
      if (res != undefined && res.status != 'fail') {
        this.setState({
          loading: false,
        });
      }
    });
  }

  render() {
    if (this.state.loading) {
      return <LoadingProgress />;
    }
    if (!this.state.loading) {
      return (
        <View>
          <StatusBar backgroundColor="#FF9100" barStyle="light-content" />

          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              marginLeft: '30%',
            }}>
            <View>
              <Image
                source={require('../../../assets/png/lg.png')}
                style={styles.imagen}
              />
              <View>
                <Text
                  style={{fontSize: 18, color: '#617792', fontWeight: 'bold'}}>
                  {' '}
                  MECÁNICA EXPRESS
                </Text>
              </View>
            </View>
          </View>
          <Text style={styles.title}>SELECCIONA EL SERVICIO</Text>
          <Text style={styles.text}>MANTENIMIENTO</Text>
          <ScrollView style={{height: '80%'}}>
            <View>
              {!this.state.mantenimineto.status ? (
                <View>
                  {this.state.mantenimineto.map(emergencia => (
                    <ListItem
                      style={{
                        paddingTop: '1%',
                      }}
                      Component={TouchableScale}
                      friction={90} //
                      tension={100} // These props are passed to the parent component (here TouchableScale)
                      activeScale={0.95} //
                      linearGradientProps={{
                        colors: ['#FFCF8F', '#FF9100'],
                        start: {x: 1, y: 0},
                        end: {x: 0.2, y: 0},
                      }}
                      ViewComponent={LinearGradient} // Only if no expo
                      onPress={() =>
                        this.props.navigation.navigate('tiposmantenimiento', {
                          emergencia: emergencia,
                        })
                      }
                      leftAvatar={{
                        rounded: true,
                        source: require('../../../assets/png/lg.png'),
                      }}
                      title={emergencia.nombre}
                      titleStyle={{color: 'white', fontWeight: 'bold'}}
                      bottomDivider
                      chevron={{color: '#617792'}}
                    />
                  ))}
                </View>
              ) : (
                <View>
                  <Block middle style={styles.nameInfo}>
                    <Text bold size={28} color="#32325D">
                      Sin Emergencias
                    </Text>
                  </Block>
                  <Block middle style={styles.nameInfo}>
                    <Text bold size={28} color="#32325D">
                      Disponibles
                    </Text>
                  </Block>
                </View>
              )}
            </View>
          </ScrollView>
        </View>
      );
    }
  }
  _renderItem = ({item, dimensions}) => (
    <View>
      <Image style={styles.image} source={item.image} />
    </View>
  );
}

const styles = StyleSheet.create({
  image: {
    width: 400,
    height: 200,
  },
  title: {
    fontFamily: 'vagron',
    fontSize: 24,
    color: '#1F4997',
    backgroundColor: 'transparent',
    textAlign: 'center',
    marginVertical: 5,
  },
  text: {
    color: '#73221C',
    backgroundColor: 'transparent',
    textAlign: 'center',
    fontSize: 18,
    paddingHorizontal: 10,
    marginBottom: 10,
  },
  imagen: {
    width: 75,
    height: 75,
    marginLeft: '25%',
  },
  iconContainer: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    width: 80,
  },
  touchable: {
    flex: 1,
    flexDirection: 'row',
  },

  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  contImage: {
    flex: 1,
    marginTop: 3,
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: 2,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
  },
  btnImage: {
    flex: 1,
    justifyContent: 'center',
    paddingBottom: 10,
    alignItems: 'center',
    textAlign: 'center',
  },
});
