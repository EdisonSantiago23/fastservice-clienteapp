import React, {Component} from 'react';
import {
  StyleSheet,
  Dimensions,
  StatusBar,
  Alert,
  View,
  ActivityIndicator,
  ScrollView,
  ImageBackground,
  Image,
  TouchableOpacity,
} from 'react-native';
import {Block, Text, theme} from 'galio-framework';
const {width, height} = Dimensions.get('screen');
const StatusHeight = StatusBar.currentHeight;
const HeaderHeight = theme.SIZES.BASE * 3.5 + (StatusHeight || 0);
const thumbMeasure = (width - 48 - 32) / 3;
import mecanicoApi from '../../../api/mecanico.js';
import AsyncStorage from '@react-native-community/async-storage';
import Constants from '../../../common/Constants';
import Geolocation from '@react-native-community/geolocation';
import {PricingCard, Header} from 'react-native-elements';
import LoadingProgress from '../../../common/LoadingProgress';
import {SCLAlert, SCLAlertButton} from 'react-native-scl-alert';
import { CommonActions } from '@react-navigation/native';

export default class ConfirmarMantenimineto extends Component {
  constructor(props) {
    super(props);
    this.mecanicoApi = new mecanicoApi();

    this.state = {
      initSplash: true,
      loginSpl: true,
      loadingBtn: false,
      visible: true,
      show: false,

      mecanico: this.props.route.params.mecanico,
      emergencia: this.props.route.params.emergencia,
      userFeatures: {
        idmecanico: '',
        idusuario: '',
        url: '',
        latitud: '',
        longitud: '',
        idemergencia: '',
        poi: {
          latitude: '',
          longitude: '',
        },
        region: {
          latitude: 0,
          longitude: 0,
          latitudeDelta: 0,
          longitudeDelta: 0.04,
        },
        tocken: '',
        headings: '',
        content: '',
        imgpequena: '',
        imggrande: '',
        tipo: 0,
      },
    };
    this.getLocationUser();
    console.log('holaa');
  }
  getLocationUser = async () => {
    await Geolocation.getCurrentPosition(
      (position) => {
        const {latitude, longitude} = position.coords;

        this.setState({
          region: {
            longitude: longitude,
            latitude: latitude,
            latitudeDelta: 0.029213524352655895,
            longitudeDelta: (width / height) * 0.029213524352655895,
          },
        });
      },
      (error) => {
        console.log(error);
      },
    );
  };
  handleOpen = () => {
    this.setState({show: true});
  };
  reset() { return this.props.navigation.dispatch(CommonActions.reset({ index: 0, routes: [{ name: 'home' },], })); }

  handleClose = () => {
    this.setState({show: false});
    this.reset();
  };
  createReserva = async () => {
    this.setState({loading: true});
    try {
      this.setState({loadingBtn: true});
      this.state.userFeatures.idmecanico = this.state.mecanico.idmecanico;
      this.state.userFeatures.latitud = this.state.region.latitude;
      this.state.userFeatures.longitud = this.state.region.longitude;
      this.state.userFeatures.idusuario = await AsyncStorage.getItem(
        Constants.STORE_ID_USER,
      );
      this.state.userFeatures.tocken = this.state.mecanico.tocken;
      this.state.userFeatures.idemergencia = this.state.emergencia.idtipoMantenimiento;
      this.state.userFeatures.headings = this.state.emergencia.nombre;
      this.state.userFeatures.tipo = 1;
      this.state.userFeatures.content =
        'Necesito al mecánico ' + this.state.mecanico.nombre;
      this.state.userFeatures.imgpequena = this.state.mecanico.url;
      this.mecanicoApi.crearReserva(this.state.userFeatures).then((res) => {
        console.log(res);
        if (res.idReservaMecanico) {
          this.setState({loading: false, show: true});
        } else {
          if (res.status && res.status == 'error') {
            Alert.alert('Error', res.message, [{text: 'Cerrar'}]);
            this.setState({loading: false});
          }
        }
      });
    } catch (err) {
      Alert.alert('Error', err.message, [{text: 'Cerrar'}]);
    }
  };
  render() {
    if (this.state.loading) {
      return <LoadingProgress />;
    }
    if (!this.state.loading) {
      return (
        <View>
          <StatusBar backgroundColor="#FF9100" barStyle="light-content" />

          <ImageBackground
            style={styles.profileContainer}
            imageStyle={styles.profileBackground}>
            <SCLAlert
              theme="success"
              show={this.state.show}
              title="Correcto"
              onRequestClose={this.handleClose}
              cancellable={true}
              subtitle="Reserva exitosa">
              <SCLAlertButton theme="success" onPress={this.handleClose}>
                Ok
              </SCLAlertButton>
            </SCLAlert>
            <ScrollView
              showsVerticalScrollIndicator={false}
              style={{width, marginTop: '5%'}}>
              <View style={styles.container2}>
                <Image
                  source={{uri: Constants.BASEURI + this.state.mecanico.imagen}}
                  style={styles.avatar}
                />
              </View>
              <PricingCard
                color="#FF9100"
                title={this.state.emergencia.nombre}
                price={'$' + this.state.emergencia.precio}
                info={['1 Mecánico', 'Mantenimiento', 'MECÁNICA EXPRESS']}
                button={{title: 'CONFIRMAR', icon: 'flight-takeoff'}}
                onButtonPress={this.createReserva}
              />
            </ScrollView>
          </ImageBackground>
        </View>
      );
    }
  }
  renderBtn() {
    if (!this.state.loadingBtn) {
      return (
        <View>
          <TouchableOpacity
            onPress={() => {
              this.reserva();
            }}
            style={styles.btnVerEnSitio}>
            <Text style={{fontSize: 14, textAlign: 'center', color: '#1F4997'}}>
              CONTINUAR
            </Text>
          </TouchableOpacity>
        </View>
      );
    } else {
      return (
        <TouchableOpacity style={styles.btnVerEnSitio}>
          <ActivityIndicator size="small" color={'#1F4997'} />
        </TouchableOpacity>
      );
    }
  }
}
const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
  },
  profile: {
    marginTop: Platform.OS === 'android' ? -HeaderHeight : 0,
    // marginBottom: -HeaderHeight * 2,
    flex: 1,
  },
  map: {
    ...StyleSheet.absoluteFillObject,
  },
  headerContainer: {
    flexDirection: 'row',
    padding: 5,
    alignItems: 'flex-start',
  },
  btnVerEnSitio: {
    backgroundColor: '#F5F6F7',
    borderRadius: 20,
    width: '100%',
    padding: '3%',
    color: 'blue',
    borderWidth: 1,
    borderColor: '#4982e6',
  },
  container2: {
    alignItems: 'center',
    padding: '3%',
  },
  carousel: {
    position: 'absolute',
    bottom: 0,
    marginBottom: 48,
  },
  cardContainer: {
    alignSelf: 'center',
    backgroundColor: '#617792',
    height: 600,
    width: 350,
    padding: 50,
    borderRadius: 24,
  },
  cardImage: {
    height: 120,
    width: 300,
    bottom: 0,
    position: 'absolute',
    borderBottomLeftRadius: 24,
    borderBottomRightRadius: 24,
  },
  button: {
    alignSelf: 'center',

    fontSize: 16,
    color: '#fff',
    backgroundColor: '#617792',
    paddingVertical: 10,
    paddingHorizontal: 15,
    textAlign: 'center',
    height: 50,
    width: 350,
    padding: 50,
    borderRadius: 24,
  },
  cardTitle: {
    color: 'white',
    fontSize: 22,
    alignSelf: 'center',
  },
  profile: {
    marginTop: Platform.OS === 'android' ? -HeaderHeight : 0,
    // marginBottom: -HeaderHeight * 2,
    flex: 1,
  },
  profileContainer: {
    width: width,
    height: height,
    color: '#617792',
    padding: 0,
    zIndex: 1,
  },
  profileBackground: {
    width: width,
    height: height / 2,
  },
  profileCard: {
    // position: "relative",
    padding: theme.SIZES.BASE,
    marginHorizontal: theme.SIZES.BASE,
    marginTop: 65,
    borderTopLeftRadius: 6,
    borderTopRightRadius: 6,
    backgroundColor: theme.COLORS.WHITE,
    shadowColor: 'black',
    shadowOffset: {width: 0, height: 0},
    shadowRadius: 8,
    shadowOpacity: 0.2,
    zIndex: 2,
  },
  info: {
    paddingHorizontal: 40,
  },
  avatarContainer: {
    position: 'relative',
    marginTop: -80,
  },
  avatar: {
    width: 124,
    height: 124,
    borderRadius: 62,
    borderWidth: 0,
  },
  nameInfo: {
    marginTop: 20,
  },
  divider: {
    width: '90%',
    borderWidth: 1,
    borderColor: '#E9ECEF',
  },
  thumb: {
    borderRadius: 4,
    marginVertical: 4,
    alignSelf: 'center',
    width: thumbMeasure,
    height: thumbMeasure,
  },
});
