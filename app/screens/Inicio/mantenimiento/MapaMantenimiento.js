import React, {Component} from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  Alert,
  Dimensions,
  StatusBar,
  View,
  FlatList,
  Image,
} from 'react-native';
import mecanicoApi from '../../../api/mecanico.js';
import {Text} from 'galio-framework';
import Geolocation from '@react-native-community/geolocation';
import MapView, {Marker} from 'react-native-maps';
import frelance from '../../../assets/free.png';
import local from '../../../assets/car.png';
import AsyncStorage from '@react-native-community/async-storage';
import Constants from '../../../common/Constants';
import LoadingProgress from '../../../common/LoadingProgress';
import Icon from 'react-native-vector-icons/FontAwesome';
var {height, width} = Dimensions.get('window');
const CARD_HEIGHT = height / 3.5;
const CARD_WIDTH = CARD_HEIGHT - 50;

import {SCLAlert, SCLAlertButton} from 'react-native-scl-alert';
export default class MapaMantenimiento extends Component {
  constructor(props) {
    super(props);
    this.mecanicoApi = new mecanicoApi();

    this.state = {
      isVisible: false,
      locales: [],
      tockens: [],
      loading: true,
      detalle: this.props.route.params.detalle,
      idtipo: this.props.route.params.idtipo,
      show: false,
      showaler: false,

      poi: {
        latitude: '',
        longitude: '',
      },
      region: {
        latitude: 0,
        longitude: 0,
        latitudeDelta: 0,
        longitudeDelta: 0.04,
      },
      markers: [],
    };
    this.getLocationUser();
  }
  handleOpen = () => {
    this.setState({showaler: true});
  };

  handleClose = () => {
    this.setState({showaler: false});
    this.props.navigation.navigate('MenuAuxilioMotos');
  };
  getLocationUser = async () => {
    await Geolocation.getCurrentPosition(
      (position) => {
        const {latitude, longitude} = position.coords;
        this.state.poi.latitude = position.coords.latitude;
        this.state.poi.longitude = position.coords.longitude;
        this.setState({
          region: {
            longitude: longitude,
            latitude: latitude,
            latitudeDelta: 0.029213524352655895,
            longitudeDelta: (width / height) * 0.029213524352655895,
          },
        });
        this.getLocales(latitude, longitude, this.state.idtipo);
      },
      (error) => {
        console.log(error);
      },
    );
  };

  createReserva = async () => {
    try {
      this.setState({loadingBtn: true});
      this.state.repuesto.idUsuario = await AsyncStorage.getItem(
        Constants.STORE_ID_USER,
      );
      this.state.repuesto.latitud = this.state.region.latitude;
      this.state.repuesto.longitud = this.state.region.longitude;

      this.mecanicoApi.crearReservaRepuesto(this.state.repuesto).then((res) => {
        if (res.idReservaRepuesto) {
          this.setState({loadingBtn: false, show: true});
        } else {
          if (res.status && res.status == 'error') {
            Alert.alert('Error', res.message, [{text: 'Cerrar'}]);
            this.setState({loadingBtn: false});
          }
        }
      });
    } catch (err) {
      Alert.alert('Error', err.message, [{text: 'Cerrar'}]);
    }
  };

  getLocales(lat, long, tipo) {
    this.mecanicoApi.getLocalesTipo(lat, long, tipo).then((res) => {
      var aux = Object.values(res).sort(function (a, b) {
        return a.distancia - b.distancia;
      });
      if (res.status == 'error') {
        this.setState({
          loading: false,

          showaler: true,
        });
      } else {
        this.setState({
          locales: aux,
          loading: false,
        });
      }
    });
  }
  _renderItem(item) {
    return (
      <View style={styles.card}>
        <Text style={{fontSize: 15, color: '#000000', textAlign: 'center'}}>
          {item.nombre}
        </Text>

        <View
          style={{
            width: '100%',
            flexDirection: 'row',
            justifyContent: 'space-between',
          }}>
          <Text style={{fontSize: 15, color: '#000000'}}>Dirección:</Text>
          <Text style={{fontSize: 15, color: '#000000'}}>{item.direccion}</Text>
        </View>
        <View
          style={{
            width: '100%',
            flexDirection: 'row',
            justifyContent: 'space-between',
          }}>
          <Text style={{fontSize: 15, color: '#000000'}}>Tiempo:</Text>
          <Text style={{color: '#FF9100'}}>{item.tiempo}</Text>
        </View>
        <View style={{width: '100%', height: '100%', flexDirection: 'row'}}>
          <Image
            source={{uri: Constants.BASEURI + item.imagen}}
            style={styles.cardImage}
            resizeMode="cover"
          />
          <TouchableOpacity
            style={styles.buy}
            onPress={() =>
              this.props.navigation.navigate('MecanicoMantenimiento', {
                local: item,
                emergencia: this.state.detalle,
              })
            }>
            <Icon
              name="arrow-right"
              type="Ionicons"
              size={30}
              color="#FF9100"
            />
          </TouchableOpacity>
        </View>
      </View>
    );
  }
  render() {
    if (this.state.loading) {
      return <LoadingProgress />;
    }
    if (!this.state.loading) {
      return (
        <View style={{height: '100%'}}>
          <StatusBar backgroundColor="#FF9100" barStyle="light-content" />
          <SCLAlert
            theme="danger"
            show={this.state.showaler}
            title="Error"
            onRequestClose={this.handleClose}
            cancellable={true}
            subtitle="No disponible">
            <SCLAlertButton theme="danger" onPress={this.handleClose}>
              Ok
            </SCLAlertButton>
          </SCLAlert>

          <View style={{width: '100%', height: '100%'}}>
            <MapView
              style={styles.map}
              mapType={'standard'}
              initialCamera={{
                center: {
                  latitude: this.state.region.latitude,
                  longitude: this.state.region.longitude,
                },
                pitch: 45,
                heading: 90,
                altitude: 1000,
                zoom: 17,
              }}
              loadingEnabled
              loadingIndicatorColor="#666666"
              loadingBackgroundColor="#eeeeee"
              showsIndoors
              showsIndoorLevelPicker
              zoomEnabled={true}
              pitchEnabled={true}
              showsUserLocation={true}
              followsUserLocation={true}
              showsCompass={true}
              showsBuildings={true}
              showsTraffic={true}
              showsIndoors={true}
              initialRegion={this.state.region}
              zoomEnabled={true}>
              {this.state.locales.map((marker) => (
                <Marker
                  title={marker.nombre}
                  image={marker.localtype == 0 ? local : frelance}
                  coordinate={marker.coordinate[0]}
                />
              ))}
            </MapView>
            {this.renderParkings()}
          </View>
        </View>
      );
    }
  }
  renderParkings() {
    return (
      <FlatList
        horizontal
        scrollEnabled
        centerContent
        showsHorizontalScrollIndicator={false}
        style={[styles.parkings, styles.shadow]}
        data={this.state.locales}
        keyExtractor={(item) => item.id.toString()}
        renderItem={({item}) => this._renderItem(item)}
      />
    );
  }
}

const styles = StyleSheet.create({
  contImage: {
    marginTop: 3,
    justifyContent: 'center',
    borderWidth: 2,
    width: '100%',
    borderColor: '#4982e6',
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
  },
  parking: {
    flexDirection: 'row',
    backgroundColor: 'white',
    borderRadius: 6,
    padding: 12,
    marginHorizontal: 12,
    width: width - 12 * 2,
  },
  parkings: {
    position: 'absolute',
    right: 0,
    left: 0,
    bottom: 0,
    paddingBottom: 24,
  },
  card: {
    backgroundColor: 'white',
    borderRadius: 6,
    padding: 12,
    marginHorizontal: 12,
    width: width - 50 * 2,
    height: CARD_HEIGHT,
  },
  title: {
    fontFamily: 'vagron',
    fontSize: 24,
    color: '#1F4997',
    backgroundColor: 'transparent',
    textAlign: 'center',
    marginVertical: 5,
  },
  text: {
    color: '#73221C',
    backgroundColor: 'transparent',
    textAlign: 'center',
    fontSize: 18,
    paddingHorizontal: 10,
    marginBottom: 10,
  },
  btnStyle: {
    alignItems: 'center',
    borderWidth: 1,
    borderColor: '#4982e6',
    borderRadius: 20,
    justifyContent: 'center',
    backgroundColor: '#F5F6F7',
    height: 50,
  },
  map: {
    width: '100%',
    height: '100%',
  },
  carousel: {
    position: 'absolute',
    bottom: 0,
    marginBottom: 48,
  },
  imagenauxlio: {
    width: 70,
    height: 70,
  },
  imagen: {
    width: 45,
    height: 30,
  },
  cardContainer: {
    backgroundColor: 'rgba(0, 0, 0, 0.6)',
    height: 200,
    width: 300,
    padding: 24,
    borderRadius: 24,
  },
  cardImage: {
    width: '80%',
    height: '60%',
  },
  cardTitle: {
    color: 'white',
    fontSize: 22,
    marginTop: -30,
    alignSelf: 'center',
  },
});
