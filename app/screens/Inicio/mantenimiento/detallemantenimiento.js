import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  ScrollView,
  Image,
  StatusBar,
} from 'react-native';
import LoadingProgress from '../../../common/LoadingProgress';
import mecanicoApi from '../../../api/mecanico';
import { Text} from 'galio-framework';
import {SectionGrid} from 'react-native-super-grid';
import Icon from 'react-native-vector-icons/FontAwesome';
export default class detallemantenimiento extends Component {
  constructor(props) {
    super(props);
    this.mecanicoApi = new mecanicoApi();
    this.state = {
      initSplash: true,
      loading: true,
      tipomantenimiento: this.props.route.params.tipomantenimiento,

      loginSpl: true,
      detalle: [],
    };
    this.GetDetalle(this.state.tipomantenimiento.idtipoMantenimiento);
  }

  GetDetalle(idtipoMantenimiento) {
    this.mecanicoApi.GetDetalles(idtipoMantenimiento).then(res => {
      this.setState({
        detalle: res,
      });
      if (res != undefined && res.status != 'fail') {
        this.setState({
          loading: false,
        });
      }
    });
  }

  render() {
    if (this.state.loading) {
      return <LoadingProgress />;
    }
    if (!this.state.loading) {
      return (
        <View>
          <StatusBar backgroundColor="#FF9100" barStyle="light-content" />

          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              marginLeft: '30%',
            }}>
            <View>
              <Image
                source={require('../../../assets/png/lg.png')}
                style={styles.imagen}
              />
              <View>
                <Text
                  style={{fontSize: 18, color: '#617792', fontWeight: 'bold'}}>
                  {' '}
                  MECÁNICA EXPRESS
                </Text>
              </View>
            </View>
          </View>
          <Text style={styles.title}>LOS SERVICIO QUE INCLUYE</Text>
          <Text style={styles.text}>
            POR $ {this.state.tipomantenimiento.precio}
          </Text>

          <View style={styles.containerdtn}>
            <TouchableOpacity
              style={styles.btnVerEnSitioL}
              onPress={() =>
                this.props.navigation.navigate('MapaMantenimiento', {
                  idtipo: 0,
                  detalle: this.state.tipomantenimiento,
                })
              }>
              <View style={styles.containerdtn}>
                <Icon raised name="search" color="#617792" size={20} />
                <Text style={{textAlign: 'center', marginLeft: '10%'}}>
                  LOCALES
                </Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.btnVerEnSitioR}
              onPress={() =>
                this.props.navigation.navigate('MapaMantenimiento', {
                  idtipo: 3,
                  detalle: this.state.tipomantenimiento,
                })
              }>
              <View style={styles.containerdtn}>
                <Icon raised name="search" color="#617792" size={20} />
                <Text style={{textAlign: 'center', marginLeft: '10%'}}>
                  FREELANCE
                </Text>
              </View>
            </TouchableOpacity>
          </View>
          <ScrollView style={{height: '60%'}}>
            <SectionGrid
              itemDimension={130}
              sections={[
                {
                  data: this.state.detalle,
                },
              ]}
              renderItem={({item}) => (
                <View
                  style={[styles.itemContainer, {backgroundColor: '#f1c40f'}]}>
                  <Icon raised name="wrench" color="#617792" size={40} />
                  <Text style={styles.itemName}>{item.nombre}</Text>
                </View>
              )}
            />
          </ScrollView>
        </View>
      );
    }
  }
}

const styles = StyleSheet.create({
  image: {
    width: 400,
    height: 200,
  },
  containerdtn: {
    bottom: 0,
    alignItems: 'center',
    flexDirection: 'row',
  },
  btnVerEnSitioL: {
    backgroundColor: '#F5F6F7',
    borderRadius: 20,
    alignItems: 'center',
    width: '50%',
    padding: '3%',
    color: 'blue',
    borderWidth: 1,
    marginTop: '5%',
    borderColor: '#4982e6',
  },
  btnVerEnSitioR: {
    backgroundColor: '#F5F6F7',
    borderRadius: 20,
    alignItems: 'center',
    width: '50%',
    padding: '3%',
    color: 'blue',
    borderWidth: 1,
    marginTop: '5%',
    borderColor: '#4982e6',
  },
  title: {
    fontFamily: 'vagron',
    fontSize: 24,
    color: '#1F4997',
    backgroundColor: 'transparent',
    textAlign: 'center',
    marginVertical: 5,
  },
  text: {
    color: '#73221C',
    backgroundColor: 'transparent',
    textAlign: 'center',
    fontSize: 18,
    paddingHorizontal: 10,
    marginBottom: 10,
  },
  gridView: {
    marginTop: 10,
    flex: 1,
  },
  itemContainerloco: {
    justifyContent: 'flex-end',
    borderRadius: 5,
    padding: 10,
    height: 150,
  },

  itemContainer: {
    justifyContent: 'flex-end',
    alignItems: 'center',
    borderRadius: 5,
    padding: 10,
    height: 150,
  },
  itemName: {
    fontSize: 16,
    color: '#617792',
    fontWeight: '600',
    fontWeight: 'bold',
  },
  itemCode: {
    fontWeight: '600',
    fontSize: 12,
    color: '#fff',
  },
  imagen: {
    width: 75,
    height: 75,
    marginLeft: '25%',
  },
  iconContainer: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    width: 80,
  },
  touchable: {
    flex: 1,
    flexDirection: 'row',
  },

  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  contImage: {
    flex: 1,
    marginTop: 3,
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: 2,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
  },
  btnImage: {
    flex: 1,
    justifyContent: 'center',
    paddingBottom: 10,
    alignItems: 'center',
    textAlign: 'center',
  },
});
