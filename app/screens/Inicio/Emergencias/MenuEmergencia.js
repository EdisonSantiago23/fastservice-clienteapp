import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  StatusBar,
  Image,
  ScrollView,
} from 'react-native';
import LoadingProgress from '../../../common/LoadingProgress';
import mecanicoApi from '../../../api/mecanico';
import {Block, Text, } from 'galio-framework';
import AsyncStorage from '@react-native-community/async-storage';
import Constants from '../../../common/Constants';
import { Card, Paragraph} from 'react-native-paper';

export default class MenuEmergencias extends Component {
  constructor(props) {
    super(props);
    this.mecanicoApi = new mecanicoApi();

    this.state = {
      initSplash: true,
      loading: true,
      nombre: '',
      loginSpl: true,
      emergencias: [],
    };
    this.getEmergencias();
    this.getname();
  }
  getname = async () => {
    var nomb = await AsyncStorage.getItem(Constants.STORE_NAME_USER);
    nomb = nomb.substring(1, nomb.length - 1);
    this.setState({
      nombre: nomb,
    });
  };
  getEmergencias() {
    this.mecanicoApi.getEmergencias().then(res => {
      var aux = Object.values(res).sort(function(a, b) {
        return a.distancia - b.distancia;
      });

      this.setState({
        emergencias: aux,
      });
      if (res != undefined && res.status != 'fail') {
        this.setState({
          loading: false,
        });
      }
    });
  }

  render() {
    if (this.state.loading) {
      return <LoadingProgress />;
    }
    if (!this.state.loading) {
      return (
        <View style={{height: '100%', width: '100%'}}>
      
          <StatusBar backgroundColor="#FF9100" barStyle="light-content" />

          {!this.state.emergencias.status ? (
            <View
              style={{
                width: '100%',
                height: '100%',
              }}>
              <ScrollView style={{height: '80%', width: '100%'}}>
                {this.state.emergencias.map(emergencia => (
                  <TouchableOpacity
                    onPress={() =>
                      this.props.navigation.navigate('Mapa', {
                        emergencia: emergencia,
                      })
                    }
                    style={styles.btnImage}>
                    <View style={styles.contImage}>
                      <Card>
                        <Card.Title title={emergencia.nombre} />
                        <Card.Cover source={{uri: emergencia.url}} />

                        <Card.Content>
                          <Paragraph>Precio:${emergencia.precio}</Paragraph>
                        </Card.Content>
                      </Card>
                    </View>
                  </TouchableOpacity>
                ))}
              </ScrollView>
            </View>
          ) : (
            <View>
              <Block middle style={styles.nameInfo}>
                <Text bold size={28} color="#32325D">
                  Sin Emergencias
                </Text>
              </Block>
              <Block middle style={styles.nameInfo}>
                <Text bold size={28} color="#32325D">
                  Disponibles
                </Text>
              </Block>
            </View>
          )}
        </View>
      );
    }
  }
  _renderItem = ({item, dimensions}) => (
    <View>
      <Image style={styles.image} source={item.image} />
    </View>
  );
}

const styles = StyleSheet.create({
  image: {
    width: 400,
    height: 200,
  },
  iconContainer: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    width: 80,
  },
  touchable: {
    flex: 1,
    flexDirection: 'row',
  },

  container: {
    flex: 1,
    alignItems: 'center',
    width: '100%',
    alignContent: 'center',
  },
  contImage: {
    elevation: 6,
    borderRadius: 10,
    backgroundColor: '#ffffff',
    padding: 10,
    marginHorizontal: 10,
    marginVertical: 10,
    minWidth: '90%',
    minHeight: 200,
  },
  btnImage: {
    flex: 1,
    justifyContent: 'center',
    paddingBottom: 10,
    alignItems: 'center',
    textAlign: 'center',
  },
});
