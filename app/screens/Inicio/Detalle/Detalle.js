import React, { Component } from 'react';
import {
    StyleSheet, Dimensions, StatusBar
    , ScrollView, View, Image, TouchableOpacity
} from 'react-native';
import { Block, Text, theme } from "galio-framework";
const { width, height } = Dimensions.get("screen");
const StatusHeight = StatusBar.currentHeight;
const HeaderHeight = (theme.SIZES.BASE * 3.5 + (StatusHeight || 0));
const thumbMeasure = (width - 48 - 32) / 3;
import { Overlay, Header } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';

export default class Detalle extends Component {
    constructor(props) {
        super(props);
        this.state = {
            initSplash: true,
            loginSpl: true,

        };
    }

    render() {
        return (
            <View
                style={styles.profileContainer}
                imageStyle={styles.profileBackground}
            >
          
                <Block middle style={styles.avatarContainer}>
                    <Image
                        source={require('../../../assets/png/lg.png')}
                        style={styles.avatar}
                    />
                    <Text
                        size={20}
                        color="#525F7F"
                        style={{ textAlign: "center", marginTop: 10 }}
                    >
                        MECÁNICA EXPRESS
                              </Text>
                </Block>
                <ScrollView style={{ height: '80%' }}>

                    <Block flex style={styles.profileCard}>
                        <Block flex>
                            <Block middle style={styles.nameInfo}>
                                <Text bold size={20} color="#32325D" style={{ textAlign: "center" }}>EMERGENCIA</Text>
                            </Block>
                            <Block middle style={{ marginTop: 5, marginBottom: 16 }}>
                                <Block style={styles.divider} />
                            </Block>
                            <Block middle>
                                <Text size={16} color="#525F7F" style={{ textAlign: "justify" }} >
                                    En caso de inmovilización del vehículo o motocicleta a consecuencia de una avería, accidente o robo parcial.</Text>
                            </Block>
                        </Block>
                    </Block>
                    <Block flex style={styles.profileCard}>
                        <Block flex>
                            <Block middle style={styles.nameInfo}>
                                <Text bold size={20} color="#32325D" style={{ textAlign: "center" }}>REPUESTOS</Text>
                            </Block>
                            <Block middle style={{ marginTop: 5, marginBottom: 16 }}>
                                <Block style={styles.divider} />
                            </Block>
                            <Block middle>
                                <Text size={16} color="#525F7F" style={{ textAlign: "justify" }} >
                                    Tenemos nuestro almacén, gestión estricta. Centrarse en el diseño y desarrollo de Repuestos y accesorios de motos durante 20 años.  Venta directa de fábrica.
                                    </Text>
                            </Block>
                        </Block>
                    </Block>
                    <Block flex style={styles.profileCard}>
                        <Block flex>
                            <Block middle style={styles.nameInfo}>
                                <Text bold size={20} color="#32325D" style={{ textAlign: "center" }}>MANTENIMIENTO</Text>
                            </Block>
                            <Block middle style={{ marginTop: 5, marginBottom: 16 }}>
                                <Block style={styles.divider} />
                            </Block>
                            <Block middle>
                                <Text size={16} color="#525F7F" style={{ textAlign: "justify" }} >
                                    Los mecánicos de motocicletas realizan los servicios técnicos y de reparación de motocicletas, desde motos de ciudad hasta motos con altas prestaciones.                                    </Text>
                            </Block>
                        </Block>
                    </Block>
                    <Block flex style={styles.profileCard}>
                        <Block flex>
                            <Block middle style={styles.nameInfo}>
                                <Text bold size={20} color="#32325D" style={{ textAlign: "center" }}>SERVICIO AUTORIZADO</Text>
                            </Block>
                            <Block middle style={{ marginTop: 10, marginBottom: 16 }}>
                                <Block style={styles.divider} />
                            </Block>
                            <Block middle>
                                <Text size={16} color="#525F7F" style={{ textAlign: "justify" }} >
                                    Seguimiento y gestion de tus garantias, con nuestros locales calificados para atender tu necesidad                          </Text>
                            </Block>
                        </Block>
                    </Block>

                </ScrollView>
            </View>

        );
    }
}
const styles = StyleSheet.create({
    container: {
        ...StyleSheet.absoluteFillObject
    },
    profile: {
        marginTop: Platform.OS === "android" ? -HeaderHeight : 0,
        // marginBottom: -HeaderHeight * 2,
        flex: 1
    },
    map: {
        ...StyleSheet.absoluteFillObject
    },
    headerContainer: {
        flexDirection: 'row',
        padding: 5,
        alignItems: 'flex-start',
    },
    btnVerEnSitio: {
        backgroundColor: '#F5F6F7',
        borderRadius: 20,
        width: '100%',
        padding: '3%',
        color: 'blue',
        borderWidth: 1,
        borderColor: '#4982e6',
    },
    carousel: {
        position: 'absolute',
        bottom: 0,
        marginBottom: 48
    },
    cardContainer: {
        alignSelf: 'center',
        backgroundColor: '#617792',
        height: 600,
        width: 350,
        padding: 50,
        borderRadius: 24
    },
    cardImage: {
        height: 120,
        width: 300,
        bottom: 0,
        position: 'absolute',
        borderBottomLeftRadius: 24,
        borderBottomRightRadius: 24
    },
    button: {
        alignSelf: 'center',

        fontSize: 16,
        color: '#fff',
        backgroundColor: '#617792',
        paddingVertical: 10,
        paddingHorizontal: 15,
        textAlign: "center",
        height: 50,
        width: 350,
        padding: 50,
        borderRadius: 24
    },
    cardTitle: {
        color: 'white',
        fontSize: 22,
        alignSelf: 'center'
    },
    profile: {
        marginTop: Platform.OS === "android" ? -HeaderHeight : 0,
        // marginBottom: -HeaderHeight * 2,
        flex: 1
    },
    profileContainer: {
        width: '100%',
        height: '100%',
        color: '#617792',
        padding: 0,
        zIndex: 1
    },
    profileBackground: {
        width: width,
        height: height / 2
    },
    profileCard: {
        // position: "relative",
        padding: theme.SIZES.BASE,
        marginHorizontal: theme.SIZES.BASE,
        marginTop: 10,
        borderTopLeftRadius: 6,
        borderTopRightRadius: 6,
        backgroundColor: theme.COLORS.WHITE,
        shadowColor: "black",
        shadowOffset: { width: 0, height: 0 },
        shadowRadius: 8,
        shadowOpacity: 0.2,
        zIndex: 2
    },
    info: {
        paddingHorizontal: 40
    },
    avatarContainer: {
        position: "relative",
        marginTop:'5%'
    },
    avatar: {
        width: 100,
        height: 100,
        borderRadius: 62,
        borderWidth: 0
    },
    nameInfo: {
        marginTop: 35,
        textAlign: 'center'
    },
    divider: {
        width: "90%",
        borderWidth: 1,
        borderColor: "#E9ECEF"
    },
    thumb: {
        borderRadius: 4,
        marginVertical: 4,
        alignSelf: "center",
        width: thumbMeasure,
        height: thumbMeasure
    }
});

