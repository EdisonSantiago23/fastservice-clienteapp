import React from 'react';
import {Text} from 'react-native';
import {Input} from 'react-native-elements';

export default function ExoInputextends(props) {
  return (
    <Input
      placeholder={props.placeholder}
      style={[{width: '100%', backgroundColor: '#ccc'}, props.others]}
      keyboardType={'numeric'}
      onChangeText={(value) => this.setState({comment: value})}
    />
  );
}
