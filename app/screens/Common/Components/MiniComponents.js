import React from 'react';
import {TouchableOpacity, View} from 'react-native';
import Colors from '../../../Common/Colors';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import {useNavigation} from '@react-navigation/native';

export function As(props) {
  return (
    <View
      style={{
        backgroundColor: '#1359EE',
        width: 10,
        height: 10,
        borderRadius: 40,
      }}
    />
  );
}

export function CustomDivider(props) {
  return (
    <View
      style={{
        borderBottomColor: '#ddd',
        width: '100%',
        borderBottomWidth: 1,
        alignItems: 'center',
      }}>
      <View style={{width: '20%', borderWidth: 2, borderColor: Colors.BTN_BLUE}} />
    </View>
  );
}
export function CustomDividerLT(props) {
  return (
    <View
      style={{
        borderBottomColor: '#ddd',
        width: '100%',
        borderBottomWidth: 1,
      }}>
      <View style={{width: '15%', borderWidth: 2, borderColor: Colors.BTN_BLUE}} />
    </View>
  );
}
export function CustomDividerColor(props) {
  return <View style={{width: 30, borderWidth: 2,backgroundColor:props.color, borderRadius: 40, borderColor: props.color}} />;
}

export function CustomDividerpracs(props) {
  return (
    <View
      style={{
        borderBottomColor: '#ddd',
        width: '100%',
        borderBottomWidth: 1,
        alignItems: 'center',
      }}
    />
  );
}
export function BackButton(props) {
  const navigation = useNavigation();
  return (
    <TouchableOpacity
      onPress={() =>  {
        try {
          navigation.goBack();
        }catch (e){
          console.log(e);
        }
      }}
      style={{
        left: 0,
        margin: 0,
        backgroundColor: 'transparent',
        zIndex: 999,
        position: 'absolute',
        top: 0,
      }}>
      <MaterialIcons name="keyboard-arrow-left" color={props.color ? Colors.BTN_BLUE : '#fff'} size={40} />
    </TouchableOpacity>
  );
}
