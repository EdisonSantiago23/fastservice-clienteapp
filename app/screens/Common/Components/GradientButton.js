import React from 'react';
import {ActivityIndicator, StyleSheet, TouchableOpacity} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import FastText from './FastText';
import PropTypes from 'prop-types';

export default function GradientButton(props) {
  let colorDelBoton = props.enabled? ['#FF9100', '#FF9100'] : ['#CDE5FC', '#BFDEFC'];

  return (
    <TouchableOpacity disabled={!props.enabled} onPress={props.onPress} style={[{width: '100%'}, props.others]}>
      <LinearGradient colors={colorDelBoton} style={styles.linearGradient}>
        {props.loading ? (
          <ActivityIndicator color={'#fff'} style={{height: 45}} size={'small'} />
        ) : (
          <FastText others={styles.buttonText} font={'bold'}>
            {props.label ? props.label : 'Aceptar'}
          </FastText>
        )}
      </LinearGradient>
    </TouchableOpacity>
  );
}
export function ButtonNext(props) {
  return (
    <TouchableOpacity onPress={props.onPress} style={[{ width: '100%' }, props.others]}>
      <LinearGradient colors={['#38B6FF', '#38B6FF']} style={styles.linearGradient}>
          <FastText others={styles.buttonText} font={'bold'}>
            {props.label ? props.label : 'EMPEZAR'}
          </FastText>
     

      </LinearGradient>
    </TouchableOpacity>
  );
}
const styleType = PropTypes.oneOfType([PropTypes.object, PropTypes.number]);
GradientButton.propTypes = {
  onPress: PropTypes.func,
  others: styleType,
  loading: PropTypes.bool,
  label: PropTypes.string,
};

var styles = StyleSheet.create({
  linearGradient: {
    paddingLeft: 15,
    paddingRight: 15,
    borderRadius: 50,
  },
  buttonText: {
    fontSize: 15,
    textAlign: 'center',
    margin: 10,
    color: '#ffffff',
    backgroundColor: 'transparent',
  },
});
