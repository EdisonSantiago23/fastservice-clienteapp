import React from 'react';
import {StyleSheet, TouchableOpacity, View} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import FastText from './FastText';


export default function GradientButtonCancelar(props) {
  return (
    <TouchableOpacity onPress={props.onPress} style={[{width: '32%'}, props.others]}>
      <LinearGradient colors={['#fff', '#fff']} style={styles.nocheck}>
        <FastText others={styles.buttonText} font={'bold'}>
          {props.label ? props.label : 'Aceptar'}
        </FastText>
      </LinearGradient>
    </TouchableOpacity>
  );
}
var styles = StyleSheet.create({
  linearGradient: {
    paddingLeft: 15,
    paddingRight: 15,
    borderRadius: 50,
    borderColor: '#FF9100',
    borderStyle: 'dashed',
    borderWidth: 1,
  },

  btnVerEnSitio: {
    borderRadius: 20,
    borderWidth: 1,
    padding: 5,
    borderColor: '#FF9100',
  },
  nocheck: {
    borderRadius: 50,
    borderColor: '#FF9100',
    borderWidth: 1,
  },
  check: {
    paddingLeft: 15,
    paddingRight: 15,
    borderRadius: 50,
    borderColor: '#FF9100',
    borderWidth: 1,
  },
  botonEdit: {
    fontSize: 14,
    textAlign: 'center',
    color: '#1F4997',
  },
  buttonText: {
    fontSize: 13,
    margin: 10,
    color: '#0367F2',
    textAlign: 'center',
  },
});
