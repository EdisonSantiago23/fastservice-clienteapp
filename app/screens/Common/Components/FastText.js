



import React from 'react';
import {Text} from 'react-native';
import PropTypes from 'prop-types';

export default class FastText extends React.Component {
  render() {
    return (
      <Text
        style={[
          {
            fontFamily: this.props.bold ? 'GlacialIndifference-Bold' : 'GlacialIndifference-Regular',
          },
          this.props.others,
        ]}>
        {this.props.children}
      </Text>
    );
  }
}
const styleType = PropTypes.oneOfType([PropTypes.object, PropTypes.number]);
FastText.propTypes = {
  others: styleType,
  bold: PropTypes.bool,
};
