import React from 'react';
import {StyleSheet, Image, View, ScrollView} from 'react-native';
import {BackButton} from '../MiniComponents';
import {SafeAreaView} from 'react-native-safe-area-context';
import ExoText from '../Components/FastText';

function Container(props) {
  return (
    <SafeAreaView style={{flex: 1}}>
      <ScrollView style={{flex: 1}} showsVerticalScrollIndicator={false}>
        <View style={styles.itemsContainer}>
          <BackButton color />

            <ExoText others={styles.Agregatuprimerservicio} font={'bold'}>
              {props.title}
            </ExoText>
       
        </View>
        <View style={styles.container}>{props.children}</View>
      </ScrollView>
    </SafeAreaView>
  );
}

export default Container;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    marginHorizontal: 30,
    marginTop: 20,
  },
  container2: {
    flexDirection: 'row',
    alignContent: 'center',
    alignSelf: 'center',
    alignItems: 'center',
  },
  itemsContainer: {
    width: '100%',
    alignItems: 'center',
    alignContent: 'center',
    alignSelf: 'center',
  },

  Agregatuprimerservicio: {
    fontSize: 20,
    textAlign: 'left',
    color: '#38B6FF',
    fontWeight: 'bold',
    alignContent: 'center',
  },
});
