import React from 'react';
import {Text, View, StyleSheet} from 'react-native';
import {TextInput} from 'react-native-paper';
import PropTypes from 'prop-types';


function FastText(props) {
  const [value, setValue] = React.useState(props.value);
  return (
    <TextInput
      style={props.style ? props.style : styles.inputStyles}
      {...props} //get ahoter props
      value={value}
      label={props.label}
      maxLength={props.max}
      onChangeText={(value) => {
        setValue(value);
        props.onChangeText(value);
      }}
      mode={'outlined'}
      disabled={props.disabled}
      theme={{
        colors: {primary: '#1359EE', background: 'white'},
        fonts: {
          normal: {fontFamily: 'CenturyGothic'},
          regular: {fontFamily: 'CenturyGothic'},
          medium: {fontFamily: 'CenturyGothic'},
          light: {fontFamily: 'CenturyGothic'},
          thin: {fontFamily: 'CenturyGothic'},
        },
      }}
      fontFamily={'CenturyGothic'}
    />
  );
}
export default FastText;
FastText.propTypes = {
  value: PropTypes.string,
  label: PropTypes.string,
  getValue: PropTypes.func,
};

const styles = StyleSheet.create({
  inputStyles: {
    backgroundColor: '#fff',
    marginVertical: 20,
    fontFamily: 'CenturyGothic',
    // fontWeight: 'normal',
  },
});