import React from 'react';
import {Image, ActivityIndicator, View, TouchableOpacity} from 'react-native';
import LoadingProgress from '../../common/LoadingProgress';
import Icon from 'react-native-vector-icons/FontAwesome';
import AppIntroSlider from 'react-native-app-intro-slider';
import {AuthContext} from '../../../app/api/AuthContext';
import style from './styles';
class IntroSliderContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      splash: props.splash,
    };
    if (props.splash.length == 0) props.navigation.navigate('Login');
    console.disableYellowBox = true;
  }

  _renderItem = ({item, index}) => {
    return (
      <View
        keyExtractor={index => item.name.toString()}
        style={style.container}>
        <Image
          placeholderStyle={{backgroundColor: 'trasparent'}}
          PlaceholderContent={
            <ActivityIndicator size={'large'} color={'#38B6FF'} />
          }
          source={{uri: item.file}}
          style={style.image}
        />
      </View>
    );
  };

  _renderNextButton = () => {
    return (
      <View style={style.buttonCircle}>
        <Icon name="arrow-right" color="rgba(255, 255, 255, .9)" size={24} />
      </View>
    );
  };
  _renderDoneButton = () => {
    return (
      <View style={style.buttonCircle}>
        <TouchableOpacity
          onPress={() => {
            this.props.navigation.navigate('Login');
          }}>
          <Icon name="check" color="rgba(255, 255, 255, .9)" size={24} />
        </TouchableOpacity>
      </View>
    );
  };
  render() {
    if (!this.state.splash) {
      return <LoadingProgress />;
    }
    return (
      <AppIntroSlider
        ref={ref => (this.Slider = ref)}
        renderItem={this._renderItem}
        slides={this.state.splash}
        renderNextButton={this._renderNextButton}
        renderDoneButton={this._renderDoneButton}
      />
    );
  }
}

export default function Slider({route, navigation}) {
  let {getSplash} = React.useContext(AuthContext);
  return (
    <IntroSliderContainer
      navigation={navigation}
      route={route}
      splash={getSplash()}
    />
  );
}
