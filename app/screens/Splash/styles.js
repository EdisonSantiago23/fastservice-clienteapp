import {StyleSheet, Dimensions} from 'react-native';
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  image: {
    width: '100%',
    height: '100%',
    resizeMode: 'cover',
    alignItems: 'center',
    alignContent: 'center',
  },
  doneButton: {
    flexDirection: 'column',
    alignContent: 'center',
    paddingHorizontal: 30,
    paddingVertical: 10,

  },
  buttonCircle: {
    width: 40,
    height: 40,
    backgroundColor: 'rgba(0, 0, 0, .2)',
    borderRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
  button: {
    fontSize: 16,
    color: '#000',
    backgroundColor: '#fff',
    paddingVertical: 10,
    paddingHorizontal: 15,
    textAlign: 'center',

  },
  iconContainer: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    width: 80,
  },
 
  touchable: {
    flex: 1,
    flexDirection: 'row',
  },
  mainContent: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'space-around',
  },
  text: {
    color: 'rgba(255, 255, 255, 0.8)',
    backgroundColor: 'transparent',
    textAlign: 'center',
    paddingHorizontal: 16,
  },

});
export default styles;
