import React, { Component } from 'react';
import {
    StyleSheet, Dimensions, StatusBar, Alert, View, ActivityIndicator
    , ScrollView, ImageBackground, Image, TouchableOpacity
} from 'react-native';
import { Block, Text, theme } from "galio-framework";
const { width, height } = Dimensions.get("screen");
const StatusHeight = StatusBar.currentHeight;
const HeaderHeight = (theme.SIZES.BASE * 3.5 + (StatusHeight || 0));
const thumbMeasure = (width - 48 - 32) / 3;
import mecanicoApi from '../../api/mecanico.js';
import { Header } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';

export default class Pagos extends Component {
    constructor(props) {
        super(props);
        this.mecanicoApi = new mecanicoApi();

        this.state = {
            initSplash: true,
            servicio: this.props.route.params.servicio,
            noti: this.props.route.params.noti,

            loginSpl: true,
            loadingBtn: false,
            userFeatures: {
                idmecanico: '',
                idusuario: '',
                tocken: '',
                headings: '',
                content: '',
                url: '',
                latitud: '',
                longitud: '',
                idemergencia: '',
            },

        };

    }
    reserva(tipopagos) {

        this.props.navigation.navigate('Calificar', { servicio: this.state.servicio, tipopago: tipopagos, noti: this.state.noti })

    }
    payphone(tipopagos) {

        this.props.navigation.navigate('PayPhone', { servicio: this.state.servicio, tipopago: tipopagos, noti: this.state.noti })

    }


    render() {
        return (<ScrollView
            showsVerticalScrollIndicator={false}
            style={{ width }}>
            <ImageBackground
                style={styles.profileContainer}
                imageStyle={styles.profileBackground}  >
         
                <View middle style={styles.avatarContainer}>
                    <Image
                        source={require('../../assets/png/lg.png')}
                        style={styles.avatar2} />
                    <Text
                        size={24}
                        color="#525F7F"
                        style={{ textAlign: "center", marginTop: 10 }}      >
                        MECÁNICA EXPRESS
      </Text>
                </View>

                <Block flex style={styles.profileCard}>
                    <Block flex>
                        <Block middle style={{ marginTop: 0, marginBottom: 10 }}>
                            <Text bold size={30} color="#32325D">
                                MÉTODOS DE PAGO
                              </Text>
                            <Block style={styles.divider} />
                        </Block>
                        <Block middle style={styles.nameInfo}>
                            <Text bold size={16} color="#525F7F">
                                Pago con 
                              </Text>
                            <Image
                                source={require('../../assets/png/metodos.png')}
                                style={{
                                    width: 300,
                                    height: 100,
                                    marginTop: '1%'

                                }} />
                            <TouchableOpacity
                                style={styles.btnVerEnSitio}
                                activeOpacity={0.7}
                                onPress={() => {this.payphone(1);
                                }}>
                                <Text style={{ fontSize: 14, textAlign: 'center', color: '#1F4997' }}>
                                    Pagar
          </Text>
                            </TouchableOpacity>
                        </Block>

                        <Block middle style={{ marginTop: 30, marginBottom: 16 }}>
                            <Block style={styles.divider} />
                        </Block>
                        <Block middle style={styles.nameInfo}>

                            <Text bold size={16} color="#525F7F">
                                Paga cuando llegue el mecánico al destino.
                              </Text>
                            <Image
                                source={require('../../assets/png/pay.png')}
                                style={{
                                    width: 160,
                                    height: 100,
                                    marginTop: '4%'
                                }} />
                        </Block>

                    </Block>
                    {this.renderBtn()}

                </Block>

            </ImageBackground>
        </ScrollView>

        );
    }
    renderBtn() {
        if (!this.state.loadingBtn) {
            return (
                <View>
                    <TouchableOpacity
                        onPress={() => {
                            this.reserva(1);
                        }}
                        style={styles.btnVerEnSitio}>
                        <Text style={{ fontSize: 14, textAlign: 'center', color: '#1F4997' }}>
                            Pagar
                        </Text>
                    </TouchableOpacity>

                </View>
            );
        } else {
            return (
                <TouchableOpacity style={styles.btnVerEnSitio}>
                    <ActivityIndicator size="small" color={'#1F4997'} />
                </TouchableOpacity>
            );
        }
    }
}
const styles = StyleSheet.create({
    container: {
        ...StyleSheet.absoluteFillObject
    },
    profile: {
        marginTop: Platform.OS === "android" ? -HeaderHeight : 0,
        // marginBottom: -HeaderHeight * 2,
        flex: 1
    },
    map: {
        ...StyleSheet.absoluteFillObject
    },
    headerContainer: {
        flexDirection: 'row',
        padding: 5,
        alignItems: 'flex-start',
    },
    btnVerEnSitio: {
        backgroundColor: '#F5F6F7',
        borderRadius: 20,
        width: '100%',
        marginTop: '5%',
        padding: '3%',
        color: 'blue',
        borderWidth: 1,
        borderColor: '#4982e6',
    },
    carousel: {
        position: 'absolute',
        bottom: 0,
        marginBottom: 48
    },
    cardContainer: {
        alignSelf: 'center',
        backgroundColor: '#617792',
        height: 600,
        width: 350,
        padding: 50,
        borderRadius: 24
    },
    cardImage: {
        height: 120,
        width: 300,
        bottom: 0,
        position: 'absolute',
        borderBottomLeftRadius: 24,
        borderBottomRightRadius: 24
    },
    button: {
        alignSelf: 'center',

        fontSize: 16,
        color: '#fff',
        backgroundColor: '#617792',
        paddingVertical: 10,
        paddingHorizontal: 15,
        textAlign: "center",
        height: 50,
        width: 350,
        padding: 50,
        borderRadius: 24
    },
    cardTitle: {
        color: 'white',
        fontSize: 22,
        alignSelf: 'center'
    },
    profile: {
        marginTop: Platform.OS === "android" ? -HeaderHeight : 0,
        // marginBottom: -HeaderHeight * 2,
        flex: 1
    },
    profileContainer: {
        width: width,
        height: height,
        color: '#617792',
        padding: 0,
        zIndex: 1
    },
    profileBackground: {
        width: width,
        height: height / 2
    },
    profileCard: {
        // position: "relative",
        padding: theme.SIZES.BASE,
        marginHorizontal: theme.SIZES.BASE,
        marginTop: 65,
        borderTopLeftRadius: 6,
        borderTopRightRadius: 6,
        backgroundColor: theme.COLORS.WHITE,
        shadowColor: "black",
        shadowOffset: { width: 0, height: 0 },
        shadowRadius: 8,
        shadowOpacity: 0.2,
        zIndex: 2
    },
    info: {
        paddingHorizontal: 40
    },
    avatarContainer: {
        position: "relative",
        marginTop: -80
    },
    avatar: {
        width: 124,
        height: 124,
        borderRadius: 62,
        borderWidth: 0
    },
    avatar2: {
        width: 100,
        height: 100,
        borderRadius: 62,
        borderWidth: 0,
        alignSelf: "center",
        marginTop: '25%'

    },
    nameInfo: {
        marginTop: 10
    },
    divider: {
        width: "90%",
        borderWidth: 1,
        borderColor: "#E9ECEF"
    },
    thumb: {
        borderRadius: 4,
        marginVertical: 4,
        alignSelf: "center",
        width: thumbMeasure,
        height: thumbMeasure
    }
});


