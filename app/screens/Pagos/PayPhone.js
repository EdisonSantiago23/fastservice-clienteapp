import React, {Component} from 'react';
import {
  StyleSheet,
  Dimensions,
  StatusBar,
  Linking,
  ScrollView,
  ImageBackground,
  Image,
  TouchableOpacity,
} from 'react-native';
import {Block, Text, theme} from 'galio-framework';
const {width, height} = Dimensions.get('screen');
const StatusHeight = StatusBar.currentHeight;
const HeaderHeight = theme.SIZES.BASE * 3.5 + (StatusHeight || 0);
const thumbMeasure = (width - 48 - 32) / 3;
import {Rating, AirbnbRating} from 'react-native-ratings';
import mecanicoApi from '../../api/mecanico';
import LoadingProgress from '../../common/LoadingProgress';
import {SCLAlert, SCLAlertButton} from 'react-native-scl-alert';
import AsyncStorage from '@react-native-community/async-storage';
import Constants from '../../common/Constants';

export default class PayPhone extends Component {
  constructor(props) {
    super(props);
    this.mecanicoApi = new mecanicoApi();

    this.state = {
      initSplash: true,
      showconfig: false,
      nombre: '',
      loading: false,

      loginSpl: true,
      starCount: 2.5,
      calificar: 5,
      noti: this.props.route.params.noti,

      servicio: this.props.route.params.servicio,
      tipopago: this.props.route.params.tipopago,
      userFeatures: {
        comentario: '',
      },
    };
    console.log(this.state.servicio.costoemergencia);
    this.ratingCompleted = this.ratingCompleted.bind(this);
    this.getname();
  }
  getname = async () => {
    var nomb = await AsyncStorage.getItem(Constants.STORE_NAME_USER);
    nomb = nomb.substring(1, nomb.length - 1);
    this.setState({
      nombre: nomb,
    });
  };
  updateStatusFeature(param, value) {
    let userFeatConst = this.state.userFeatures;
    userFeatConst[param] = value;
    this.setState({userFeatures: userFeatConst});
  }
  handleClose = () => {
    this.setState({showconfig: false});
    this.props.navigation.navigate('MenuAuxilioMotos');
  };
  estado = async cliente => {
    if (this.state.noti == 0) {
      try {
        this.setState({loading: true});
        cliente.estado = 4;
        cliente.pago = this.state.tipopago;
        cliente.comentario = this.state.userFeatures.comentario;
        cliente.calificacion =
          (cliente.calificacion + this.state.calificar) / 2;
        cliente.content =
          'EL cliente ' + cliente.nombrecliente + ' pago el servicio ';

        this.mecanicoApi.respuestaCliente(cliente).then(res => {
          if (res) {
            this.setState({loading: false, showconfig: true});
          } else {
            if (res.status && res.status == 'error') {
              Alert.alert('Error', res.message, [{text: 'Cerrar'}]);
              this.setState({loading: false});
            }
          }
        });
      } catch (err) {
        Alert.alert('Error', err.message, [{text: 'Cerrar'}]);
      }
    }
    if (this.state.noti == 1) {
      try {
        this.setState({loading: true});
        cliente.pago = this.state.tipopago;
        cliente.comentario = this.state.userFeatures.comentario;
        cliente.estado = 4;
        cliente.content =
          'EL cliente ' + this.state.nombre + ' compró la cotización';

        cliente.calificacion =
          (cliente.calificacion + this.state.calificar) / 2;
        this.mecanicoApi.comprarRepuesto(cliente).then(res => {
          if (res) {
            this.setState({loading: false, showconfig: true});
          } else {
            if (res.status && res.status == 'error') {
              Alert.alert('Error', res.message, [{text: 'Cerrar'}]);
              this.setState({loading: false});
            }
          }
        });
      } catch (err) {
        Alert.alert('Error', err.message, [{text: 'Cerrar'}]);
      }
    }
    if (this.state.noti == 2) {
      try {
        this.setState({loading: true});
        cliente.pago = this.state.tipopago;
        cliente.estado = 4;
        cliente.comentario = this.state.userFeatures.comentario;
        cliente.calificacion =
          (cliente.calificacion + this.state.calificar) / 2;
        this.mecanicoApi.respuestaCliente(cliente).then(res => {
          if (res) {
            this.setState({loading: false, showconfig: true});
          } else {
            if (res.status && res.status == 'error') {
              Alert.alert('Error', res.message, [{text: 'Cerrar'}]);
              this.setState({loading: false});
            }
          }
        });
      } catch (err) {
        Alert.alert('Error', err.message, [{text: 'Cerrar'}]);
      }
    }
    if (this.state.noti == 3) {
      try {
        this.setState({loading: true});
        cliente.pago = this.state.tipopago;
        cliente.estado = 4;
        cliente.nombreemergencia =
          'Garantia ' + cliente.nombreemergencia + ' Kilometros';
        cliente.comentario = this.state.userFeatures.comentario;
        cliente.calificacion =
          (cliente.calificacion + this.state.calificar) / 2;

        this.mecanicoApi.respuestaCliente(cliente).then(res => {
          if (res) {
            this.setState({loading: false, showconfig: true});
          } else {
            if (res.status && res.status == 'error') {
              Alert.alert('Error', res.message, [{text: 'Cerrar'}]);
              this.setState({loading: false});
            }
          }
        });
      } catch (err) {
        Alert.alert('Error', err.message, [{text: 'Cerrar'}]);
      }
    }
  };

  validador() {
    this.setState({loading: true});

    this.mecanicoApi
      .getInfoUserPay(this.state.servicio.idcliente)
      .then(resp => {
        if (resp?.documentId) {
          this.pagoseguro(this.state.servicio.idemergencia);
        } else {
          this.setState({loading: false});

          Linking.openURL('http://onelink.to/7qvdmu');
        }
      });
  }
  pagoseguro(id) {
    this.mecanicoApi.newpago(this.state.servicio.idcliente, id).then(resp => {
      this.setState({loading: false});
      if (resp.code === 200) {
        this.estado(this.state.servicio);
      }
    });
  }
  ratingCompleted(rating) {
    this.setState({calificar: rating});
  }

  render() {
    if (this.state.loading) {
      return <LoadingProgress />;
    }
    if (!this.state.loading) {
      return (
        <ScrollView>
          <SCLAlert
            theme="success"
            show={this.state.showconfig}
            title="MECÁNICA EXPRESS"
            onRequestClose={this.handleClose}
            cancellable={true}
            subtitle="Servicio finalizado">
            <SCLAlertButton theme="success" onPress={this.handleClose}>
              Ok
            </SCLAlertButton>
          </SCLAlert>
          <ImageBackground
            style={styles.profileContainer}
            imageStyle={styles.profileBackground}>
            <Block flex style={styles.profileCard}>
              <Block middle style={styles.avatarContainer}>
                <Image
                  source={{uri: this.state.servicio.fotomecanico}}
                  style={styles.avatar}
                />
              </Block>

              <Block flex>
                <Block style={{textAlign: 'center', alignItems: 'center'}}>
                  <Text bold size={30} color="#32325D">
                    Gracias por tu compra.
                  </Text>
                  <Image
                    source={require('../../assets/png/metodos.png')}
                    style={{
                      width: 300,
                      height: 100,
                      marginTop: '1%',
                    }}
                  />
                </Block>
                <Block middle style={{marginTop: 30, marginBottom: 16}}>
                  <Block style={styles.divider} />
                </Block>

                <Block middle style={styles.nameInfo}>
                  <Text bold size={16} color="#525F7F">
                    Confirmar tu pago
                  </Text>
                  <Text bold size={20} color="#32325D">
                    Servicio: {this.state.servicio.nombreemergencia}
                  </Text>
                  <Text bold size={20} color="#32325D">
                    Tu pago total es : $ {this.state.servicio.costoemergencia}
                  </Text>
                </Block>

                <TouchableOpacity
                  style={styles.btnVerEnSitio}
                  onPress={() => {
                    this.validador();
                  }}>
                  <Text
                    style={{
                      fontSize: 14,
                      textAlign: 'center',
                      color: '#1F4997',
                    }}>
                    Pagar
                  </Text>
                </TouchableOpacity>
              </Block>
            </Block>
          </ImageBackground>
        </ScrollView>
      );
    }
  }
}
const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
  },
  profile: {
    marginTop: Platform.OS === 'android' ? -HeaderHeight : 0,
    // marginBottom: -HeaderHeight * 2,
    flex: 1,
  },
  map: {
    ...StyleSheet.absoluteFillObject,
  },
  headerContainer: {
    flexDirection: 'row',
    padding: 5,
    alignItems: 'flex-start',
  },
  btnVerEnSitio: {
    backgroundColor: '#F5F6F7',
    borderRadius: 20,
    width: '100%',
    padding: '3%',
    color: 'blue',
    borderWidth: 1,
    borderColor: '#4982e6',
    marginTop: 20,
  },
  carousel: {
    position: 'absolute',
    bottom: 0,
    marginBottom: 48,
  },
  cardContainer: {
    alignSelf: 'center',
    backgroundColor: '#617792',
    height: 600,
    width: 350,
    padding: 50,
    borderRadius: 24,
  },
  cardImage: {
    height: 120,
    width: 300,
    bottom: 0,
    position: 'absolute',
    borderBottomLeftRadius: 24,
    borderBottomRightRadius: 24,
  },
  inputSearch: {
    borderBottomWidth: 1,
  },
  button: {
    alignSelf: 'center',

    fontSize: 16,
    color: '#fff',
    backgroundColor: '#617792',
    paddingVertical: 10,
    paddingHorizontal: 15,
    textAlign: 'center',
    height: 50,
    width: 350,
    padding: 50,
    borderRadius: 24,
  },
  cardTitle: {
    color: 'white',
    fontSize: 22,
    alignSelf: 'center',
  },
  profile: {
    marginTop: Platform.OS === 'android' ? -HeaderHeight : 0,
    // marginBottom: -HeaderHeight * 2,
    flex: 1,
  },
  profileContainer: {
    width: width,
    height: height,
    color: '#617792',
    padding: 0,
    zIndex: 1,
  },
  profileBackground: {
    width: width,
    height: height / 2,
  },
  profileCard: {
    // position: "relative",
    padding: theme.SIZES.BASE,
    marginHorizontal: theme.SIZES.BASE,
    marginTop: 65,
    borderTopLeftRadius: 6,
    borderTopRightRadius: 6,
    backgroundColor: theme.COLORS.WHITE,
    shadowColor: 'black',
    shadowOffset: {width: 0, height: 0},
    shadowRadius: 8,
    shadowOpacity: 0.2,
    zIndex: 2,
  },
  info: {
    paddingHorizontal: 40,
  },
  avatarContainer: {
    position: 'relative',
    marginTop: -80,
  },
  avatar: {
    width: 124,
    height: 124,
    borderRadius: 62,
    borderWidth: 0,
  },
  nameInfo: {
    marginTop: 20,
    textAlign: 'center',
  },
  divider: {
    width: '90%',
    borderWidth: 1,
    borderColor: '#E9ECEF',
  },
  thumb: {
    borderRadius: 4,
    marginVertical: 4,
    alignSelf: 'center',
    width: thumbMeasure,
    height: thumbMeasure,
  },
});
