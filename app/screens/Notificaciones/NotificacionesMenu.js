import React, {Component} from 'react';
import {
  Text,
  StyleSheet,
  View,
  TouchableOpacity,
  StatusBar,
  Image,
  Animated,
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import Constants from '../../common/Constants';
export default class NotificacionesMenu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      initSplash: true,
      loginSpl: true,
      nombre: '',
    };
    this.RotateValueHolder = new Animated.Value(0);
    this.getname();
  }
  getname = async () => {
    var nomb = await AsyncStorage.getItem(Constants.STORE_NAME_USER);
    nomb = nomb.substring(1, nomb.length - 1);
    this.setState({
      nombre: nomb,
    });
  };
  render() {
    const {panel2, panel2x} = style;

    return (
      <View style={style.container}>
        <StatusBar backgroundColor="#FF9100" barStyle="light-content" />

        <View style={[{flex: 1}, style.elementsContainer]}>
          <View style={{flex: 2}}>
            <TouchableOpacity
              style={[
                panel2x,
                {
                  marginTop: 4,
                  marginRight: 4,
                  padding: '5%',
                  alignItems: 'center',
                  marginBottom: 4,
                  marginLeft: 8,
                },
              ]}>
              <Text
                style={{
                  fontSize: 18,
                  fontWeight: 'bold',
                  color: '#617792',
                  textAlign: 'center',
                }}>
                MECÁNICA EXPRESS
              </Text>

              <Image
                style={{height: '30%', width: '100%', resizeMode: 'contain'}}
                source={require('../../assets/png/lg.png')}
                resizeMode="contain"
                resizeMethod="resize"
              />

              <Text
                style={{
                  fontSize: 20,
                  color: '#617792',
                  fontWeight: 'bold',
                  textAlign: 'center',
                }}>
                MIS PEDIDOS
              </Text>
            </TouchableOpacity>
          </View>

          <View style={{flex: 3}}>
            <View style={panel2}>
              <TouchableOpacity
                style={[
                  panel2x,
                  {
                    marginTop: 4,
                    marginRight: 4,
                    marginBottom: 4,
                    marginLeft: 8,
                  },
                ]}
                onPress={() =>
                  this.props.navigation.navigate('NotificacionesEmergencias')
                }>
                <Image
                  style={{height: '40%', width: '100%', resizeMode: 'contain'}}
                  source={require('../../assets/png/home/emergencia.png')}
                  resizeMode="contain"
                  resizeMethod="resize"
                />
                <Text
                  style={{
                    fontSize: 20,
                    color: '#617792',
                    fontWeight: 'bold',
                    textAlign: 'center',
                  }}>
                  EMERGENCIA
                </Text>
              </TouchableOpacity>
            </View>

            <View style={panel2}>
              <TouchableOpacity
                style={[
                  panel2x,
                  {
                    marginTop: 4,
                    marginRight: 8,
                    marginBottom: 4,
                    marginLeft: 4,
                  },
                ]}
                onPress={(_) =>
                  this.props.navigation.navigate('NotificacionesRepuestos')
                }>
                <Image
                  style={{height: '50%', width: '100%', resizeMode: 'contain'}}
                  resizeMode="contain"
                  resizeMethod="resize"
                  source={require('../../assets/png/home/mantenimiento.png')}
                />
                <Text
                  style={{
                    fontSize: 20,
                    color: '#617792',
                    fontWeight: 'bold',
                    textAlign: 'center',
                  }}>
                  REPUESTOS
                </Text>
              </TouchableOpacity>
            </View>

            <View style={panel2}>
              <TouchableOpacity
                style={[
                  panel2x,
                  {
                    marginTop: 4,
                    marginRight: 8,
                    marginBottom: 4,
                    marginLeft: 4,
                  },
                ]}
                onPress={(_) =>
                  this.props.navigation.navigate('NotificacionesMantenimiento')
                }>
                <Image
                  style={{height: '50%', width: '100%', resizeMode: 'contain'}}
                  resizeMode="contain"
                  resizeMethod="resize"
                  source={require('../../assets/png/home/repuesto.png')}
                />
                <Text
                  style={{
                    fontSize: 20,
                    color: '#617792',
                    fontWeight: 'bold',
                    textAlign: 'center',
                  }}>
                  MANTENIMIENTOS
                </Text>
              </TouchableOpacity>
            </View>
            <View style={panel2}>
              <TouchableOpacity
                style={[
                  panel2x,
                  {
                    marginTop: 4,
                    marginRight: 8,
                    marginBottom: 4,
                    marginLeft: 4,
                  },
                ]}
                onPress={(_) =>
                  this.props.navigation.navigate('NotificacionesMoto')
                }>
                <Image
                  style={{height: '50%', width: '100%', resizeMode: 'contain'}}
                  resizeMode="contain"
                  resizeMethod="resize"
                  source={require('../../assets/png/home/nuevamoto.png')}
                />
                <Text
                  style={{
                    fontSize: 20,
                    color: '#617792',
                    fontWeight: 'bold',
                    textAlign: 'center',
                  }}>
                  SERVICIO AUTORIZADO
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
    );
  }
}

const style = StyleSheet.create({
  container: {
    flex: 1,
  },
  headerStyle: {
    fontSize: 36,
    textAlign: 'center',
    fontWeight: '100',
    marginBottom: 24,
  },
  elementsContainer: {
    backgroundColor: '#fff',
    margin: 1,
  },

  panel1: {
    flex: 1,
    justifyContent: 'center',
  },
  panel2: {
    flex: 1,
    flexDirection: 'row',
  },
  panel2x: {
    flex: 1,
    justifyContent: 'center',
    elevation: 6,
    borderRadius: 10,
    backgroundColor: '#ffffff',
    padding: 10,
    marginHorizontal: 10,
    marginVertical: 10,
  },
  textTitle: {
    fontSize: 15,
  },
  touchable: {
    flex: 1,
  },
  colorFondo: {
    backgroundColor: '#617792',
    alignItems: 'center',
  },
});
