import React, {Component} from 'react';
import {
  StyleSheet,
  Alert,
  ScrollView,
  TouchableOpacity,
  Image,
  Animated,
  View,
} from 'react-native';
import mecanicoApi from '../../api/mecanico';
import LoadingProgress from '../../common/LoadingProgress';
import Constants from '../../common/Constants';
import {Block, Text, theme} from 'galio-framework';
import AsyncStorage from '@react-native-community/async-storage';
import {SCLAlert, SCLAlertButton} from 'react-native-scl-alert';
export default class NotificacionesMantenimiento extends Component {
  constructor(props) {
    super(props);
    this.mecanicoApi = new mecanicoApi();

    this.state = {
      initSplash: true,
      loginSpl: true,
      loading: true,
      reservas: [],
      show: false,
      showconfig: false,
      showconfigfin: false,
    };
    this.RotateValueHolder = new Animated.Value(0);
    this.getReservas();
  }

  getReservas = async () => {
    this.mecanicoApi
      .getMantenimientos(await AsyncStorage.getItem(Constants.STORE_ID_USER))
      .then((res) => {
        if (res.status) {
          this.setState({show: true, loading: false});
        }
        this.setState({
          reservas: res,
        });
        if (res != undefined) {
          this.setState({
            loading: false,
          });
        }
      });
  };
  handleOpen = () => {
    this.setState({show: true});
  };

  handleClose = () => {
    this.setState({show: false});
    this.props.navigation.navigate('NotificacionesMenu');
  };
  handleCloseinfofin = () => {
    this.setState({showconfigfin: false});
    this.getReservas();
  };
  handleCloseconf = () => {
    this.setState({showconfig: false});
  };
  estado = async (cliente, estado) => {
    try {
      this.setState({loading: true});
      cliente.estado = estado;
      if (estado == 5) {
        cliente.content =
          'EL cliente ' + cliente.nombrecliente + ' finalizo el servicio ';
      } else {
        cliente.content =
          'EL cliente ' +
          cliente.nombrecliente +
          ' aceptó al mecánico ' +
          cliente.nombremecanico;
      }
      this.mecanicoApi.respuestaCliente(cliente).then((res) => {
        if (res) {
          if (estado == 5) {
            this.setState({loading: false, showconfigfin: true});
          } else {
            this.setState({loading: false, showconfig: true});
            this.getReservas();
          }
        } else {
          if (res.status && res.status == 'error') {
            Alert.alert('Error', res.message, [{text: 'Cerrar'}]);
            this.setState({loading: false});
          }
        }
      });
    } catch (err) {
      Alert.alert('Error', err.message, [{text: 'Cerrar'}]);
    }
  };
  render() {
    if (this.state.loading) {
      return <LoadingProgress />;
    }
    if (!this.state.loading) {
      if (!this.state.reservas.status) {
        return (
          <View>
            <SCLAlert
              theme="success"
              show={this.state.showconfig}
              title="Auxlio Mecánico"
              onRequestClose={this.handleCloseconf}
              cancellable={true}
              subtitle="Confirmaste el mecánico">
              <SCLAlertButton theme="success" onPress={this.handleCloseconf}>
                Ok
              </SCLAlertButton>
            </SCLAlert>
            <SCLAlert
              theme="success"
              show={this.state.showconfigfin}
              title="Servicio finalizado"
              onRequestClose={this.handleCloseinfofin}
              cancellable={true}
              subtitle="Revisa tu factura en tu correo.">
              <SCLAlertButton theme="success" onPress={this.handleCloseinfofin}>
                Ok
              </SCLAlertButton>
            </SCLAlert>

            <ScrollView style={{height: '100%'}}>
              <View style={styles.container}>
                <View>
                  {this.state.reservas.map((novedad) => (
                    <View style={styles.contImage}>
                      <Block flex style={styles.profileCard}>
                        <Block middle style={styles.avatarContainer}>
                          <Text
                            style={{
                              fontSize: 20,
                              color: '#617792',
                              fontWeight: 'bold',
                            }}>
                            {novedad.nombremantenimiento}
                          </Text>
                          <Image
                            source={{
                              uri: Constants.BASEURI + novedad.fotomecanico,
                            }}
                            style={styles.avatar}
                          />
                          <Text
                            style={{
                              fontSize: 15,
                              color: '#617792',
                              fontWeight: 'bold',
                            }}>
                            {novedad.nombremecanico}
                          </Text>
                          {novedad.estado == 1 ? (
                            <TouchableOpacity
                              style={styles.btnVerEnSitio}
                              onPress={() => this.estado(novedad, 3)}>
                              <Text
                                style={{
                                  fontSize: 14,
                                  textAlign: 'center',
                                  color: '#1F4997',
                                }}>
                                CONFIRMAR MECÁNICO
                              </Text>
                            </TouchableOpacity>
                          ) : novedad.estado == 0 ? (
                            <Text
                              style={{
                                fontSize: 15,
                                color: '#FF333C',
                                fontWeight: 'bold',
                                textAlign: 'center',
                              }}>
                              {' '}
                              PENDIENTE{' '}
                            </Text>
                          ) : novedad.estado == 4 ? (
                            <TouchableOpacity
                              style={styles.btnVerEnSitio}
                              onPress={(_) => this.estado(novedad, 5)}>
                              <Text
                                style={{
                                  fontSize: 14,
                                  textAlign: 'center',
                                  color: '#1F4997',
                                }}>
                                FINALIZAR
                              </Text>
                            </TouchableOpacity>
                          ) : (
                            <TouchableOpacity
                              style={styles.btnVerEnSitio}
                              onPress={(_) =>
                                this.props.navigation.navigate('Pagos', {
                                  servicio: novedad,
                                  noti: 2,
                                })
                              }>
                              <Text
                                style={{
                                  fontSize: 14,
                                  textAlign: 'center',
                                  color: '#1F4997',
                                }}>
                                PAGAR SERVICIOS
                              </Text>
                            </TouchableOpacity>
                          )}
                          <Text
                            style={{
                              fontSize: 15,
                              color: '#617792',
                              fontWeight: 'bold',
                            }}>
                            {novedad.fecha}
                          </Text>
                          <Text
                            style={{
                              fontSize: 15,
                              color: '#617792',
                              fontWeight: 'bold',
                            }}>
                            {novedad.hora}
                          </Text>
                          <Text size={16}>Fecha de la solicitud</Text>
                        </Block>

                        <Block style={styles.info}>
                          <Block
                            middle
                            row
                            space="evenly"
                            style={{marginTop: 10, paddingBottom: 24}}
                          />
                          <Block middle style={styles.nameInfo}>
                            <Text bold size={28} color="#32325D">
                              {novedad.nombre}
                            </Text>
                          </Block>

                          <Block row space="between">
                            <Block middle>
                              <Text
                                bold
                                size={16}
                                color="#525F7F"
                                style={{marginBottom: 4}}>
                                {novedad.nombreemergencia}
                              </Text>
                              <Text size={16}>Tipo</Text>
                            </Block>
                            <Block middle>
                              <Text
                                bold
                                size={16}
                                color="#525F7F"
                                style={{marginBottom: 4}}>
                                ${novedad.costoemergencia}
                              </Text>
                              <Text size={16}>Precio</Text>
                            </Block>
                            <Block middle>
                              <Text
                                bold
                                color="#525F7F"
                                size={16}
                                style={{marginBottom: 4}}>
                                {novedad.nombrelocal}
                              </Text>

                              <Text size={16}>Local</Text>
                            </Block>
                            <Block middle>
                              <Text
                                bold
                                color="#525F7F"
                                size={16}
                                style={{marginBottom: 15}}>
                                {novedad.estado == 0 ? (
                                  <Text
                                    bold
                                    size={16}
                                    color="#FF333C"
                                    style={{marginBottom: 4, marginTop: '15%'}}>
                                    Pendiente
                                  </Text>
                                ) : novedad.estado == 1 ? (
                                  <Text
                                    bold
                                    size={16}
                                    color="#32BEDC"
                                    style={{marginBottom: 4}}>
                                    Aceptado
                                  </Text>
                                ) : novedad.estado == 4 ? (
                                  <Text
                                    bold
                                    size={16}
                                    color="#32DC36"
                                    style={{marginBottom: 4}}>
                                    Pagado
                                  </Text>
                                ) : (
                                  <Text
                                    bold
                                    size={16}
                                    color="#32DC36"
                                    style={{marginBottom: 4}}>
                                    Confirmado
                                  </Text>
                                )}
                              </Text>
                              <Text size={16}>Estado</Text>
                            </Block>
                          </Block>
                        </Block>
                      </Block>
                    </View>
                  ))}
                </View>
              </View>
            </ScrollView>
          </View>
        );
      } else {
        return (
          <View style={styles.container}>
            <SCLAlert
              theme="info"
              show={this.state.show}
              title="Mantenimientos"
              onRequestClose={this.handleClose}
              cancellable={true}
              subtitle="Actualmente no tienes mantenimientos pendientes">
              <SCLAlertButton theme="info" onPress={this.handleClose}>
                Atras
              </SCLAlertButton>
            </SCLAlert>
          </View>
        );
      }
    }
  }
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
  },
  profile: {
    flex: 1,
  },
  imagen: {
    width: 75,
    height: 75,
    marginLeft: '25%',
  },
  btnVerEnSitioL: {
    flexDirection: 'row',
    alignItems: 'center',
    alignContent: 'center',
    justifyContent: 'center',
    fontSize: 16,
    color: '#000',
    backgroundColor: '#32BEDC',
    paddingVertical: 10,
    paddingHorizontal: 15,
    textAlign: 'center',
    width: '50%',
    height: 50,
    marginVertical: 5,
  },
  contImage: {
    flex: 1,
    marginTop: 3,
    justifyContent: 'center',
    borderWidth: 2,
    width: 400,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
  },
  map: {
    ...StyleSheet.absoluteFillObject,
  },
  headerContainer: {
    flexDirection: 'row',
    padding: 5,
    alignItems: 'flex-start',
  },
  btnVerEnSitio: {
    backgroundColor: '#F5F6F7',
    borderRadius: 20,
    width: '35%',
    padding: '3%',
    color: 'blue',
    borderWidth: 1,
    marginTop: '5%',
    borderColor: '#4982e6',
  },
  carousel: {
    position: 'absolute',
    bottom: 0,
    marginBottom: 48,
  },
  cardContainer: {
    alignSelf: 'center',
    backgroundColor: '#617792',
    height: 600,
    width: 350,
    padding: 50,
    borderRadius: 24,
  },
  cardImage: {
    height: 120,
    width: 300,
    bottom: 0,
    position: 'absolute',
    borderBottomLeftRadius: 24,
    borderBottomRightRadius: 24,
  },
  button: {
    alignSelf: 'center',

    fontSize: 16,
    color: '#fff',
    backgroundColor: '#617792',
    paddingVertical: 10,
    paddingHorizontal: 15,
    textAlign: 'center',
    height: 50,
    width: 350,
    padding: 50,
    borderRadius: 24,
  },
  cardTitle: {
    color: 'white',
    fontSize: 22,
    alignSelf: 'center',
  },
  profile: {
    flex: 1,
  },
  profileContainer: {
    width: '100%',
    height: '50%',
    color: '#617792',
    padding: 0,
    zIndex: 1,
  },
  profileBackground: {
    width: '100%',
    height: 350 / 2,
  },
  profileCard: {
    // position: "relative",
    padding: 1,
    marginHorizontal: 1,
    marginTop: 65,
    borderTopLeftRadius: 6,
    borderTopRightRadius: 6,
    backgroundColor: 1,
    shadowColor: '#617792',
    shadowOffset: {200: 0, 200: 0},
    shadowRadius: 8,
    shadowOpacity: 0.2,
    zIndex: 2,
  },
  info: {
    paddingHorizontal: 30,
  },
  avatarContainer: {
    position: 'relative',
    marginTop: -60,
  },
  avatar: {
    width: 124,
    height: 124,
    borderWidth: 0,
    marginTop: '5%',
  },
  nameInfo: {
    marginTop: -7,
  },
  divider: {
    width: '90%',
    borderWidth: 1,
    borderColor: '#E9ECEF',
  },
  thumb: {
    borderRadius: 4,
    marginVertical: 4,
    alignSelf: 'center',
    width: 300,
    height: 300,
  },
});
