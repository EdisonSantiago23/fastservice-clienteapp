import React, {Component} from 'react';
import {
  StyleSheet,
  Alert,
  ScrollView,
  TouchableOpacity,
  Image,
  Animated,
  View,
} from 'react-native';
import mecanicoApi from '../../api/mecanico';
import LoadingProgress from '../../common/LoadingProgress';
import Constants from '../../common/Constants';
import {Block, Text} from 'galio-framework';
import AsyncStorage from '@react-native-community/async-storage';
import {SCLAlert, SCLAlertButton} from 'react-native-scl-alert';
export default class NotificacionesRepuestos extends Component {
  constructor(props) {
    super(props);
    this.mecanicoApi = new mecanicoApi();
    this.state = {
      initSplash: true,
      loginSpl: true,
      loading: true,
      reservas: [],
      show: false,
    };
    this.RotateValueHolder = new Animated.Value(0);
    this.getreservasrepuesto();
  }

  getreservasrepuesto = async () => {
    this.mecanicoApi
      .getreservasrepuesto(await AsyncStorage.getItem(Constants.STORE_ID_USER))
      .then((res) => {
        if (res.status) {
          this.setState({show: true, loading: false});
        }
        this.setState({
          reservas: res,
        });
        if (res != undefined) {
          this.setState({
            loading: false,
          });
        }
      });
  };
  handleOpen = () => {
    this.setState({show: true});
  };

  handleClose = () => {
    this.setState({show: false});
    this.props.navigation.navigate('NotificacionesMenu');
  };
  estado = async (cliente) => {
    try {
      this.setState({loading: true});
      cliente.estado = 5;
      console.log(cliente);
      this.mecanicoApi.respuestaclienteEstado(cliente).then((res) => {
        console.log(res);
        if (res) {
          this.getreservasrepuesto();

          this.setState({loading: false});
        } else {
          if (res.status && res.status == 'error') {
            Alert.alert('Error', res.message, [{text: 'Cerrar'}]);
            this.setState({loading: false});
          }
        }
      });
    } catch (err) {
      Alert.alert('Error', err.message, [{text: 'Cerrar'}]);
    }
  };
  render() {
    if (this.state.loading) {
      return <LoadingProgress />;
    }
    if (!this.state.loading) {
      if (!this.state.reservas.status) {
        return (
          <View>
            <ScrollView style={{height: '100%'}}>
              <View style={styles.container}>
                <View>
                  {this.state.reservas.map((novedad) => (
                    <View style={styles.contImage}>
                      <Block flex style={styles.profileCard}>
                        <Block middle style={styles.avatarContainer}>
                          <Image
                            source={{uri: Constants.BASEURI + novedad.url}}
                            style={styles.avatar}
                          />
                          <Text
                            style={{
                              fontSize: 15,
                              color: '#617792',
                              fontWeight: 'bold',
                            }}>
                            {novedad.nombremecanico}
                          </Text>
                          <View
                            style={{
                              flexDirection: 'row',
                              alignItems: 'center',
                            }}>
                            {novedad.totalrespuestas > 0 ? (
                              <TouchableOpacity
                                style={styles.btnVerEnSitio}
                                onPress={() =>
                                  this.props.navigation.navigate(
                                    'CotizacionesRepuestos',
                                    {cotizacion: novedad},
                                  )
                                }>
                                <Text
                                  style={{
                                    fontSize: 14,
                                    textAlign: 'center',
                                    color: '#1F4997',
                                  }}>
                                  VER COTIZACIONES
                                </Text>
                              </TouchableOpacity>
                            ) : (
                              <Text
                                bold
                                size={16}
                                color="#FF333C"
                                style={{marginBottom: 4, marginTop: '15%'}}>
                                SIN COTIZACIONES
                              </Text>
                            )}
                            {novedad.estado == 4 ? (
                              <TouchableOpacity
                                style={styles.btnVerEnSitio}
                                onPress={() => this.estado(novedad)}>
                                <Text
                                  style={{
                                    fontSize: 14,
                                    textAlign: 'center',
                                    color: '#1F4997',
                                  }}>
                                  Finalizar Cotización
                                </Text>
                              </TouchableOpacity>
                            ) : null}
                          </View>
                          <Text
                            style={{
                              fontSize: 15,
                              color: '#617792',
                              fontWeight: 'bold',
                            }}>
                            {novedad.fecha}
                          </Text>
                          <Text
                            style={{
                              fontSize: 15,
                              color: '#617792',
                              fontWeight: 'bold',
                            }}>
                            {novedad.hora}
                          </Text>
                          <Text size={16}>Fecha de la solicitud</Text>
                        </Block>

                        <Block style={styles.info}>
                          <Block
                            middle
                            row
                            space="evenly"
                            style={{marginTop: 10, paddingBottom: 24}}></Block>

                          <Block row space="between">
                            <Block middle>
                              <Text
                                bold
                                size={16}
                                color="#525F7F"
                                style={{marginBottom: 4}}>
                                {novedad.totalrespuestas}
                              </Text>
                              <Text size={16}>Cotizaciones</Text>
                            </Block>
                            <Block middle>
                              <Text
                                bold
                                color="#525F7F"
                                size={16}
                                style={{marginBottom: 4}}>
                                {novedad.nombre}
                              </Text>

                              <Text size={16}>Repuesto</Text>
                            </Block>
                            <Block middle>
                              <Text
                                bold
                                color="#525F7F"
                                size={16}
                                style={{marginBottom: 15}}>
                                {novedad.estado == 0 ? (
                                  <Text
                                    bold
                                    size={16}
                                    color="#32BEDC"
                                    style={{marginBottom: 4, marginTop: '15%'}}>
                                    Cotizando
                                  </Text>
                                ) : (
                                  <Text
                                    bold
                                    size={16}
                                    color="#FF333C"
                                    style={{marginBottom: 4, marginTop: '15%'}}>
                                    Adquitido
                                  </Text>
                                )}
                              </Text>
                              <Text size={16}>Estado</Text>
                            </Block>
                          </Block>
                        </Block>
                      </Block>
                    </View>
                  ))}
                </View>
              </View>
            </ScrollView>
          </View>
        );
      } else {
        return (
          <View style={styles.container}>
            <SCLAlert
              theme="info"
              show={this.state.show}
              title="Repuestos"
              onRequestClose={this.handleClose}
              cancellable={true}
              subtitle="Actualmente no tienes repuestos pendientes">
              <SCLAlertButton theme="info" onPress={this.handleClose}>
                Atras
              </SCLAlertButton>
            </SCLAlert>
          </View>
        );
      }
    }
  }
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
  },
  profile: {
    flex: 1,
  },
  imagen: {
    width: 75,
    height: 75,
    marginLeft: '25%',
  },
  btnVerEnSitioL: {
    flexDirection: 'row',
    alignItems: 'center',
    alignContent: 'center',
    justifyContent: 'center',
    fontSize: 16,
    color: '#000',
    backgroundColor: '#32BEDC',
    paddingVertical: 10,
    paddingHorizontal: 15,
    textAlign: 'center',
    width: '50%',
    height: 50,
    marginVertical: 5,
  },
  contImage: {
    flex: 1,
    marginTop: 3,
    justifyContent: 'center',
    borderWidth: 2,
    width: 400,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
  },
  map: {
    ...StyleSheet.absoluteFillObject,
  },
  headerContainer: {
    flexDirection: 'row',
    padding: 5,
    alignItems: 'flex-start',
  },
  btnVerEnSitio: {
    backgroundColor: '#F5F6F7',
    borderRadius: 20,
    width: '45%',
    padding: '3%',
    color: 'blue',
    borderWidth: 1,
    marginTop: '5%',
    borderColor: '#4982e6',
  },
  carousel: {
    position: 'absolute',
    bottom: 0,
    marginBottom: 48,
  },
  cardContainer: {
    alignSelf: 'center',
    backgroundColor: '#617792',
    height: 600,
    width: 350,
    padding: 50,
    borderRadius: 24,
  },
  cardImage: {
    height: 120,
    width: 300,
    bottom: 0,
    position: 'absolute',
    borderBottomLeftRadius: 24,
    borderBottomRightRadius: 24,
  },
  button: {
    alignSelf: 'center',

    fontSize: 16,
    color: '#fff',
    backgroundColor: '#617792',
    paddingVertical: 10,
    paddingHorizontal: 15,
    textAlign: 'center',
    height: 50,
    width: 350,
    padding: 50,
    borderRadius: 24,
  },
  cardTitle: {
    color: 'white',
    fontSize: 22,
    alignSelf: 'center',
  },
  profile: {
    flex: 1,
  },
  profileContainer: {
    width: '100%',
    height: '50%',
    color: '#617792',
    padding: 0,
    zIndex: 1,
  },
  profileBackground: {
    width: '100%',
    height: 350 / 2,
  },
  profileCard: {
    // position: "relative",
    padding: 1,
    marginHorizontal: 1,
    marginTop: 65,
    borderTopLeftRadius: 6,
    borderTopRightRadius: 6,
    backgroundColor: 1,
    shadowColor: '#617792',
    shadowOffset: {200: 0, 200: 0},
    shadowRadius: 8,
    shadowOpacity: 0.2,
    zIndex: 2,
  },
  info: {
    paddingHorizontal: 30,
  },
  avatarContainer: {
    position: 'relative',
    marginTop: -60,
  },
  avatar: {
    width: 224,
    height: 224,
    borderWidth: 0,
    marginTop: '5%',
  },
  nameInfo: {
    marginTop: -7,
  },
  divider: {
    width: '90%',
    borderWidth: 1,
    borderColor: '#E9ECEF',
  },
  thumb: {
    borderRadius: 4,
    marginVertical: 4,
    alignSelf: 'center',
    width: 300,
    height: 300,
  },
});
