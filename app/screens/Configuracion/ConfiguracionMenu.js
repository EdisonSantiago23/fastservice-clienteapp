import React, {useEffect} from 'react';
import {
  TouchableOpacity,
  View,
  ScrollView,
  Image,
  Linking,
  Modal,
} from 'react-native';
import FastText from '../Common/Components/FastText';
import {Contrata} from './InfoConfiData';
import Feather from 'react-native-vector-icons/AntDesign';
import styles from './styles';
import {CustomDividerpracs} from '../Common/MiniComponents';
import {AuthContext} from '../../api/AuthContext';
import {SafeAreaView} from 'react-native-safe-area-context';
import GradientButton from '../Common/Components/GradientButton';
import GradientButtonCancelar from '../Common/Components/GradientButtonCancelar';
import ExoLoading from '../../common/LoadingProgress';
function ConfiguracionMenu({navigation, route}) {
  const {getUser} = React.useContext(AuthContext);
  const [userFeatures] = React.useState(getUser());
  const [data] = React.useState(Contrata);
  const {signOut} = React.useContext(AuthContext);
  const [visible, setVisible] = React.useState(false);
  const [visiblesalir, setVisiblesalir] = React.useState(false);
  const [terminos, setTerminos] = React.useState(false);
  const [loading] = React.useState(false);
  useEffect(() => {
    navigation.addListener('focus', () => {
      setTerminos(false);
    });
  }, []);

  if (loading) {
    return <ExoLoading />;
  } else {
    return (
      <SafeAreaView style={{flex: 1}}>
        <Modal
          visible={visible}
          animationType="slide"
          transparent={true}
          onRequestClose={() => {
            setVisible(false);
          }}
          onTouchOutside={() => {
            setVisible(false);
          }}>
          <View style={styles.centeredView}>
            <View style={styles.modalView}>
              <Image
                style={{width: 200, height: 100}}
                resizeMode={'contain'}
                source={require('../../assets/png/lg.png')}
              />
              <FastText others={styles.modalText}>Contactos</FastText>
              <View>
                <View style={{marginTop: 20}} />
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                  }}>
                  <FastText bold others={styles.hola}>
                    Teléfono: 0995930430
                  </FastText>
                  <TouchableOpacity
                    onPress={() =>
                      Linking.openURL(
                        'whatsapp://send?text=Hola me gustaría obtener más información.&phone= +5930995930430',
                      )
                    }>
                    <Image
                      style={{width: 50, height: 50, marginTop: -35}}
                      resizeMode={'contain'}
                      source={require('../../assets/png/whatsapp.png')}
                    />
                  </TouchableOpacity>
                </View>

                <FastText bold others={styles.completa}>
                  Correo: info@balinetsoft.com
                </FastText>
              </View>

              <View
                style={{
                  width: '100%',
                  alignItems: 'center',
                  alignSelf: 'center',
                  alignContent: 'center',
                }}>
                <GradientButton
                  label={'Ok'}
                  enabled={true}
                  others={{marginVertical: 20, width: '100%'}}
                  onPress={() => setVisible(false)}
                />
              </View>
            </View>
          </View>
        </Modal>
        <Modal
          visible={visiblesalir}
          animationType="slide"
          transparent={true}
          onRequestClose={() => {
            setVisiblesalir(false);
          }}
          onTouchOutside={() => {
            setVisiblesalir(false);
          }}>
          <View style={styles.centeredView}>
            <View style={styles.modalView}>
              <Image
                style={{width: 200, height: 100}}
                resizeMode={'contain'}
                source={require('../../assets/png/lg.png')}
              />
              <FastText others={styles.modalText}>
                ¿Quieres cerrar sesión?
              </FastText>

              <View style={{width: '100%'}}>
                <GradientButton
                  label={'Confirmar'}
                  others={{marginVertical: 20, width: '100%'}}
                  enabled={true}
                  onPress={() => signOut()}
                />
                <GradientButtonCancelar
                  label={'Cancelar'}
                  campo={false}
                  others={{marginVertical: 1, width: '100%'}}
                  onPress={() => setVisiblesalir(false)}
                />
              </View>
            </View>
          </View>
        </Modal>
        <ScrollView style={{flex: 1}} showsVerticalScrollIndicator={false}>
          <View style={styles.hederall}>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                width: '100%',
                marginTop: 20,
              }}>
              <View style={{width: '30%'}}>
                <Image
                  style={styles.avatar}
                  resizeMode={'contain'}
                  source={require('../../assets/png/lg.png')}
                />
              </View>
              <View style={{width: '60%'}}>
                <FastText others={styles.name} bold>
                  {userFeatures.nombre}
                </FastText>
                <FastText others={styles.profesion}>
                  {userFeatures.correo}
                </FastText>
              </View>
              <View style={{width: '10%', marginVertical: -20}} />
            </View>

            <View style={{marginVertical: 20}} />
            <ScrollView showsVerticalScrollIndicator={false}>
              {data.items.map((item, index) => renderItems(item, index))}
            </ScrollView>
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
  function renderItems(item, index) {
    if (item.name == 'terminoslegales') {
      return (
        <View key={index.toString()}>
          <TouchableOpacity
            key={index.toString()}
            onPress={() => {
              setTerminos(!terminos);
            }}>
            <CustomDividerpracs />
            <View style={styles.cardV}>
              <View
                style={{
                  flexDirection: 'row',
                  alignContent: 'center',
                  alignSelf: 'center',
                  alignItems: 'center',
                  marginVertical: 20,
                }}>
                <Image
                  resizeMode="contain"
                  source={item.icon}
                  style={styles.imagesva}
                />
                <FastText others={styles.itemTitleV}>{item.title}</FastText>
              </View>
              <Feather
                name={terminos ? 'down' : 'right'}
                onPress={() => {
                  setTerminos(!terminos);
                }}
                style={{}}
                color={'#FF9100'}
                size={30}
              />
            </View>
          </TouchableOpacity>
          {terminos ? (
            <View style={{marginLeft: '10%'}}>
              <TouchableOpacity
                onPress={() => {
                  Linking.openURL(item.legales);
                }}>
                <View style={styles.cardV}>
                  <View
                    style={{
                      flexDirection: 'row',
                      alignContent: 'center',
                      alignSelf: 'center',
                      alignItems: 'center',
                      marginVertical: 20,
                    }}>
                    <Image
                      resizeMode="contain"
                      source={item.icon}
                      style={styles.imagesva}
                    />
                    <FastText others={styles.itemTitleV}>
                      Términos y condiciones
                    </FastText>
                  </View>
                  <Feather
                    name={'right'}
                    onPress={() => {
                      Linking.openURL(item.legales);
                    }}
                    style={{}}
                    color={'#FF9100'}
                    size={30}
                  />
                </View>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => {
                  Linking.openURL(item.privacidad);
                }}>
                <CustomDividerpracs />
                <View style={styles.cardV}>
                  <View
                    style={{
                      flexDirection: 'row',
                      alignContent: 'center',
                      alignSelf: 'center',
                      alignItems: 'center',
                      marginVertical: 20,
                    }}>
                    <Image
                      resizeMode="contain"
                      source={item.icon}
                      style={styles.imagesva}
                    />
                    <FastText others={styles.itemTitleV}>
                      Políticas de privacidad
                    </FastText>
                  </View>
                  <Feather
                    name={'right'}
                    onPress={() => {
                      Linking.openURL(item.privacidad);
                    }}
                    style={{}}
                    color={'#FF9100'}
                    size={30}
                  />
                </View>
              </TouchableOpacity>
            </View>
          ) : null}
        </View>
      );
    }
    if (item.name === 'Cerrar') {
      return (
        <TouchableOpacity
          key={index.toString()}
          onPress={() => {
            setVisiblesalir(true);
          }}>
          <CustomDividerpracs />
          <View key={index.toString()} style={styles.cardV}>
            <View
              style={{
                flexDirection: 'row',
                alignContent: 'center',
                alignSelf: 'center',
                alignItems: 'center',
                marginVertical: 20,
              }}>
              <Image
                resizeMode="contain"
                source={item.icon}
                style={styles.imagesva}
              />
              <FastText others={styles.itemTitleV}>{item.title}</FastText>
            </View>
            <Feather
              name={'right'}
              onPress={() => {
                setVisiblesalir(true);
              }}
              style={{}}
              color={'#FF9100'}
              size={30}
            />
          </View>
        </TouchableOpacity>
      );
    }
    if (item.name === 'contactos') {
      return (
        <TouchableOpacity
          key={index.toString()}
          onPress={() => {
            setVisible(true);
          }}>
          <CustomDividerpracs />
          <View key={index.toString()} style={styles.cardV}>
            <View
              style={{
                flexDirection: 'row',
                alignContent: 'center',
                alignSelf: 'center',
                alignItems: 'center',
                marginVertical: 20,
              }}>
              <Image
                resizeMode="contain"
                source={item.icon}
                style={styles.imagesva}
              />
              <FastText others={styles.itemTitleV}>{item.title}</FastText>
            </View>
            <Feather
              name={'right'}
              onPress={() => {
                setVisible(true);
              }}
              style={{}}
              color={'#FF9100'}
              size={30}
            />
          </View>
        </TouchableOpacity>
      );
    }
    if (item.name == 'Detalle') {
      return (
        <TouchableOpacity
          key={index.toString()}
          onPress={() => {
            navigation.navigate(item.name);
          }}>
          <CustomDividerpracs />
          <View key={index.toString()} style={styles.cardV}>
            <View
              style={{
                flexDirection: 'row',
                alignContent: 'center',
                alignSelf: 'center',
                alignItems: 'center',
                marginVertical: 20,
              }}>
              <Image
                resizeMode="contain"
                source={item.icon}
                style={styles.imagesva}
              />
              <FastText others={styles.itemTitleV}>{item.title}</FastText>
            </View>
            <Feather
              name={'right'}
              onPress={() => {
                navigation.navigate(item.name);
              }}
              style={{}}
              color={'#FF9100'}
              size={30}
            />
          </View>
        </TouchableOpacity>
      );
    }
    if (item.name == 'Informacion') {
      return (
        <TouchableOpacity
          key={index.toString()}
          onPress={() => {
            navigation.navigate(item.name);
          }}>
          <CustomDividerpracs />
          <View key={index.toString()} style={styles.cardV}>
            <View
              style={{
                flexDirection: 'row',
                alignContent: 'center',
                alignSelf: 'center',
                alignItems: 'center',
                marginVertical: 20,
              }}>
              <Image
                resizeMode="contain"
                source={item.icon}
                style={styles.imagesva}
              />
              <FastText others={styles.itemTitleV}>{item.title}</FastText>
            </View>
            <Feather
              name={'right'}
              onPress={() => {
                navigation.navigate(item.name);
              }}
              style={{}}
              color={'#FF9100'}
              size={30}
            />
          </View>
        </TouchableOpacity>
      );
    }
  }
}

export default ConfiguracionMenu;
