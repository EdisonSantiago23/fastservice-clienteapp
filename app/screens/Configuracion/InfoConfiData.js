export let Contrata;
export let Ofrece;
Contrata = {
  items: [
    {
      icon: require('../../assets/png/Config/Pago.png'),
      title: 'Acerca de nosotros',
      name: 'Informacion',
    },
    {
      icon: require('../../assets/png/Config/MisDatos.png'),
      title: 'Nuestros servicios',
      name: 'Detalle',
    },
    {
      icon: require('../../assets/png/Config/terminos.png'),
      title: 'Términos legales',
      name: 'terminoslegales',
      legales: 'https://exoskill.app/terminos-y-condiciones',
      privacidad: 'https://exoskill.app/politica-de-privacidad',
    },

    {
      icon: require('../../assets/png/Config/confi.png'),
      title: 'Contactos',
      name: 'contactos',
    },
    {
      icon: require('../../assets/png/Config/cerrar.png'),
      title: 'Cerrar sesión',
      name: 'Cerrar',
    },

  ],
};
