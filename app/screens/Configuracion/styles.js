import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  cardV: {
    flexDirection: 'row',
    alignContent: 'center',
    alignSelf: 'center',
    alignItems: 'center',
    width: '100%',
    justifyContent: 'space-between',
  },
  hola: {textAlign: 'left'},
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(97, 97, 97, 0.3)',
  },
  modalView: {
    margin: 20,
    backgroundColor: 'white',
    borderRadius: 20,
    padding: 35,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    width: '100%',
  },
  modalText: {
    fontSize: 20,
    textAlign: 'center',
  },
  hederall: {
    width: '95%',
    alignSelf: 'center',
    flex: 1,
    margin: 5,
  },
  name: {
    fontSize: 20,
    color: '#FF9100',
  },
  avatar: {
    width: 85,
    height: 85,
    borderRadius: 62,
  },
  profesion: {
    fontSize: 13,
    color: '#2F2F2F',
  },

  itemTitleV: {
    color: '#000',
    fontSize: 15,
    marginHorizontal: 10,
  },
  imagesva: {
    width: 30,
    height: 30,
  },
});
export default styles;
